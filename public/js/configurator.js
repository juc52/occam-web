$(function() {
  var url = window.document.location.protocol +
            '//' +
            window.document.location.host;

  caja.initialize({
    cajaServer: url + "/js/caja",
    debug:      true
  });

  $('.configurator').each(function() {
    var configurator_url = $(this).data('configurator-url');
    var base_url = url + configurator_url;
    var frame_path = base_url + "/" + $(this).data('configurator-file');

    var uriPolicy = {
      rewrite: function(uri) {
        console.log("rewrite " + uri);
        console.log((uri + "").substring(0, base_url.length));
        if ((uri + "").substring(0, base_url.length) === base_url) {
          console.log("ok");
          return uri;
        }
        console.log("rejected");
        return undefined;
      },
      fetch: function(uri) {
        console.log("fetch " + uri);
        console.log((uri + "").substring(0, base_url.length));
        if ((uri + "").substring(0, base_url.length) === base_url) {
          console.log("ok");
          return true;
        }
        console.log("rejected");
        return undefined;
      }
    };

    var occam = {
      log: function(message) {
        console.log(message);
      },
      xhr: function(url, mime, callback) {
        // TODO: handle various url schemes
        url = base_url + "/" + url;
        var req = new XMLHttpRequest;
        if (arguments.length < 3) callback = mime, mime = null; else if (mime && req.overrideMimeType) req.overrideMimeType(mime);
        req.open("GET", url, true);
        if (mime) req.setRequestHeader("Accept", mime);
        req.onreadystatechange = function() {
          if (req.readyState === 4) {
            var s = req.status;
            callback(!s && req.response || s >= 200 && s < 300 || s === 304 ? req : null);
          }
        };
        req.send(null);
      },
      text: function(url, mime, callback) {
        function ready(req) {
          callback(req && req.responseText);
        }
        if (arguments.length < 3) {
          callback = mime;
          mime = null;
        }
        occam.xhr(url, mime, ready);
      },
      json: function(url, callback) {
        occam.text(url, "application/json", function(text) {
          callback(text ? JSON.parse(text) : null);
        });
      },
      html: function(url, callback) {
        occam.text(url, "text/html", function(text) {
          if (text != null) {
            var range = document.createRange();
            range.selectNode(document.body);
            text = range.createContextualFragment(text);
          }
          callback(text);
        });
      },
      xml: function(url, mime, callback) {
        function ready(req) {
          console.log(req);
          console.log(req.responseXML);
          console.log(req);
          console.log(req.responseXML);
          //callback("!!!");
          callback(req.responseXML);
          //callback(req && req.responseXML);
        }
        if (arguments.length < 3) {
          callback = mime;
          mime = null;
        }
        occam.xhr(url, mime, ready);
      },
      appendSVG: function(url, selector, callback) {
        function ready(req) {
          console.log(req);
          var importedNode = document.importNode(req.responseXML.documentElement, true);
          // TODO: remove other events
          // TODO: (security) audit
          $(importedNode).find('*').attr('onmouseover', '');
          $(selector).append(importedNode);
          callback();
        }
        occam.xhr(url, ready);
      }
    };

    caja.load($(this)[0], uriPolicy, function(frame) {
      // Tame functions
      caja.markReadOnlyRecord(occam);
      caja.markFunction(occam.log);
      caja.markFunction(occam.appendSVG);
      caja.markFunction(occam.text);
      caja.markFunction(occam.json);
      caja.markFunction(occam.html);
      caja.markFunction(occam.xhr);
      caja.markFunction(occam.xml);
      var tamedOccam = caja.tame(occam);

      var api = {
        occam: tamedOccam
      };

      frame.code(frame_path, 'text/html')
           .api(api)
           .run();
    });
  });
});
