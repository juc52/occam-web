function startWebsocket(timer_id) {
  var scheme = "ws://";
  if (window.document.location.protocol == "https:") {
    scheme = "wss://";
  }
  var uri    = scheme + window.document.location.host + "/";
  var ws     = new WebSocket(uri);

  $(window).on('beforeunload', function(){
    ws.close();
  });

  var occam_ws = {
    "pending": [],
    "terminals": {}
  };

  occam_ws.updateTerminal = function(name, data) {
    occam_ws.terminals[name].data = data;
  };

  occam_ws.attachTerminal = function(name, data, element, link_text) {
    var terminal_internal_name = name + Math.floor(Math.random() * (1000 + 1)) + Object.keys(occam_ws.terminals).length;
    var context = {
      "lineCount": 0,
      "buffer": {"mode": "normal", "saved": []},
      "cursor": {"visible": true, "blinking": false},
      "scroll": {"start": 0, "end": 0},
      "saved": {"x": 0, "y": 0, "fg": "#ddd", "bg": "#333"},
      "lineWrap": true,
      "charHeight": 0,
      "charWidth":  0,
      "height":     0,
      "width":      0,
      "x":  0,
      "y":  0,
      "fg": "#ddd",
      "bg": "#333",
      "caret": $("<div></div>").addClass("caret").css({
        "position": "absolute",
        "width": 0,
        "height": 0,
        "background-color": "#ddd",
        "z-index": "999"
      })
    };

    var terminal = {
      "element": element,
      "data": data,
      "internal_name": terminal_internal_name,
      "name": name,
      "context": context
    };

    occam_ws.terminals[terminal_internal_name] = terminal;

    var tmp = $('<span>X</span>');
    element.append(tmp);
    context.charHeight = tmp.height();
    context.charWidth = tmp.width();
    context.height = 20;
    context.width  = element[0].clientWidth / context.charWidth;
    context.height = Math.round(context.height - 0.5);
    context.width  = Math.round(context.width  - 0.5);
    context.scroll.start = 0;
    context.scroll.end   = context.height - 1;
    // Recalcuate the average cell width because some browsers
    // are trying to be too clever with monospace fonts.
    tmp.text(new Array(context.width + 1).join("\u00a0"));
    context.charWidth = tmp.width() / context.width;
    tmp.remove();
    element.css({
      "position": "relative"
    });
    element.innerHeight(context.height * context.charHeight + 30);

    console.log("adding lines to element");
    console.log(element);
    console.log(context);
    vtAddLines(context.height, element, context);

    if (link_text === undefined) {
      link_text = "Run Terminal";
    }

    if (element.data('link-text') != undefined) {
      link_text = element.data('link-text');
    }

    var run_link = $('<a>' + link_text + '</a>').attr('href', '#').on('click', function(event) {
      event.stopPropagation();
      event.preventDefault();

      $(this).remove();
      vtMove(0,0,element,context);

      occam_ws.openTerminal(name, terminal_internal_name, terminal.data, element);
    });
    element.prepend(run_link);

    context.caret.css({
      "height": context.charHeight,
      "width":  context.charWidth,
      "left": run_link.position().left + run_link.width() + 3,
      "top": run_link.position().top + element[0].scrollTop,
    });

    element.append(context.caret);
  }

  occam_ws.openTerminal = function(name, terminal_internal_name, data, element) {
    if (ws.readyState == 1) {
      var terminal = occam_ws.terminals[terminal_internal_name];
      var message = {
        "request":  "open",
        "spawning": name,
        "data": data,
        "rows": terminal.context.height,
        "cols": terminal.context.width,
        "terminal": terminal_internal_name
      };
      console.log(message);
      ws.send(JSON.stringify(message));

      var sendKey = function(key) {
        var message = {
          "request": "write",
          "terminal": terminal_internal_name,
          "input": key
        };
        ws.send(JSON.stringify(message));
      };

      element.on('keydown', function(event) {
        // Handle special cases
        var key = "";
        if (event.which == 8) {
          key = "\b";
        }
        else if (event.which == 0x1b) {
          key = "\x1b";
        }
        else if (event.which == 37) { // Left arrow
          key = "\x1b[D";
        }
        else if (event.which == 38) { // Up arrow
          key = "\x1b[A";
        }
        else if (event.which == 39) { // Right arrow
          key = "\x1b[C";
        }
        else if (event.which == 40) { // Down arrow
          key = "\x1b[B";
        }

        if (key != "") {
          sendKey(key);
          event.stopPropagation();
          event.preventDefault();
        }
      });

      element.on('keyup', function(event) {
        // Handle special cases
        if (event.which == 8) {
          event.stopPropagation();
          event.preventDefault();
        }
      });

      element.on('keypress', function(event) {
        event.stopPropagation();
        event.preventDefault();

        var key = event.key;
        if (key == undefined) {
          key = String.fromCharCode(event.which || event.charCode);
        }

        if (event.which == 0x1e) {
          return;
        }
        else if (event.which == 13) {
          key = "\n";
        }
        else if (event.which == 9 || event.charCode == 9 || event.key == "Tab") {
          key = "\t";
        }
        else if (event.which == 8) {
          return;
        }
        else if (key.length > 1) {
          return;
        }
        else if (event.ctrlKey) {
          key = String.fromCharCode(key.charCodeAt(0) - "a".charCodeAt(0) + 1);
        }
        else if (event.altKey) {
          key = "\e" + key;
        }

        sendKey(key);
      });
    }
    else {
      occam_ws.pending[occam_ws.pending.length] = terminal_internal_name;
    }
  };

  ws.onclose = function(message) {
  };

  var vtHideCaret = function(element, context) {
    context.cursor.visible = false;
    context.caret.css({
      "visibility": "hidden"
    });
  };

  var vtShowCaret = function(element, context) {
    context.cursor.visible = true;
    context.caret.css({
      "visibility": "visible"
    });
  };

  var vtFitCoords = function(x, y, element, context, fromCaret, scrollDown) {
    if (fromCaret == undefined) {
      fromCaret = false;
    }

    if (scrollDown == undefined) {
      scrollDown = false;
    }

    if (y > context.scroll.end) {
      if (fromCaret && scrollDown) {
        vtAddLines(y - context.scroll.end, element, context, context.scroll.end + 1);
        y = context.scroll.end;
      }
    }
    if (y < 0) {
      y = 0;
    }
    if (x >= context.width) {
      x = context.width - 1;
    }
    if (x < 0) {
      x = 0;
    }

    return {
      "x": x,
      "y": y
    };
  };

  var vtMove = function(x, y, element, context, fromCaret, scrollDown) {
    if (fromCaret == undefined) {
      fromCaret = false;
    }

    if (scrollDown == undefined) {
      scrollDown = false;
    }

    var pos = vtFitCoords(x, y, element, context, fromCaret, scrollDown);
    context.x = pos.x;
    context.y = pos.y;

    var lines       = element.children('.line');
    var currentLine = lines.eq(lines.length-(context.height - context.y));
    context.caret.css({
      "left": context.x * context.charWidth + currentLine.position().left+1,
      "top":  currentLine.position().top + element[0].scrollTop,
      "background-color": context.fg,
    });
  };

  var vtDeleteLines = function(count, element, context, position) {
    if (position == undefined) {
      // Remove last line, count times
      position = context.height-1;
    }

    var lines = element.children('.line');

    for (var i = 0; i < count; i++) {
      lines.eq(lines.length - context.height + position).remove();
    }
  };

  var vtAddLines = function(count, element, context, position) {
    if (position == undefined) {
      // Add to end
      position = context.height;
    }

    var lines = element.children('.line');
    var beforeLine = lines.eq(lines.length - context.height + position);

    var blankLine = new Array(context.width + 1).join("&nbsp;");
    for (var i = 0; i < count; i++) {
      var line = $("<div class='line'></div>")
      if (position == context.height) {
        element.append(line);
      }
      else {
        beforeLine.before(line);
      }

      context.lineCount++;

      // Remove first line if the buffer is full
      while (context.lineCount > 50) {
        element.children('.line:first').remove();
        context.lineCount--;
      }

      var appendSpan = $("<span style='color:" + context.fg + ";background-color:" + context.bg + ";'>" + blankLine + "</span>");
      line.append(appendSpan);
    }
  };

  var vtPrint = function(x, y, data, element, context, fromCaret, scrollDown) {
    if (fromCaret == undefined) {
      fromCaret = false;
    }

    if (scrollDown == undefined) {
      scrollDown = false;
    }

    var pos = vtFitCoords(x, y, element, context, fromCaret, scrollDown);
    x = pos.x;
    y = pos.y;

    if (data.length == 0) {
      return {
        "x": x,
        "y": y
      };
    }

    // Look at coordinates and append the text with the current context
    // Combine contexts when appropriate
    // Newlines/backspaces/tabs are resolved here
    data = data.replace("\t", "    ");

    var lines      = element.children('.line');
    var lineCount  = lines.length;

    var currentLine = lines.eq(lines.length-(context.height - y));
    var currentSpan = null;
    var currentX = 0;
    currentLine.children().each(function() {
      currentSpan = $(this);
      if (currentSpan.text().length > 0 && (currentX + currentSpan.text().length >= x)) {
        // Split this span
        var splitPosition = x - currentX;
        var tmp = currentSpan.clone();
        var left = currentSpan.text().substring(0, splitPosition);
        var right = currentSpan.text().substring(splitPosition);
        currentSpan.text(left);
        tmp.text(right);

        if (right.length > 0) {
          currentSpan.after(tmp);
        }

        if (splitPosition == 0) {
          currentSpan.remove();
          currentSpan = tmp;
        }

        if (right.length > 0) {
          currentSpan = tmp;
        }
        else {
          currentSpan = currentSpan.next();
        }

        currentX = 0;
        return false;
      }
      currentX += currentSpan.text().length;
    });

    var appendSpan = $("<span style='color:" + context.fg + ";background-color:" + context.bg + ";'>" + "</span>");
    currentSpan.before(appendSpan);

    for (var i = 0, len = data.length; i < len; i++) {
      // Characters that precede us on the line stay there
      // Characters are overwritten as we write to the terminal
      var chr = data[i];
      if (chr == "\x07") {
        console.log("ding");
        // Bell
        continue;
      }
      else if (chr == " ") {
        // All spaces should be non-breaking
        chr = "\u00a0";
      }
      else if (chr == "\r") {
        // Go to beginning of line
        if (x != 0) {
          return vtPrint(0, y, data.substring(i+1), element, context, fromCaret);
        }
        continue;
      }
      else if (chr == "\b") {
        // Backspace, just jump back one character
        if (x > 0) {
          return vtPrint(x - 1, y, data.substring(i+1), element, context, fromCaret);
        }
        continue;
      }
      else if (chr == "\n") {
        // Go to next line
        return vtPrint(0, y + 1, data.substring(i+1), element, context, fromCaret, true);
      }

      if (chr != "") {
        // Add character to our span
        appendSpan.text(appendSpan.text() + chr);

        // Overwrite the current character
        // Maintain the context throughout
        if (currentSpan == null) {
        }
        else if (currentSpan.text().length == 1) {
          // Destroy this span
          var tmp = currentSpan;
          currentSpan = currentSpan.next();
          tmp.remove();
        }
        else {
          // Remove first character of current span
          currentSpan.text(currentSpan.text().substring(1));
        }

        x++;
        if (x >= context.width) {
          return vtPrint(0, y + 1, data.substring(i+1), element, context, fromCaret, context.lineWrap);
        }
      }
    }

    return {
      "x": x,
      "y": y
    };
  }

  var vtInterpretHome = function(values, element, context) {
    // Cursor move
    if (values[0] == "") {
      values[0] = 1;
    }
    if (values.length < 2) {
      values[1] = "";
    }
    if (values[1] == "") {
      values[1] = 1;
    }
    if (values[0] < 1) {
      values[0] = 1;
    }
    if (values[1] < 1) {
      values[0] = 1;
    }
    vtMove(values[1] - 1, values[0] - 1, element, context);
  }

  var vtInterpretScrollSet = function(values, element, context) {
    // Scroll set
    if (values.length < 2) {
      values[0] = 1;
      values[1] = context.height;
    }

    // values[0] - start row
    // values[1] - end row
    context.scroll.start = values[0] - 1;
    context.scroll.end   = values[1] - 1;
  }

  var vtInterpretCursorSave = function(values, element, context) {
    // Save cursor position
    context.saved.x = context.x;
    context.saved.y = context.y;
  }

  var vtInterpretCursorRestore = function(values, element, context) {
    vtMove(context.saved.x, context.saved.y, element, context);
  }

  var vtInterpretQueryCursorPosition = function(values, element, context) {
    context.response += "\e[" + context.y + 1 + ";" + context.x + 1 + "R";
  }

  var vtInterpretSettingsReset = function(values, element, context) {
    // Terminal settings reset
    context.lineWrap = true;
    context.scroll.start = 0;
    context.scroll.end = context.height - 1;
  }

  var vtInterpretClearLine = function(values, element, context) {
    if (values.length == 0 || values[0] == 0) {
      // Clear until end of the line
      var blankLine = new Array(context.width - context.x + 1).join(" ");
      vtPrint(context.x, context.y, blankLine, element, context);
    }
    else if (values[0] == 1) {
      // Clear from cursor to beginning of line
      var blankLine = new Array(context.x + 1).join(" ");
      vtPrint(0, context.y, blankLine, element, context);
    }
    else if (values[0] == 2) {
      // Clear entire line
      var blankLine = new Array(context.width + 1).join(" ");
      vtPrint(0, context.y, blankLine, element, context);
    }
  }

  var vtInterpretSendDevice = function(values, element, context) {
    if (values.length == 0) {
      values[0] = 0;
    }

    if (values[0] == 0) {
      // Respond with terminal id code
      // Tell them we are a VT100, firmware patch 95, rom 0 (a don't care value)
      context.response += "\x1b>0;95;0c";
    }
  }

  var vtInterpretModeClear = function(values, element, context) {
    // xterm DEC Private Mode Set
    if (values.length == 0) {
    }
    else {
      values.forEach(function(value) {
        if (value == 7) {
          // Line wrap disable
          context.lineWrap = false;
        }
        else if (value == 12) {
          // Stop blinking cursor
          context.cursor.blinking = false;
        }
        else if (value == 25) {
          // Show Cursor
          vtHideCaret(element, context);
        }
        else if (value == 1049) {
          // Go back to normal buffer from alternative buffer
          context.buffer.mode = "normal";

          // Restore buffer
          context.buffer.saved.each(function() {
            element.append($(this).clone());
          });

          // Restore cursor
          vtMove(context.saved.x, context.saved.y, element, context);
        }
      });
    }
  }

  var vtInterpretModeSet = function(values, element, context) {
    // xterm DEC Private Mode Set
    if (values.length == 0) {
    }
    else {
      values.forEach(function(value) {
        if (value == 7) {
          // Line wrap enable
          context.lineWrap = true;
        }
        else if (value == 12) {
          // Start blinking cursor
          context.cursor.blinking = true;
        }
        else if (value == 25) {
          // Show Cursor
          vtShowCaret(element, context);
        }
        else if (value == 1049) {
          // Save buffer
          var lines = element.children('.line');
          context.buffer.saved = lines.slice(lines.length - context.height);

          // Clear buffer (interpret a 2J code)
          vtInterpretClear(false, [2], element, context);

          // Go to alternative buffer, clearing it
          context.buffer.mode = "alternate";

          // Save cursor
          context.saved.x = context.x;
          context.saved.y = context.y;
        }
      });
    }
  }

  var vtInterpretClear = function(values, element, context) {
    if (values.length == 0 || values[0] == 0) {
      // Clear from cursor to end of screen

      // Clear rest of this line
      var blankLine = new Array(context.width - context.x + 1).join(" ");
      vtPrint(context.x, context.y, blankLine, element, context);

      // Clear rest of screen
      blankLine = new Array(context.width + 1).join(" ");
      for (var i = context.y+1; i < context.height; i++) {
        vtPrint(0, i, blankLine, element, context);
      }
    }
    else if (values[0] == 1) {
      // Clear from cursor to beginning of screen

      // Clear from cursor to beginning of line
      var blankLine = new Array(context.x + 1).join(" ");
      vtPrint(0, context.y, blankLine, element, context);

      // Clear rest of screen
      blankLine = new Array(context.width + 1).join(" ");
      for (var i = 0; i < context.y; i++) {
        vtPrint(0, i, blankLine, element, context);
      }
    }
    else if (values[0] == 2) {
      // Clear screen.
      vtAddLines(context.height, element, context);
    }
  }

  var vtInterpretCursorUp = function(values, element, context) {
    if (values.length == 0) {
      values[0] = 1;
    }
    vtMove(context.x, context.y - values[0], element, context);
  }

  var vtInterpretCursorDown = function(values, element, context) {
    // Cursor down
    if (values.length == 0) {
      values[0] = 1;
    }
    vtMove(context.x, context.y + values[0], element, context);
  }

  var vtInterpretCursorForward = function(values, element, context) {
    if (values.length == 0) {
      values[0] = 1;
    }
    vtMove(context.x + values[0], context.y, element, context);
  }

  var vtInterpretCursorBack = function(values, element, context) {
    if (values.length == 0) {
      values[0] = 1;
    }
    vtMove(context.x - values[0], context.y, element, context);
  }

  var vtInterpretNextLine = function(values, element, context) {
    if (values.length == 0) {
      values[0] = 1;
    }
    vtMove(0, context.y + values[0], element, context);
  }

  var vtInterpretPreviousLine = function(values, element, context) {
    if (values.length == 0) {
      values[0] = 1;
    }
    vtMove(0, context.y - values[0], element, context);
  }

  var vtInterpretUpdateAttributes = function(values, element, context) {
    // Change color
    var new_fg_color = context.fg;
    var new_bg_color = context.bg;

    var bright = false;

    var colors = [
      "#333",              // Black
      "rgb(194, 54, 33)",  // Red
      "rgb(37, 188, 36)",  // Green
      "rgb(173, 173, 39)", // Yellow
      "rgb(73, 46, 225)",  // Blue
      "rgb(211, 56, 211)", // Magenta
      "rgb(51, 187, 200)", // Cyan
      "#ddd"               // White
    ];

    var brights = [
      "rgb(85, 85, 85)",   // Black
      "rgb(252, 57, 31)",  // Red
      "rgb(49, 231, 34)",  // Green
      "rgb(234, 236, 35)", // Yellow
      "rgb(88, 51, 255)",  // Blue
      "rgb(249, 53, 248)", // Magenta
      "rgb(20, 240, 240)", // Cyan
      "#fff"               // White
    ];

    if (values.length == 0) {
      values = [0];
    }

    // Check for bright flag
    values.forEach(function(value) {
      if (value == 1) {
        bright = true;
      }
    });

    // Go through color values and other parameters
    values.forEach(function(value) {
      if (value == 0) {
        // Return to default
        new_fg_color = colors[7];
        new_bg_color = colors[0];
      }
      else if (value >= 30 && value <= 37) {
        if (bright) {
          new_fg_color = brights[value - 30];
        }
        else {
          new_fg_color = colors[value - 30];
        }
      }
      else if (value >= 40 && value <= 47) {
        if (bright) {
          new_bg_color = brights[value - 40];
        }
        else {
          new_bg_color = colors[value - 40];
        }
      }
    });

    if (new_fg_color != context.fg || new_bg_color != context.bg) {
      context.fg = new_fg_color;
      context.bg = new_bg_color;
    }
  }

  var vtInterpretInsertLines = function(values, element, context) {
    if (values.length == 0 || values[0] == "") {
      values[0] = 1;
    }

    // Add lines to the top
    vtAddLines(values[0], element, context, context.y);

    // Remove lines from the bottom
    vtDeleteLines(values[0], element, context, context.scroll.end);
  }

  var vtInterpretCode = function(code, values, element, context) {
    if (code == "H" || code == "f") {
      vtInterpretHome(values, element, context);
    }
    else if (code == "r") {
      vtInterpretScrollSet(values, element, context);
    }
    else if (code == "s") {
      vtInterpretCursorSave(values, element, context);
    }
    else if (code == "u") {
      vtInterpretCursorRestore(values, element, context);
    }
    else if (code == "n") {
      vtInterpretQueryCursorPosition(values, element, context);
    }
    else if (code == "c") {
      vtInterpretSettingsReset(values, element, context);
    }
    else if (code == "K") {
      vtInterpretClearLine(values, element, context);
    }
    else if (code == ">c") {
      vtInterpretSendDevice(values, element, context);
    }
    else if (code == "l" || code == "?l") {
      vtInterpretModeClear(values, element, context);
    }
    else if (code == "h" || code == "?h") {
      vtInterpretModeSet(values, element, context);
    }
    else if (code == "J") { // Clear
      vtInterpretClear(values, element, context);
    }
    else if (code == "A") {
      vtInterpretCursorUp(values, element, context);
    }
    else if (code == "B") {
      vtInterpretCursorDown(values, element, context);
    }
    else if (code == "C") {
      vtInterpretCursorForward(values, element, context);
    }
    else if (code == "D") {
      vtInterpretCursorBack(values, element, context);
    }
    else if (code == "E") {
      vtInterpretNextLine(values, element, context);
    }
    else if (code == "F") {
      vtInterpretPreviousLine(values, element, context);
    }
    else if (code == "m") {
      vtInterpretUpdateAttributes(values, element, context);
    }
    else if (code == "L") {
      vtInterpretInsertLines(values, element, context);
    }
    else {
      console.log("code: " + code + " values: " + values);
    }
  };

  var vtAppend = function(data, element, context) {
    var pos = vtPrint(context.x, context.y, data, element, context, true);
    vtMove(pos.x, pos.y, element, context, true);
  };

  var vtParse = function(data, element, context) {
    context.response = "";

    // Replace escaped colors and such
    // Find each escape sequence and interpret it
    var esc_sequence_re = new RegExp("\u001b\\[([?>!])?(?:(?:(\\d*);)+)?(\\d*)([a-zA-Z])", 'g');
    var re = new RegExp("\u001b", 'g');

    var lastIndex = 0;
    while ((match = re.exec(data)) != null) {
      // Is this a single code or an escape sequence?
      var chr = data[match.index+1];
      if (chr == "=") {
        // Alternate Keypad Mode
      }
      else if (chr != '[') {
        console.log("Escape sequence: " + chr);
      }
      else if (chr == '[') {
        esc_sequence_re.lastIndex = match.index
        if ((match = esc_sequence_re.exec(data)) != null) {
          // Interpret code
          var code = match[match.length-1];

          vtAppend(data.substring(lastIndex, match.index), element, context);
          lastIndex = esc_sequence_re.lastIndex;

          values = match.slice(2,match.length-1);
          if (values[0] == undefined) {
            values = values.slice(1);
          }

          if (match[1] == undefined) {
            match[1] = "";
          }
          code = match[1] + code;

          values = values.map(function(value) {
            if (value == "") {
              return "";
            }
            else {
              return parseInt(value);
            }
          });

          vtInterpretCode(code, values, element, context);
        }
      }
    }

    vtAppend(data.substring(lastIndex), element, context);

    return context.response;
  };

  ws.onmessage = function(message) {
    var data = JSON.parse(message.data);
    var terminal = occam_ws.terminals[data.terminal];
    if (data.log !== undefined) {
      terminal.element.text(data.log);
    }
    var maxScroll = terminal.element[0].scrollHeight - terminal.element[0].clientHeight;
    var autoScroll = (terminal.element[0].scrollTop >= maxScroll - 0.5);
    var response = vtParse(data.output, terminal.element, terminal.context);
    if (response.length > 0) {
      var message = {
        "request": "write",
        "terminal": name,
        "input": response
      };
      ws.send(JSON.stringify(message));
    }
    if (autoScroll) {
      terminal.element[0].scrollTop = terminal.element[0].scrollHeight - terminal.element[0].clientHeight;
    }
  };

  ws.onopen = function(event) {
    ws.send(JSON.stringify({}));
    if (timer_id != undefined) {
      clearInterval(timer_id);
    }
    occam_ws.pending.forEach(function(internal_name) {
      var terminal = occam_ws.terminals[internal_name];
      occam_ws.openTerminal(terminal.name, terminal.internal_name, terminal.data, terminal.element);
    });
    occam_ws.pending = [];
  };

  return occam_ws;
}

function detachTerminal(occam_ws, terminal) {
  var message = {
    "request": "close"
  };
}

$(document).ready(function() {
  var query = 0;

  if ($('.information .installation span.status').text().trim() !== "finished") {
    query++;
  }

  if ($('.information .build-progress span.status').text().trim() !== "finished") {
    query++;
  }

  if ($('.information .run-progress span.status').text().trim() !== "finished") {
    query++;
  }

  $('.card.terminal').attr('aria-hidden', 'false');
  $('.terminal').attr('aria-hidden', 'false');

  query = 1;
  if (query > 0) {
    var id = setInterval(function() {
/*      $.getJSON(document.URL + "/output", function(data) {
        $('pre#install-output').text(data["install_log"]);
        $('pre#build-output').text(data["build_log"]);
        $('pre#worker-output').text(data["run_log"]);
        $('pre#run-output').text(data["output_log"]);
      });*/
    }, 500);

    // Check for websockets
    // Attempt connecting to websockets
    // Fallback on poll
    $('.card.terminal .terminal').each(function() {
    });
  }
});
