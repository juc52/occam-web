function init_expands() {
  // Tabs
  $('ul.tabs li.tab').on('click.reveal-tab', function(event) {
    // Change active tab
    $(this).parent().children('.active').removeClass('active');
    $(this).addClass('active');

    // Hide tab
    $(this).parent().parent().parent().find('.tab-panels').first().children('.tab-panel.active').removeClass('active').attr('aria-hidden', 'true');

    // Reveal tab
    $(this).parent().parent().parent().find('.tab-panels').first().children('.tab-panel:eq(' + $(this).index() + ')').addClass('active').attr('aria-hidden', 'false').trigger('resize');
  });

  // Displays values only when enabled
  $('.configuration-group li > .select > select').on('change keyup', function(e) {
    var enablesCount = parseInt($(this).data("enables-count"));
    for (var i = 0; i < enablesCount; i++) {
      var key = $(this).data("enables-key-" + i);
      var is = $(this).data("enables-is-" + i);
      var input = $(this).children("option:selected").text();
      if(is === input) {
        $('.configuration-group li[data-key='+key+'] input').prop('disabled', false);
      }
      else {
        $('.configuration-group li[data-key='+key+'] input').prop('disabled', true);
      }
    }
    var disablesCount = parseInt($(this).data("disables-count"));
    for (var i = 0; i < disablesCount; i++) {
      var key = $(this).data("disables-key-" + i);
      var is = $(this).data("disables-is-" + i);
      var input = $(this).children("option:selected").text();
      if(is === input) {
        $('.configuration-group li[data-key='+key+'] input').prop('disabled', true);
      }
      else {
        $('.configuration-group li[data-key='+key+'] input').prop('disabled', false);
      }
    }
    var showsCount = parseInt($(this).data("shows-count"));
    for (var i = 0; i < showsCount; i++) {
      var key = $(this).data("shows-key-" + i);
      var is = $(this).data("shows-is-" + i);
      var input = $(this).children("option:selected").text();
      if(is === input) {
        $('.configuration-group li[data-key='+key+']').css({
          display:''
        });
      }
      else {
        $('.configuration-group li[data-key='+key+']').css({
          display:'none'
        });
      }
    }
    var hidesCount = parseInt($(this).data("hides-count"));
    for (var i = 0; i < hidesCount; i++) {
      var key = $(this).data("hides-key-" + i);
      var is = $(this).data("hides-is-" + i);
      var input = $(this).children("option:selected").text();
      if(is === input) {
        $('.configuration-group li[data-key='+key+']').css({
          display:'none'
        });
      }
      else {
        $('.configuration-group li[data-key='+key+']').css({
          display:''
        });
      }
    }
  }).trigger('change');

  // Set up div expands
  $('.configuration-group li:not(.field) > label').on('click', function(e) {
    $(this).parent().children('span.expand').trigger('click');
  }).css({
    cursor: 'pointer'
  });

  $('.configuration-group li:not(.field) > span.expand').on('click', function(e) {
    $(this).toggleClass('shown');
    // Get associated description div
    if ($(this).hasClass('shown')) {
      $(this).parent().find('.description').slideDown(150);
      $(this).parent().find('.parameters').slideDown(150);
      $(this).text("[ - ]");
    }
    else {
      $(this).parent().find('.description').slideUp(150);
      $(this).parent().find('.parameters').slideUp(150);
      $(this).text("[+]");
    }
  }).css({
    cursor: 'pointer'
  });

  $('.configuration-group h2').on('click', function(e) {
    var span = $(this).children('span.expand');
    span.toggleClass('shown');
    // Get associated description div
    if (span.hasClass('shown')) {
      span.parent().parent().children('ul').slideDown(150);
      span.text("\u25be");
    }
    else {
      span.parent().parent().children('ul').slideUp(150);
      span.text("\u25b8");
    }
  }).css({
    cursor: 'pointer'
  });

  $('.configuration-group h2 > span.expand').on('click', function(e) {
  }).css({
    cursor: 'pointer'
  });

  // Collapse all but the first group
  $('ul.configuration-group > li:not(:first-child) > ul.configuration-group').each(function(e) {
    $(this).parent().children('h2').children('span.expand').trigger('click');
  });

  // collapse all / expand all
  $('ul.header_bar').css({display: 'block'});
  $('ul.header_bar #collapse_all_link').on('click', function(e) {
    $('ul.configuration-group h2 > span.expand').each(function(e) {
      if ($(this).hasClass('shown')) {
        $(this).toggleClass('shown');
        $(this).parent().parent().children('ul').css({
          display: 'none'
        });
        $(this).text("\u25b8");
      }
    });
  });
  $('ul.header_bar #expand_all_link').on('click', function(e) {
    $('ul.configuration-group h2 > span.expand').each(function(e) {
      if (!($(this).hasClass('shown'))) {
        $(this).toggleClass('shown');
        $(this).parent().parent().children('ul').css({
          display: 'block'
        });
        $(this).text("\u25be");
      }
    });
  });
}

// Autocomplete for experiment tags
$(function(){
  var tag_cache = {};
  $('input.tagged').tagit({
    allowSpaces: true,
    singleField: true,
    singleFieldDelimiter: ';'
  });
  $('input.tagged.autocomplete').tagit({
    singleField: true,
    singleFieldDelimiter: ';',
    autocomplete: {
      delay: 0,
      minLength: 2,
      source: function(request, response) {
        var term = request.term;
        if (term in tag_cache) {
          response(tag_cache[term]);
          return;
        }

        $.getJSON($('input.tagged.autocomplete').data("source"), {term: term}, function(data, status, xhr) {
          tag_cache[term] = data;
          response(data);
          return;
        });
      },
    }
  });
  $('#search').searchlight('/search', {
    showIcons: false,
    align: 'left'
  });

  // Configuration expands
  init_expands();

  $('.configuration-group > li > .recipe > select').on('change', function(e) {
    var selected = $(this).children(':selected');
    var ul_group = $(this).parent().parent().children('ul.configuration-group');
    var expand = $(this).parent().parent().children('h2').children('span.expand');
    var nesting = ul_group.data('nesting');

    // Expand group
    if (!expand.hasClass('shown')) {
      expand.trigger('click');
    }

    // Ask for the config options
    jQuery.getJSON(selected.data('template'), function(data, status, xhr) {
      // Set all input fields
      $.each(data, function(key, value) {
        code = $.base64.encode(key);
        // Replace + / with - _
        code = code.replace('+', '-').replace('/', '_');
        // Remove padding
        while (code.charAt(code.length-1) == '=') {
          code = code.slice(0, code.length-1);
        }
        code = nesting + '[' + code + ']';

        $('*[name="'+code+'"]').each(function(e) {
          $(this).val(value);
        });
      });
    });
  });

  // Help bubble expansion
  $('a.help-bubble').on('click', function(event) {
    // Discover the help bubble on the page and toggle it
    var bubble_div = $(this).parent().next();

    if (bubble_div.attr('aria-hidden') == "true") {
      bubble_div.slideDown(150, function() {
        bubble_div.attr('aria-hidden', bubble_div.attr('aria-hidden') == "true" ? "false" : "true");
      });
    }
    else {
      bubble_div.slideUp(150, function() {
        bubble_div.attr('aria-hidden', bubble_div.attr('aria-hidden') == "true" ? "false" : "true");
      });
    }
    event.stopPropagation();
    event.preventDefault();
  });

  // Add 'delete' button to help bubbles
  $('.help').append("<div class='delete'></div>");

  // Help 'delete' button should close the help bubble
  $('.help .delete').on('click', function(event) {
    $(this).parent().prev().children('a.help-bubble').trigger('click');
  })
  // Help 'delete' button animation
  .on('mouseenter', function(event) {
    $(this).animate({
      "background-color": "hsl(0, 50%, 50%)",
      "border-color":     "hsl(0, 20%, 50%)"
    }, 200);
  })
  .on('mouseout', function(event) {
    $(this).stop(true).animate({
      "background-color": "hsl(0, 50%, 80%)",
      "border-color":     "hsl(0, 20%, 90%)"
    }, 200);
  });

  // add prettyprint class to all <pre><code></code></pre> blocks
  var prettify = false;
  $("pre code").parent().each(function() {
    $(this).addClass('prettyprint');
    prettify = true;
  });

  // if code blocks were found, bring in the prettifier ...
  if ( prettify ) {
    $.getScript("/js/prettify.js", function() { prettyPrint() });
  }

  // Group listing dynamic content

  // Add group lists
  $('ul.groups li.group').append($('<ul></ul>').addClass('groups').attr('aria-hidden', 'true'));

  // Add triangle
  $('ul.groups li.group span.name').parent().prepend($('<span>\u25b8</span>').addClass('expand').css({
    "background": "none",
    "font-size": "20px",
    "color": "#555",
    "cursor": "pointer",
    "margin-left": "10px"
  }).on('click', function(event) {
    var span = $(this);
    span.toggleClass('shown');
    // Get associated description div
    var group = span.parent().parent().parent();
    var group_list = group.children('ul');
    if (span.hasClass('shown')) {
      // Pull in group list, if there are no children
      if (group_list.children('li').length == 0) {
        var group_id   = group.data('group-id');
        var workset_id = group.data('workset-id');

        $.getJSON('/worksets/' + workset_id + '/groups/' + group_id, function(data) {
          data.experiments.forEach(function(experiment) {
            var item = group.clone();
            item.find('span.expand').remove();
            item.find('span.units').remove();
            item.find('span.value').remove();
            item.removeClass('group').addClass('experiment');
            item.children('ul').remove();

            item.find('span.name a').text(experiment.name).attr('href', '/experiments/' + experiment.uid);
            item.find('span.value').text("0");

            group_list.append(item);
          });
        });
      }
      group_list.slideDown(150);
      group_list.attr('aria-hidden', 'false');
      span.text("\u25be");
    }
    else {
      group_list.slideUp(150);
      group_list.attr('aria-hidden', 'true');
      span.text("\u25b8");
    }
    event.stopPropagation();
    event.preventDefault();
  }));

  /* Handle collapsable cards */
  $('.card.collapsable').each(function() {
    // Put children into a separate container
    var container = $('<div></div>').addClass("container").attr('aria-hidden', 'false');
    $(this).children().each(function() {
      if ($(this)[0].tagName.toLowerCase() != "h2" && !$(this).hasClass("help")) {
        container.append($(this));
      }
    });
    $(this).append(container);
    $(this).children('h2').css({
      "cursor": "pointer"
    }).prepend($('<span>\u25be</span>').addClass('expand shown').css({
    })).on('click', function(event) {
      var span = $(this).children('span.expand');
      span.toggleClass('shown');
      // Get associated description div
      var card = span.parent().parent();
      card.toggleClass('collapsed');

      var container = card.children('.container');

      if (span.hasClass('shown')) {
        container.slideDown(250, function() {
          container.attr('aria-hidden', container.attr('aria-hidden') == "true" ? "false" : "true");
        });
        span.text("\u25be");
      }
      else {
        container.slideUp(250, function() {
          container.attr('aria-hidden', container.attr('aria-hidden') == "true" ? "false" : "true");
        });
        span.text("\u25b8");
      }

      event.stopPropagation();
      event.preventDefault();
    });
    if ($(this).hasClass('collapsed')) {
      $(this).children('h2').trigger('click');
    }
  });
});
