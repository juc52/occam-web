$(function() {
  var ws = null;
  if (ws === null) {
    ws = startWebsocket();
  }

  var elements = $('.card.terminal .terminal');

  elements.each(function() {
    var element = $(this);
    if (element.data('type') == "logs") {
      var job_id = element.data('job-id');

      ws.attachTerminal("logs", {
        "job_id": job_id
      }, element);
    }
    else {
      var experiment_id       = element.data("experiment-id");
      var experiment_revision = element.data("experiment-revision");
      var workset_id          = element.data("workset-id");
      var workset_revision    = element.data("workset-revision");

      ws.attachTerminal("run", {
        "experiment_id":        experiment_id,
        "experiment_revision":  experiment_revision,
        "workset_id":           workset_id,
        "workset_revision":     workset_revision
      }, element);
    }
  });
});
