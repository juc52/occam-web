/* This code handles workflow editing.
 */

$(function() {
  /* Remove buttons for non-javascript version */
  $('#workflow .input select.inputs + input.button').css({
    display: 'none'
  });

  /* Add workflow boxes */
  var workflow = $('#workflow');

  var active_box = $('<div class="container embedded"><div class="container-content"></div></div>').css({
    "position": "absolute",
    "left": 0,
    "top": 0,
    "visibility": "hidden",
    "z-index": "1111"
  });
  workflow.append(active_box);

  /* Squish workflow */
  $('#workflow').addClass('fitted');

  var reveal_active_box = function(e) {
    /* Move active box to where this container is */
    var inputFlag = $(this).parent().hasClass('input');
    var position = $(this).offset();
    var workflow_offset = workflow.offset();

    var x = position.left - workflow_offset.left;
    var y = position.top  - workflow_offset.top + workflow.scrollTop();

    var new_active_box = active_box.clone();

    new_active_box.empty();
    new_active_box.append($(this).clone());
    new_active_box.find('.label').remove();

    new_active_box.attr('data-left', x);
    new_active_box.attr('data-top', y);

    if (inputFlag) {
      new_active_box.addClass('input');
    }

    /* Embedded container fits its contents */
    new_active_box.children('.container-content').css({
      "left": "0px",
      "top": "0px"
    });

    new_active_box.children('.container-content').children().css({
      "visibility": "visible",
      "opacity":    0.0
    });

    new_active_box.css({
      "width": "30px",
      "height": "30px",
      "left": x-1,
      "top":  y-1,
      "visibility": "visible"
    });

    /* Reuse border color */
    new_active_box.children(".container-content").css({
      "border-color": $(this).css("border-left-color")
    });

    // Expand workflow nodes when the mouse enters, collapse when they leave:
    new_active_box.on('mouseenter', function(event) {
      // Ensure that input boxes stay up and aren't reset:
      if ($(this).hasClass('input') && $(this).hasClass('opened')) {
        return;
      }

      // Mark this container as having 'opened'
      $(this).addClass('opened');

      // Determine box location after expansion:
      var x = parseFloat(new_active_box.attr('data-left'));
      var y = parseFloat(new_active_box.attr('data-top'));

      new_active_box.stop(true).animate({
        "left": x-46,
        "top":  y-46,
      }, 200);

      // Expand to this size:
      new_active_box.children('.container-content').stop(true).animate({
        "width": "120px",
        "height": "120px",
        "opacity": 1.0
      }, 200);

      // Ensure that children are eventually visible (because they look
      // rather silly as they are expanding and as they fill the space wrt
      // css overflow)
      new_active_box.children('.container-content').children().stop(true).animate({
        "opacity": 1.0
      }, 200);
    })
    .on('mouseleave', function(event) {
      // Do not collapse (green border) input containers
      if ($(this).hasClass('input')) {
        return;
      }

      // Determine old location and animate:
      var x = parseFloat(new_active_box.attr('data-left'));
      var y = parseFloat(new_active_box.attr('data-top'));

      $(this).stop(true).animate({
        "left": x-1,
        "top":  y-1,
      }, 200, "swing", function() {
        new_active_box.remove();
      });

      // Shrink to this size:
      $(this).children('.container-content').stop(true).animate({
        "width": "30px",
        "height": "30px",
      }, 200);

      // Slowly hide children because, like before, they look silly as they
      // shrink down due to overflow oddness.
      $(this).children('.container-content').children().stop(true).animate({
        "opacity": 0.0
      }, 200, "swing", function() {
        // Remove the box when this is done
        new_active_box.remove();
      });
    });

    var object_type_selector = new_active_box.find('form select.inputs');

    /* Object type dropdown */
    object_type_selector.on('change', function() {
      var selected = $(this).children(':selected');

      /* Fill out the object selection dropdown with appropriate objects that
       * are of the specified type.
       */
      var object_id  = $(this).data('object-id');
      var thiz = $(this);
      var input_type = selected.attr('value');

      if (object_id === undefined) {
        // This is the global object. Grab ALL object types and inputs.
        var url = '/objects?by_type=' + input_type;

        $.getJSON(url, function(data) {
          var object_selector = thiz.parent().children("select.objects");
          object_selector.children().remove();
          data.forEach(function(object) {
            var option = $('<option></option>');
            option.text(object["name"]);
            option.attr('value', object["id"]);
            option.data('object-id', object["id"]);
            object_selector.append(option);
          });
        });
      }
      else {
        var url = '/objects/' + object_id + '/inputs?by_type=' + input_type;

        $.getJSON(url, function(data) {
          var object_selector = thiz.parent().children("select.objects");
          object_selector.children().remove();
          data.forEach(function(input) {
            input["objects"].forEach(function(object) {
              var option = $('<option></option>');
              option.text(object["name"]);
              option.attr('value', object["id"]);
              option.data('object-id', object["id"]);
              object_selector.append(option);
            });

            input["indirect_objects"].forEach(function(object) {
              var option = $('<option></option>');
              option.text(object["name"]);
              option.attr('value', object["id"]);
              option.data('object-id', object["id"]);
              object_selector.append(option);
            });
          });
        });
      }
    });

    workflow.append(new_active_box);
  };

  $('#workflow .container:not(.embedded):not(.created) .container-content').on('mouseenter', reveal_active_box);

  /* Remove input boxes */
  var input_containers = [];
  $('#workflow li.input').each(function() {
    /* Retain input container */
    $(this).find('.container').css({
      "height": "0px"
    });
    input_containers[input_containers.length] = {
      "container": $(this),
      "parent":    $(this).parent()
    };

    /* Remove input container from DOM */
    $(this).remove();
  });

  /* Show labels */
  $('#workflow .container:not(.embedded) .container-content .label').css({
    "display": "inline-block"
  });

  /* Add input button to re-add input container to DOM */
  input_containers.forEach(function(e, i) {
    var add_button = $('<div></div>');
    var last_container = e.parent.children("li:last-child").children('.row').children('.info').children('.container');
    var position = last_container.offset();
    var workflow_offset = workflow.offset();
    var appending = true;
    var x = 0;
    var y = 0;
    var bottom = 0;
    if (last_container.length == 0) {
      /* This is adding the first input for a node */
      /* In this case, we want a (+) bubble directly to the
       * left of the existing node. */
      last_container = e.parent;
      position = e.parent.offset();
      x = position.left - workflow_offset.left + 39;
      y = position.top  - workflow_offset.top;

      bottom = (e.parent.height() - 15) / 2;

      appending = false;
    }
    else {
      x = position.left - workflow_offset.left + 39;
      y = position.top  - workflow_offset.top  + 77;
      bottom = 8;
    }
    add_button.css({
      "width": "15px",
      "height": "15px",
      "color": "white",
      "overflow": "hidden",
      "position": "absolute",
      "vertical-align": "middle",
      "line-height": "15px",
      "font-size": "12px",
      "font-weight": "700",
      "background-color": "hsl(120, 50%, 80%)",
      "border-radius": "8px",
      "margin": "0",
      "text-align": "center",
      "right": 45,
      "bottom": bottom,
      "cursor": "pointer",
      "z-index": "888",
    }).on('mouseenter', function(e) {
      $(this).animate({
        "background-color": "hsl(120, 50%, 50%)"
      }, 200);
      $(this).parent().children('.input-dashes[data-input-index=' + $(this).data("input-index") + ']').stop(true).animate({
        "border-color": "hsl(120, 50%, 50%)"
      }, 200);
    }).on('mouseout', function(event) {
      $(this).stop(true).animate({
        "background-color": "hsl(120, 50%, 80%)"
      }, 200);
      $(this).parent().children('.input-dashes[data-input-index=' + $(this).data("input-index") + ']').stop(true).animate({
        "border-color": "hsl(180, 40%, 90%)"
      }, 200);
    }).one('click', function(event) {
      /* Add input container element to line DOM */
      $(this).unbind('mouseout mouseenter click');
      $(this).parent().children('.input-dashes[data-input-index=' + $(this).data("input-index") + ']').stop(true).animate({
        "opacity": "0.0"
      }, 200, "swing", function() {
        $(this).remove();
      });
      $(this).stop(true).animate({
        "opacity": "0.0"
      }, 200, "swing", function() {
        $(this).remove();
      });

      e.parent.append(e.container);

      e.container.find('.container').animate({
        "height": "100px"
      }, 200);

      e.container.find('.container-content').on('mouseenter', reveal_active_box);
    });
    var add_button_dashes = $('<div></div>').css({
      "width": "33px",
      "height": "32px",
      "color": "white",
      "position": "absolute",
      "font-weight": "700",
      "margin": "0",
      "right": 0,
      "z-index": "888",
    });
    if (!appending) {
      add_button_dashes.css({
        "border-top": "1px solid hsl(180, 40%, 90%)",
        "bottom": 16,
      });
    }
    else {
      add_button_dashes.css({
        "bottom": 14,
        "border-right": "5px solid hsl(180, 40%, 90%)",
        "border-bottom": "1px solid hsl(180, 40%, 90%)",
        "border-bottom-right-radius": "10px",
      });
    }
    add_button.addClass("input-button");
    add_button_dashes.addClass("input-dashes");
    add_button.attr("data-input-index", i);
    add_button_dashes.attr("data-input-index", i);
    add_button.text("+");
    last_container.append(add_button);
    last_container.append(add_button_dashes);
  });

  /* Allow resizing of the workflow area */
  $('#workflow').css({
    "resize": "vertical",
    "height": $('#workflow').height() + "px",
    "max-height": "none"
  });

});
