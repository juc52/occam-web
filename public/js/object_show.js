$(function() {
  var ws = null;
  if (ws === null) {
    ws = startWebsocket();
  }

  var element = $('.card.terminal .terminal#build-terminal');

  var object_id       = element.data('object-id');
  var object_revision = element.data('object-revision');

  if (element.length > 0) {
    ws.attachTerminal("build", {
      "object_id":       object_id,
      "object_revision": object_revision
    }, element);
  }

  var element = $('.card.terminal .terminal#console-terminal');

  var object_id       = element.data('object-id');
  var object_revision = element.data('object-revision');

  if (element.length > 0) {
    ws.attachTerminal("console", {
      "object_id":       object_id,
      "object_revision": object_revision
    }, element);
  }
});
