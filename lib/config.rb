class Occam
  module Config
    @@config = nil
    @@prepared_config = nil
    def self.configuration(env=nil)
      if env.nil?
        env = ENV["RACK_ENV"]
      end

      if env.nil?
        env = "development"
      end

      if @@config.nil?
        occam_path = File.join(ENV['HOME'], '.occam')
        config_file_path = File.join(occam_path, 'config.yml')
        unless File.exists?(config_file_path)
          raise "OCCAM not initialized"
        end

        @@config = YAML.load_file(config_file_path) || {}
      end

      if @@prepared_config.nil?
        @@prepared_config = {}
      end

      if not @@prepared_config.has_key? env.to_s
        config = @@config[env.to_s] || {}

        occam_path = File.join(ENV['HOME'], '.occam')

        # Default password scheme
        config['passwords'] ||= {}
        passwords = config['passwords']

        passwords['scheme'] ||= "bcrypt"
        passwords['rounds'] ||= 20

        # Default paths
        config['paths'] ||= {}
        paths = config['paths']

        paths['objects'] ||= File.join(occam_path, "objects")
        paths['git']     ||= File.join(occam_path, "git")
        paths['hg']      ||= File.join(occam_path, "hg")
        paths['svn']     ||= File.join(occam_path, "svn")
        paths['builds']  ||= File.join(occam_path, "builds")
        paths['store']   ||= File.join(occam_path, "store")
        paths['jobs']    ||= File.join(occam_path, "jobs")

        # Default queue settings
        config['queues'] ||= {}
        queues = config['queues']

        queues['task'] ||= {}
        queues['task']['dispatch'] ||= "local"

        queues['command'] ||= {}
        queues['command']['dispatch'] ||= "local"

        @@prepared_config[env.to_s] = config
      end

      @@prepared_config[env.to_s]
    end
  end
end
