class Occam < Sinatra::Base
  class WebSocketBackend
    require 'json'
    require 'open3'
    require 'tmpdir'
    require 'fileutils'
    require 'pty'

    KEEPALIVE_TIME = 15 # in seconds

    def initialize(app)
      @app       = app
      @clients   = []
      @terminals = {}
    end

    # For 'thin' servers, we need an adapter
    Faye::WebSocket.load_adapter('thin')

    def call(env)
      if Faye::WebSocket.websocket?(env)
        # WebSockets logic goes here
        ws = Faye::WebSocket.new(env, nil, {
          ping: KEEPALIVE_TIME
        })

        ws.on :error do |event|
          puts "Error"
        end

        ws.on :open do |event|
          @clients << ws
        end

        ws.on :message do |event|
          data = JSON.parse(event.data)

          request = data["request"]
          terminal = data["terminal"]

          case request
          when "write"
            # Input writes to the open session
            begin
              @terminals[data["terminal"]][:input].write data["input"]
            rescue
            end
          when "open"
            object_type = data["objectType"]
            object_name = data["objectName"]
            if data.has_key? "spawning"
              case data["spawning"]
              when "build"
                rows = data["rows"]
                cols = data["cols"]
                object_id       = data["data"]["object_id"]
                object_revision = data["data"]["object_revision"]
                command = "sh -c 'stty rows #{rows}; stty cols #{cols}; occam build --to #{object_id} --revision #{object_revision}'"

                # Create a response thread I guess
                @terminals[data["terminal"]] = {}
                @terminals[data["terminal"]][:thread] = Thread.new do
                  PTY.spawn(command) do |output, input, pid|
                    @terminals[data["terminal"]][:input] = input

                    until output.eof? and error.eof?
                      IO.select([output])

                      no_output = false
                      begin
                        output_string = output.read_nonblock(1024)
                        p output_string
                      rescue
                        output_string = ""
                      end

                      ws.send({
                        :response => "data",
                        :output   => output_string,
                        :terminal => data["terminal"],
                      }.to_json)
                    end
                  end
                  puts "Done"
                end
              when "logs"
                puts "logs"
                puts data["data"]
                rows = data["rows"]
                cols = data["cols"]

                data["data"] = data["data"] || {}
                job_id = data["data"]["job_id"]

                # TODO: sanitize (only allow valid uuids and valid revisions) (could base64 the arguments)
                command = "sh -c 'stty rows #{rows}; stty cols #{cols}; occam logs --job #{job_id} --tail'"

                # Create a response thread I guess
                @terminals[data["terminal"]] = {}
                @terminals[data["terminal"]][:thread] = Thread.new do
                  PTY.spawn(command) do |output, input, pid|
                    @terminals[data["terminal"]][:input] = input

                    until output.eof? and error.eof?
                      IO.select([output])

                      no_output = false
                      begin
                        output_string = output.read_nonblock(1024)
                        p output_string
                      rescue
                        output_string = ""
                      end

                      ws.send({
                        :response => "data",
                        :output   => output_string,
                        :terminal => data["terminal"],
                      }.to_json)
                    end
                  end
                  puts "Done"
                end
              when "run"
                puts "run"
                puts data["data"]
                rows = data["rows"]
                cols = data["cols"]

                data["data"] = data["data"] || {}
                experiment_id = data["data"]["experiment_id"]
                experiment_revision = data["data"]["experiment_revision"]
                workset_id = data["data"]["workset_id"]
                workset_revision= data["data"]["workset_revision"]

                # TODO: sanitize (only allow valid uuids and valid revisions)
                command = "sh -c 'stty rows #{rows}; stty cols #{cols}; occam run --to #{experiment_id} --revision #{experiment_revision} --within #{workset_id} --within-revision #{workset_revision}'"

                # Create a response thread I guess
                @terminals[data["terminal"]] = {}
                @terminals[data["terminal"]][:thread] = Thread.new do
                  PTY.spawn(command) do |output, input, pid|
                    @terminals[data["terminal"]][:input] = input

                    until output.eof? and error.eof?
                      IO.select([output])

                      no_output = false
                      begin
                        output_string = output.read_nonblock(1024)
                        p output_string
                      rescue
                        output_string = ""
                      end

                      ws.send({
                        :response => "data",
                        :output   => output_string,
                        :terminal => data["terminal"],
                      }.to_json)
                    end
                  end
                  puts "Done"
                end
              when "console"
                rows = data["rows"]
                cols = data["cols"]
                object_id       = data["data"]["object_id"]
                object_revision = data["data"]["object_revision"]
                command = "sh -c 'stty rows #{rows}; stty cols #{cols}; occam console --to #{object_id} --revision #{object_revision}'"

                # Create a response thread I guess
                @terminals[data["terminal"]] = {}
                @terminals[data["terminal"]][:thread] = Thread.new do
                  PTY.spawn(command) do |output, input, pid|
                    @terminals[data["terminal"]][:input] = input

                    until output.eof? and error.eof?
                      IO.select([output])

                      no_output = false
                      begin
                        output_string = output.read_nonblock(1024)
                        p output_string
                      rescue
                        output_string = ""
                      end

                      ws.send({
                        :response => "data",
                        :output   => output_string,
                        :terminal => data["terminal"],
                      }.to_json)
                    end
                  end
                  puts "Done"
                end
              else
                command = ""
              end
            end
          end
        end

        ws.on :close do |event|
          @clients.delete(ws)
          ws = nil
        end

        # Return async Rack response
        ws.rack_response
      else
        @app.call(env)
      end
    end
  end
end
