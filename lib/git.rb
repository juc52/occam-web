class Occam
  require 'grit'
  require 'json'

  class Git
    def initialize(path, revision="HEAD")
      require 'tmpdir'
      require 'fileutils'
      require 'open3'

      @revision = revision
      @path = path
      @tmp  = false

      # Is this a URL?
      if path.match /^[a-z]+:\/\//
        tmpdir = Dir.mktmpdir("occam-")
        @tmp = true
        @path = tmpdir

        # Clone git repository
        Open3.popen3('git', 'clone', path, '.', :chdir => tmpdir) do |i, o, e, t|
        end

        # Define destructor
        ObjectSpace.define_finalizer(self, proc do
          FileUtils.remove_entry_secure tmpdir
        end)
      end
    end

    def self.fullRevision(path, revision="HEAD")
      Open3.popen3('git', 'rev-parse', revision, :chdir => path) do |i, o, e, t|
        ret = o.read.strip

        errorCode = t.value
        if errorCode != 0
          return nil
        end

        return ret
      end
    end

    def self.hasRevision(path, revision="HEAD")
      Git.fullRevision(path, revision) != false
    end

    def head()
      Git.fullRevision(@path, "HEAD")
    end

    def parent(revision="HEAD")
      Git.fullRevision(@path, revision + "^")
    end

    def children(revision="HEAD")
      revision = Git.fullRevision(@path, revision)

      Open3.popen3('git', 'rev-list', '--all', '--parents', "^" + revision, :chdir => @path) do |i, o, e, t|
        ret = []

        hashes = o.read.lines.last
        if hashes
          hashes = hashes.split(' ').first
          ret << hashes
        end

        errorCode = t.value
        if errorCode != 0
          return []
        end

        return ret
      end
    end

    def retrieveJSON(filepath)
      Open3.popen2('git', 'show', "#{@revision}:#{filepath}", :chdir => @path) do |i, o, t|
        i.close

        ret = JSON.parse(o.read)

        errorCode = t.value
        if errorCode != 0
          return {}
        end

        return ret
      end

      {}
    end

    def retrieveFile(filepath)
      Open3.popen2('git', 'show', "#{@revision}:#{filepath}", :chdir => @path) do |i, o, t|
        i.close

        ret = o.read

        errorCode = t.value
        if errorCode != 0
          return false
        end

        return ret
      end

      ""
    end

    def objectInfo
      self.retrieveJSON('object.json')
    end

    def name
      self.objectInfo['name'] || ""
    end

    def type
      self.objectInfo['type'] || ""
    end

    def authors
      self.objectInfo['authors'] || []
    end

    def description
      self.objectInfo['description'] || ""
    end

    def configurations
      self.objectInfo['configurations'] || []
    end

    def outputs
      self.objectInfo['outputs'] || []
    end

    def inputs
      self.objectInfo['inputs'] || []
    end

    def installInfo
      ret = self.objectInfo['install'] || []

      if not ret.is_a? Array
        ret = [ret]
      end

      ret
    end

    def buildInfo
      self.objectInfo['build'] || {}
    end

    def runInfo
      self.objectInfo['run'] || {}
    end

    # Clones the given git repository and executes the given
    # block. The block is passed the name of the temporary
    # path of the repo contents. This directory is destroyed
    # after the call.
    def self.import_scripts(path, &blk)
      # Create a temporary directory.
      # Clone the repo given into that directory.
      # This directory will automatically be destroyed.
      Dir.mktmpdir do |dir|
        repo = Grit::Git.new(dir)
        repo.clone({}, path, dir)

        yield(dir)
      end
    end

    # Read the OCCAM Object description JSON from the given
    # git repository. Returns a Hash of the description data
    # found there.
    def self.description(path)
      Occam::Git.import_scripts(path) do |dir|
        begin
          description = JSON.parse(IO.read("#{dir}/object.json"))
        rescue
          description = {}
        end

        description
      end
    end
  end
end
