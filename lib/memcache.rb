class Occam
  module Memcache
    require 'json'

    def self.client
      # TODO: Maybe do this by the thread id since it
      # isn't entirely threadsafe: https://github.com/evan/memcached#threading
      @client ||= Dalli::Client.new("localhost:11211")
    end

    def self.getJSON(key)
      begin
        JSON.parse(self.get(key))
      rescue
        nil
      end
    end

    def self.get(key)
      begin
        self.client.get(key)
      rescue
        nil
      end
    end
  end
end
