class Occam
  # This module
  module Backend
    def self.connected
      not self.config.empty?
    end

    def self.parse_configuration
      config = Occam::Config.configuration['backend'] || {}

      if not config.empty?
        config['port'] ||= 80
        config['scheme'] ||= 'http'
      end

      config
    end

    def self.config
      @backend_config ||= self.parse_configuration
    end

    def self.fullRevision(uuid, revision)
      object_type = Occam::Object.objectTypeFromUUID(uuid)
      data = self.downloadJSON("/objects/#{object_type}/#{uuid}/#{revision}/full_revision")

      if data
        data["revision"]
      else
        data
      end
    end

    def self.parentRevision(uuid, revision)
      object_type = Occam::Object.objectTypeFromUUID(uuid)
      data = self.downloadJSON("/objects/#{object_type}/#{uuid}/#{revision}/parent_revision")

      if data
        data["parentRevision"]
      else
        data
      end
    end

    def self.childRevisions(uuid, revision)
      object_type = Occam::Object.objectTypeFromUUID(uuid)
      data = self.downloadJSON("/objects/#{object_type}/#{uuid}/#{revision}/child_revisions")

      if data
        data["childRevisions"]
      else
        data
      end
    end

    def self.retrieveJSON(uuid, revision, path)
      if self.connected
        object_type = Occam::Object.objectTypeFromUUID(uuid)
        self.downloadJSON("/objects/#{object_type}/#{uuid}/#{revision}/tree/#{path}")
      else
        false
      end
    end

    def self.retrieveFile(uuid, revision, path)
      if self.connected
        object_type = Occam::Object.objectTypeFromUUID(uuid)
        data = self.download("/objects/#{object_type}/#{uuid}/#{revision}/tree/#{path}")
        data
      else
        false
      end
    end

    def self.downloadJSON(path)
      if self.connected
        data = self.download(path)
        if data == false
          false
        else
          JSON.parse(data)
        end
      else
        false
      end
    end

    def self.download(path, content_type = 'text/plain', limit = 10)
      request = Net::HTTP::Get.new(path)
      request['Accept'] = content_type
      request.content_type = content_type

      http = Net::HTTP.new(self.config['host'], self.config['port'])
      if self.config['scheme'].downcase == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end

      response = http.request(request)

      # Did we get a response? (allow redirects)
      if response.is_a?(Net::HTTPRedirection) && limit > 0
        location = response['location']
        uri = URI(location)
        if uri.host == self.config['host'] && uri.port == self.config['port'] && uri.scheme == self.config['scheme']
          location = uri.request_uri
          Occam::Backend.download(location, content_type, limit - 1)
        else
          # Do not allow a host/port change redirect
          false
        end
      elsif response.is_a?(Net::HTTPSuccess)
        response.body
      else # Otherwise, error out
        false
      end
    end
  end
end
