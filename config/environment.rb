require 'sinatra'
require 'sinatra/activerecord'

class Occam < Sinatra::Base
  register Sinatra::ActiveRecordExtension

  # Load configuration
  require_relative '../lib/config'

  config = Occam::Config.configuration

  if config["document_store"]["adapter"] == 'mongodb'
    require 'mongo_mapper'
  end

  if config["database"]["adapter"] == 'postgres'
    require 'pg'
  elsif config["database"]["adapter"] == 'sqlite3'
    require 'sqlite3'
  else
    throw "Unknown database adapter '#{config["database"]["adapter"]}'"
  end

  require_relative '../config/application'

  [:test, :development, :production].each do |env|
    configure env do
      # Set up BCRYPT rounds
      config = Occam::Config.configuration(env)
      if config['passwords']
        if config['passwords']['scheme'] == 'bcrypt'
          if config['passwords']['rounds']
            Occam.send(:remove_const, :BCRYPT_ROUNDS)
            Occam.const_set(:BCRYPT_ROUNDS, config['passwords']['rounds'])
          end
        end
      end

      # Set up database
      database_opts = config["database"]

      options = {}

      if database_opts["uri"]
        db = URI.parse(database_opts["uri"])
        options[:adapter]  = db.scheme
        options[:host]     = db.host
        options[:user]     = db.user
        options[:password] = db.password
        options[:database] = db.path[1..-1]
      end

      options[:adapter]  ||= database_opts["adapter"]
      options[:database] ||= database_opts["database"]
      options[:timeout]  ||= database_opts["timeout"]

      # Explicitly set encoding
      options[:encoding] = 'utf8'

      # Coerse adapter types for ActiveRecord use
      options[:adapter] = "postgresql" if options[:adapter] == 'postgres'

      # Default database location for sqlite3
      occam_path = File.join(ENV['HOME'], '.occam')
      if options[:adapter] == 'sqlite3'
        db_filename = "occam.db"
        if env == :test
          db_filename = "occam-test.db"
        end
        options[:database] ||= File.join(occam_path, db_filename)
      end

      # Set up ActiveRecord
      ActiveRecord::Base.establish_connection(options)

      # Document store options
      dstore_opts = config["document_store"]
      options = {}

      if dstore_opts["adapter"] == 'mongodb'
        MongoMapper.database = dstore_opts["database"] || "occam-#{env.to_s}"
      end
    end
  end
end
