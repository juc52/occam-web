require_relative "helper"
require_model "workset"

describe Occam::Workset do
  describe "#initialize" do
    it "should not allow a workset with the same name to the same person" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name => "foo")
      workset = Occam::Workset.create(:name => "foo",
                                      :person => a)
      workset.save

      workset.errors.empty?.wont_equal true
    end

    it "should allow a workset with the same name from a different person" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      b = Occam::Person.create(:username => "cullen",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name => "foo")
      workset = Occam::Workset.create(:name => "foo",
                                      :person => b)
      workset.save

      workset.errors.empty?.must_equal true
    end
  end

  describe "#can_view?" do
    it "should not allow non owners/collaborators to view a private workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 1)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.can_view?(b).must_equal false
    end

    it "should allow owner to view a private workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 1)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.can_view?(a).must_equal true
    end

    it "should allow collaborator to view a private workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 1)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.people << b

      a.worksets.first.can_view?(b).must_equal true
    end

    it "should allow non owners/collaborators to view a public workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 0)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.can_view?(b).must_equal true
    end

    it "should allow owner to view a public workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 0)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.can_view?(a).must_equal true
    end

    it "should allow collaborator to view a public workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 0)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.people << b

      a.worksets.first.can_view?(b).must_equal true
    end
  end

  describe "#can_edit?" do
    it "should not allow non owners/collaborators to edit a private workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 1)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.can_edit?(b).must_equal false
    end

    it "should allow owner to edit a private workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 1)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.can_edit?(a).must_equal true
    end

    it "should allow collaborator to edit a private workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 1)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.people << b

      a.worksets.first.can_edit?(b).must_equal true
    end

    it "should not allow non owners/collaborators to edit a public workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 0)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.can_edit?(b).must_equal false
    end

    it "should allow owner to edit a public workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 0)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.can_edit?(a).must_equal true
    end

    it "should allow collaborator to edit a public workset" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 0)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.people << b

      a.worksets.first.can_edit?(b).must_equal true
    end
  end

  describe "#is_collaborator?" do
    it "should return false for non-owner/non-collaborator" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 0)

      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.is_collaborator?(b).must_equal false
    end

    it "should return true for owner" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 0)

      a.worksets.first.is_collaborator?(a).must_equal true
    end

    it "should return true for non-owning collaborator" do
      a = Occam::Person.create(:username => "wilkie",
                                :password => "foobar")
      a.worksets << Occam::Workset.create(:name    => "foo",
                                          :private => 0)
      b = Occam::Person.create(:username => "somebody_else",
                                :password => "foobar")

      a.worksets.first.people << b

      a.worksets.first.is_collaborator?(b).must_equal true
    end
  end
end
