require_relative 'helper'

describe Occam do
  describe "Session Controller" do
    describe "GET /login" do
      it "should render the login form" do
        Occam.any_instance.expects(:render).with(
          anything,
          :"sessions/login",
          anything
        )

        get '/login'
      end
    end

    describe "GET /logout" do
      it "should delete the person_id session key" do
        session = stub('session')
        session.expects(:[]=).with(:person_id, nil)
        session.stubs(:[]=).with(:nonce, anything)
        Occam.any_instance.stubs(:session).returns(session)

        get '/logout'
      end

      it "should delete the nonse session key" do
        session = stub('session')
        session.stubs(:[]=).with(:person_id, anything)
        session.expects(:[]=).with(:nonce, nil)
        Occam.any_instance.stubs(:session).returns(session)

        get '/logout'
      end
    end

    describe "POST /login" do
      before do
        @person = Occam::Person.create!(:username => "wilkie",
                                        :uid      => "abcd1234567890-1234-2345-34567890",
                                        :password => "foobar")

        Occam::Person.any_instance.stubs(:authenticated?).returns(false)
        Occam::Person.any_instance.stubs(:authenticated?).with("foobar").returns(true)

        @session = stub('session')
        @session.stubs(:[]=).with(:person_id, @person.id)
        @session.stubs(:[]=).with(:nonce, anything)

        Occam.any_instance.stubs(:session).returns(@session)
      end

      it "should post 302 when login is successful" do
        post '/login', {
          "username" => "wilkie",
          "password" => "foobar"
        }

        last_response.status.must_equal 302
      end

      it "should set the person_id in the session when login is successful" do
        @session.expects(:[]=).with(:person_id, @person.id)

        post '/login', {
          "username" => "wilkie",
          "password" => "foobar"
        }
      end

      it "should redirect to personal dashboard when login is successful" do
        post '/login', {
          "username" => "wilkie",
          "password" => "foobar"
        }

        last_response.location.must_equal "http://example.org/people/#{@person.uid}"
      end

      it "should post 422 when login is unsuccessful" do
        post '/login', {
          "username" => "wilkie",
          "password" => "WRONG"
        }

        last_response.status.must_equal 422
      end

      it "should report an error when login is unsuccessful" do
        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_of_type(:errors => ActiveModel::Errors)
        )

        post '/login', {
          "username" => "wilkie",
          "password" => "WRONG"
        }
      end

      it "should render the login form when login is unsuccessful" do
        Occam.any_instance.expects(:render).with(
          anything,
          :"sessions/login",
          anything
        )

        post '/login', {
          "username" => "wilkie",
          "password" => "WRONG"
        }
      end
    end
  end
end
