require_relative 'helper'

describe Occam do
  describe "Persons Controller" do
    describe "GET /people" do
      it "should pass an array of people to the view" do
        Occam::Person.create!(:username => "wilkie")
        Occam::Person.create!(:username => "wilkie2")
        Occam::Person.create!(:username => "wilkie3")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_of_type(:people => ActiveRecord::Relation::ActiveRecord_Relation_Occam_Person)
        )

        get '/people'
      end

      it "should query and pass the people to the view" do
        Occam::Person.create!(:username => "wilkie")

        person = Occam::Person.create!(:username => "wilkie3")

        Occam::Person.create!(:username => "wilkie2")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_includes(:people, person)
        )

        get '/people'
      end

      it "should return 200" do
        get '/people'

        last_response.status.must_equal 200
      end

      it "should render people/index.haml" do
        Occam.any_instance.expects(:render).with(
          anything,
          :"people/index",
          anything
        )

        get '/people'
      end
    end

    describe "GET /people/new" do
      it "should render people/new.haml" do
        Occam.any_instance.expects(:render).with(
          anything,
          :"people/new",
          anything
        )

        get '/people/new'
      end

      it "should return 200" do
        get '/people/new'

        last_response.status.must_equal 200
      end
    end

    describe "GET /people/:uuid/edit" do
      it "should render people/edit.haml when authorized" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        login_as(a.username, a)

        Occam.any_instance.expects(:render).with(
          anything,
          :"people/edit",
          anything
        )

        get "/people/#{a.uid}/edit"
      end

      it "should return 200 when authorized" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        login_as(a.username, a)

        get "/people/#{a.uid}/edit"

        last_response.status.must_equal 200
      end

      it "should return 404 when person is not found" do
        Occam.any_instance.stubs(:markdown)

        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        login_as(a.username, a)

        get "/people/asdf/edit"

        last_response.status.must_equal 404
      end

      it "should return 406 when unauthorized" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        b = Occam::Person.create!(:username => "jane",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        login_as(b.username, b)

        get "/people/#{a.uid}/edit"

        last_response.status.must_equal 406
      end
    end

    describe "GET /people/:uuid" do
      it "should render people/show.haml" do
        person = Occam::Person.create!(:username => "wilkie",
                                       :uid      => "abcd1234567890-1234-2345-34567890")

        Occam.any_instance.expects(:render).with(
          anything,
          :"people/show",
          anything
        )

        get "/people/#{person.uid}"
      end

      it "should query and pass the given person" do
        person = Occam::Person.create!(:username => "wilkie",
                                       :uid      => "abcd1234567890-1234-2345-34567890")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:person, person)
        )

        get "/people/#{person.uid}"
      end

      it "should return 200 when person is found" do
        person = Occam::Person.create!(:username => "wilkie",
                                       :uid      => "abcd1234567890-1234-2345-34567890")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:person, person)
        )

        get "/people/#{person.uid}"

        last_response.status.must_equal 200
      end

      it "should return 404 when person is not found" do
        Occam.any_instance.stubs(:markdown)

        get "/people/1234"

        last_response.status.must_equal 404
      end
    end

    describe "POST /people" do
      before do
        @person = Occam::Person.create!(:username => "wilkie",
                                        :uid      => "abcd1234567890-1234-2345-34567890")
        Occam::Person.stubs('create').returns(@person)

        session = stub('session')
        session.stubs(:[]=).with(:person_id, anything)
        session.stubs(:[]=).with(:nonce, anything)
        Occam.any_instance.stubs(:session).returns(session)
      end

      it "should return 302 when person is created" do
        post "/people", {
          "username" => "wilkie",
          "password" => "foobar"
        }

        last_response.status.must_equal 302
      end

      it "should redirect to person page when person is created" do
        post "/people", {
          "username" => "wilkie",
          "password" => "foobar"
        }

        person = Occam::Person.find_by(:username => "wilkie")

        last_response.location.must_equal(
          "http://example.org/people/#{person.uid}")
      end
    end

    describe "POST /people/:uuid" do
      it "should return a 404 if the person is not found" do
        Occam.any_instance.stubs(:markdown)

        post "/people/asdf", {}

        last_response.status.must_equal 404
      end

      it "should return a 406 if the person is not authorized to update" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        b = Occam::Person.create!(:username => "jane",
                                  :uid      => "dcba1234567890-1234-2345-34567890")

        login_as(b.username, b)

        post "/people/#{a.uid}", {
          "username" => "foobar"
        }

        last_response.status.must_equal 406
      end

      it "should not allow changes unless the person is currently logged in" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        post "/people/#{a.uid}", {
          "username" => "foobar"
        }

        Occam::Person.find_by(:uid => a.uid).username.must_equal "wilkie"
      end

      it "should allow changing of email" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :email    => "foo@example.org",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        login_as(a.username, a)

        post "/people/#{a.uid}", {
          "email" => "foobar@example.org"
        }

        Occam::Person.find_by(:uid => a.uid).email.must_equal "foobar@example.org"
      end

      it "should allow changing of organization" do
        a = Occam::Person.create!(:username     => "wilkie",
                                  :uid          => "abcd1234567890-1234-2345-34567890",
                                  :organization => "foobar")

        login_as(a.username, a)

        post "/people/#{a.uid}", {
          "organization" => "bazfoo"
        }

        Occam::Person.find_by(:uid => a.uid).organization.must_equal "bazfoo"
      end

      it "should allow changing of display_name" do
        a = Occam::Person.create!(:username     => "wilkie",
                                  :uid          => "abcd1234567890-1234-2345-34567890",
                                  :name         => "foobar")

        login_as(a.username, a)

        post "/people/#{a.uid}", {
          "name" => "bazfoo"
        }

        Occam::Person.find_by(:uid => a.uid).name.must_equal "bazfoo"
      end

      it "should allow changing of bio" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890",
                                  :bio      => "foobar")

        login_as(a.username, a)

        post "/people/#{a.uid}", {
          "bio" => "bazfoo"
        }

        Occam::Person.find_by(:uid => a.uid).bio.must_equal "bazfoo"
      end

      it "should allow changing of username" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        login_as(a.username, a)

        post "/people/#{a.uid}", {
          "username" => "foobar"
        }

        Occam::Person.find_by(:uid => a.uid).username.must_equal "foobar"
      end

      it "should not allow activation if not an administrator" do
        Occam::System.first.update_attributes(:moderate_people => true)

        # First person *is* an administrator
        a = Occam::Person.create!(:username => "admin",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        b = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "dcba1234567890-1234-2345-34567890")

        login_as(b.username, b)

        post "/people/#{b.uid}", {
          "active" => "1"
        }

        Occam::Person.find_by(:uid => b.uid).active.must_equal 0
      end

      it "should allow activation by an administrator" do
        Occam::System.first.update_attributes(:moderate_people => true)

        # First person *is* an administrator
        a = Occam::Person.create!(:username => "admin",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        b = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "dcba1234567890-1234-2345-34567890")

        login_as(a.username, a)

        post "/people/#{b.uid}", {
          "active" => "1"
        }

        Occam::Person.find_by(:uid => b.uid).active.must_equal 1
      end
    end
  end
end
