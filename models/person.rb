class Occam
  class Person < ActiveRecord::Base
    require 'securerandom'
    require 'digest'
    require 'open-uri'

    DEFAULT_GRAVATAR = "wilkie+occam@xomb.org"

    # A Person is attached to an Account, sometimes
    has_one :account

    # Fields

    # id              - Unique identifier.

    # username        - The name preferred for usernames

    # email           - The email for this person.

    # People can author worksets
    has_many :authorships
    has_many :worksets, :through => :authorships

    # People can collaborate on worksets
    has_many :collaboratorships
    has_many :collaborations, :class_name => "Occam::Workset",
                              :through    => :collaboratorships,
                              :source     => :workset

    # People can have more than one active session
    has_many :sessions

    # People have many runs of workflows
    has_many :runs

    # People have jobs on the queue
    has_many :jobs

    # Determine the canonical display name
    def name
      if !self[:name].blank?
        self[:name]
      else
        self.username
      end
    end

    # Determine the avatar image url
    def avatar_url(size)
      if !self.email.blank?
        "https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(self.email.strip.downcase)}?size=#{size}&d=#{URI::encode("https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(DEFAULT_GRAVATAR)}?size=#{size}")}"
      else
        if size >= 96
          "/images/person_large.png"
        else
          "/images/person.png"
        end
      end
    end

    def self.create(opts)
      # Spawn job
      username = opts[:username]
      password = opts[:password]
      command = "new person #{username} --password #{password}"
      Occam::Worker.perform(command)

      Occam::Person.find_by(:username => username)
    end

    def addWorkset(name)
      # Spawn job
      command = "new workset #{name} --as #{self.uid} --internal"
      Occam::Worker.perform(command).strip
    end

    def path
      # Return the path of the object in the local object store
      base_path = File.realpath(Occam::Config.configuration['paths']['objects'])
      code = self.uid[self.uid.length - 36..-1]
      code_1 = code[0..1]
      code_2 = code[2..3]
      File.join(base_path, 'person', code_1, code_2, self.uid)
    end

    def active_session_for?(nonce)
      self.sessions.where(:nonce => nonce).any?
    end

    def deactivate_session_for(nonce)
      self.sessions.where(:nonce => nonce).map(&:destroy)
    end

    def activate_session
      session = Occam::Session.create(:nonce => SecureRandom.hex(32))
      self.sessions << session
      self.save

      session
    end
  end
end
