class Occam
  class Workset < ActiveRecord::Base
    require 'json'

    # Fields

    # id        - Unique identifier.

    # name      - The name of the experiment.

    # tags      - The semicolon separated list of tags.

    # Worksets are authored by many people
    has_many :authorships
    has_many :authors, :class_name => "Occam::Person",
                       :through => :authorships,
                       :source  => :person

    # People can collaborate on worksets
    has_many :collaboratorships
    has_many :collaborators, :class_name => "Occam::Person",
                             :through => :collaboratorships,
                             :source  => :person

    # Many jobs can be spawned within a workset
    has_many :jobs

    # Returns true when the given person is collaborating (includes owner)
    def is_collaborator?(person)
      person and (self.authors.map(&:uid).include?(person.uid) or self.collaborators.map(&:uid).include?(person.uid))
    end

    # We can allow a reviewer access to a workset
    has_many :review_capabilities

    def can_review?(revision=nil)
      revision ||= self.revision
      if self.can_view?(nil)
        true
      else
        self.review_capabilities.where(:revision => revision).any?
      end
    end

    def allow_review(person)
      review = self.review_capabilities.where(:revision => self.revision)
      if review.empty?
        self.review_capabilities.create(:person => person, :revision => self.revision)
      end
    end

    # private - Boolean: whether or not the experiment is locked to creators/collaborators

    # Determines whether or not the person given can see this workset
    def can_view?(person)
      if self.private == 1
        self.is_collaborator? person
      else
        true
      end
    end

    # Returns the LocalLink's for the given Person
    def local_links(person)
      if person
        Occam::LocalLink.where(:person_id => person.id, :workset_id => self.id)
      else
        []
      end
    end

    # Determines whether or not the person given can edit this workset
    def can_edit?(person)
      self.is_collaborator? person
    end

    def self.all_tags(with = "")
      with ||= ""
      self.all.to_a.map(&:tags).flatten.uniq.select{|tag| tag.match(Regexp.escape(with))}
    end

    def self.all_tags_json(with = "")
      self.all_tags(with).to_json
    end

    def self.with_tag(tag)
      Workset.where('tags LIKE ?', "%;#{tag};%")
    end

    def objectInfoTags
      info = self.objectInfo()
      ret = info['tags'] or []
      if not ret.is_a? Array
        return []
      end
    end

    def tags
      (self[:tags] || ";;").split(';')[1..-1] or []
    end

    def update_info(info)
      # Encode all options as base64 for the sake of sanitization and so we
      # don't worry about escaping things
      def expandDataMap(hash, key="")
        # TODO: handle dots in base64
        hash.map do |k, v|
          new_key = key + "." + k
          if key == ""
            new_key = k
          end

          if v.is_a? Hash
            expandDataMap(v, new_key)
          else
            "\"#{Base64.strict_encode64(new_key.to_s)}\" \"#{Base64.strict_encode64(v.to_s)}\""
          end
        end.join(" ")
      end

      data = expandDataMap(info)

      command = "set --base64 #{data} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)}"
      Occam::Worker.perform(command)

      self
    end

    def fork(name, person)
      # TODO: sanitize
      command = "clone #{name} --to #{self.uid} --revision #{self.revision} --store --as #{person.uid}"
      new_id = Occam::Worker.perform(command).strip

      Workset.where(:uid => new_id).first
    end

    def forked_us
      Workset.where(:forked_from_id => self.id).select([:name, :id])
    end

    def instantiate
      require 'open3'

      # Create a directory for it in the workset path
      worksets_path = Occam::System.first.worksets_path
      self.path = File.join(worksets_path, self.id.to_s)

      # Run OCCAM workset creation
      Open3.popen3('occam', 'create', self.name, self.path, '--no-db', :chdir => worksets_path) do |i, o, e, t|
      end

      self.save
    end

    def initialize(options = {}, *args)
      # Ensure a leading and ending ',' in the tag CSV
      options[:tags] ||= ""
      if options[:tags].is_a? Array
        options[:tags] = ";#{options[:tags].join(';')};"
      end

      if !options[:tags].start_with?(";")
        options[:tags] = ";#{options[:tags]}"
      end
      if !options[:tags].end_with?(";")
        options[:tags] = "#{options[:tags]};"
      end

      super options, *args
    end

    def git
      @git ||= Git.new(self.path, self.revision)
    end

    def parentRevision
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.parentRevision(self.uid, self.revision)
      else
        self.git.parent(self.revision)
      end
    end

    def childRevisions
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.childRevisions(self.uid, self.revision)
      else
        self.git.children(self.revision)
      end
    end

    def fullRevision
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.fullRevision(self.uid, self.revision)
      else
        Git.fullRevision(self.path, self.revision)
      end
    end

    def authors
      info = self.objectInfo
      ret = []

      (info['authors'] or []).each do |author|
        if author.is_a? Hash
          id = author['id'] or nil
        else
          id = author
        end

        if not id.nil?
          person = Occam::Person.find_by(:uid => id)
          if person
            ret << person
          else
            ret << author
          end
        end
      end

      ret
    end

    def collaborators
      info = self.objectInfo
      ret = []

      (info['collaborators'] or []).each do |author|
        if author.is_a? Hash
          id = author['id'] or nil
        else
          id = author
        end

        if not id.nil?
          person = Occam::Person.find_by(:uid => id)
          if person
            ret << person
          else
            ret << author
          end
        end
      end

      ret
    end

    def addGroup(name)
      # TODO: sanitize
      command = "new group #{name} --to #{self.uid} --revision #{self.revision}"
      Occam::Worker.perform(command)
    end

    def addExperiment(name)
      # TODO: sanitize
      command = "new experiment #{name} --to #{self.uid} --revision #{self.revision}"
      Occam::Worker.perform(command)
    end

    def objectInfo
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      @objectInfo ||= self.retrieveJSON("object.json")
    end

    def retrieveJSON(path)
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.retrieveJSON(self.uid, self.revision, path)
      else
        self.git.retrieveJSON(path)
      end
    end

    def retrieveFile(path)
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.retrieveFile(self.uid, self.revision, path)
      else
        self.git.retrieveFile(path)
      end
    end

    def groups
      info = self.objectInfo()
      ret = []

      (info['dependencies'] or []).each do |dependency|
        if dependency['type'] == 'group'
          group = Occam::Group.find_by(:uid => dependency['id'])

          if group
            if dependency['revision']
              group.revision = dependency['revision']
            end
            ret << group
          end
        end
      end

      ret
    end

    def experiments
      info = self.objectInfo()
      ret = []

      (info['dependencies'] or []).each do |dependency|
        if dependency['type'] == 'experiment'
          experiment = Occam::Experiment.find_by(:uid => dependency['id'])

          if experiment
            if dependency['revision']
              experiment.revision = dependency['revision']
            end
            ret << experiment
          end
        end
      end

      ret
    end

    def path
      # Return the path of the object in the local object store
      base_path = File.realpath(Occam::Config.configuration['paths']['objects'])
      code = self.uid[self.uid.length - 36..-1]
      code_1 = code[0..1]
      code_2 = code[2..3]
      File.join(base_path, 'workset', code_1, code_2, self.uid)
    end
  end
end
