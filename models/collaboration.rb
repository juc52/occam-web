class Occam
  # The Collaboration table is a join table from worksets and people
  class Collaboration < ActiveRecord::Base
    # Fields
    belongs_to :workset
    belongs_to :person
  end
end
