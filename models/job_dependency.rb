class Occam
  class JobDependency < ActiveRecord::Base
    # Fields

    # id - Unique identifier.

    # Associations

    # job_id - Tags the job.
    belongs_to :job

    # depends_on_job_id - Tags the dependant job
    belongs_to :depends_on_job, :class_name => "Occam::Job"
  end
end
