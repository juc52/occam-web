class Occam
  class Group < ActiveRecord::Base
    # Fields

    # id        - Unique identifier.

    # name      - The name of the group.
    validates :name, :presence => true

    # tags      - The semicolon separated list of tags.

    # Experiment has a group_id foreign key
    has_many :experiments

    # person   - The person that created this group.
    belongs_to :person

    # workset - The Workset this group belongs to.
    belongs_to :workset

    # workflow - The Workflow this group uses.
    has_one :workflow

    # recipe - Groups can be based off of configurations.
    has_one :recipe

    # forked_from - The group this one was based off of (if any).
    belongs_to :forked_from, :class_name => "Group"

    def can_view?(person)
      self.worksetBelongsTo.can_view?(person)
    end

    # Whether or not there are results for any group within this group.
    def results?
      !self.experiments.select{|e| e.results?}.empty?
    end

    # Returns some aggregate understanding of the status of this group.
    # If all jobs have completed, it will be finished.
    # The rest of the statuses override this in this order:
    # If any job is unqueued, it will be unqueued.
    # If any job is queued, it will be queued.
    # If any job is running, it will be running.
    # If any job has failed, it will have failed.
    def status
      # Assume it is finished
      status = :finished

      order = [:unqueued, :queued, :running, :failed, :finished]

      self.experiments.map do |experiment|
        job_status = experiment.status

        if order.index(job_status) < order.index(status)
          status = job_status
        end
      end

      status
    end

    def self.all_tags(with = "")
      self.all.to_a.map{|ex| ex.tags[1..-2].split(";")}.flatten.uniq.select{|tag| tag.match(Regexp.escape(with))}
    end

    def self.all_tags_json(with = "")
      self.all_tags(with).to_json
    end

    def self.with_tag(tag)
      Group.where('tags LIKE ?', "%;#{tag};%")
    end

    def forked_us
      Group.where(:forked_from_id => self.id).select([:name, :id])
    end

    def instantiate
      require 'open3'

      # Create a directory for it in the workset
      self.path = File.join(self.workset.path, "group-#{self.id}")

      # Run OCCAM workset creation
      Open3.popen3('occam', 'add', 'group', self.name, '--no-db', :chdir => self.workset.path) do |i, o, e, t|
      end

      self.save
    end

    def initialize(options = {}, *args)
      # Ensure a leading and ending ',' in the tag CSV
      options[:tags] ||= ""
      if options[:tags].is_a? Array
        options[:tags] = ";#{options[:tags].join(';')};"
      end

      if !options[:tags].start_with?(";")
        options[:tags] = ";#{options[:tags]}"
      end
      if !options[:tags].end_with?(";")
        options[:tags] = "#{options[:tags]};"
      end

      super options, *args
    end

    def git
      @git ||= Git.new(self.path, self.revision)
    end

    def parentRevision
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.parentRevision(self.uid, self.revision)
      else
        self.git.parent(self.revision)
      end
    end

    def childRevisions
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.childRevisions(self.uid, self.revision)
      else
        self.git.children(self.revision)
      end
    end

    def fullRevision
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.fullRevision(self.uid, self.revision)
      else
        Git.fullRevision(self.path, self.revision)
      end
    end

    def objectInfo
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      @objectInfo ||= self.retrieveJSON("object.json")
    end

    def retrieveJSON(path)
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.retrieveJSON(self.uid, self.revision, path)
      else
        self.git.retrieveJSON(path)
      end
    end

    def retrieveFile(path)
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.retrieveFile(self.uid, self.revision, path)
      else
        self.git.retrieveFile(path)
      end
    end

    def authors
      info = self.objectInfo
      ret = []

      (info['authors'] or []).each do |author|
        person = Occam::Person.find_by(:uid => author['id'])
        if person
          ret << person
        end
      end

      ret
    end

    def collaborators
      info = self.objectInfo
      ret = []

      (info['collaborators'] or []).each do |author|
        person = Occam::Person.find_by(:uid => author['id'])
        if person
          ret << person
        end
      end

      ret
    end

    def groups
      info = self.objectInfo()
      ret = []

      (info['dependencies'] or []).each do |dependency|
        if dependency['type'] == 'group'
          group = Occam::Group.find_by(:uid => dependency['id'])

          if group
            if dependency['revision']
              group.revision = dependency['revision']
            end
            ret << group
          end
        end
      end

      ret
    end

    def experiments
      info = self.objectInfo()
      ret = []

      (info['dependencies'] or []).each do |dependency|
        if dependency['type'] == 'experiment'
          experiment = Occam::Experiment.find_by(:uid => dependency['id'])

          if experiment
            if dependency['revision']
              experiment.revision = dependency['revision']
            end
            ret << experiment
          end
        end
      end

      ret
    end

    def addGroup(workset, name)
      # TODO: sanitize
      command = "new group #{name} --to #{self.uid} --revision #{self.revision} --within #{workset.uid} --within-revision #{workset.revision}"
      Occam::Worker.perform(command)
    end

    def addExperiment(workset, name)
      # TODO: sanitize
      command = "new experiment #{name} --to #{self.uid} --revision #{self.revision} --within #{workset.uid} --within-revision #{workset.revision}"
      Occam::Worker.perform(command)
    end

    def path
      # Return the path of the object in the local object store
      base_path = File.realpath(Occam::Config.configuration['paths']['objects'])
      code = self.uid[self.uid.length - 36..-1]
      code_1 = code[0..1]
      code_2 = code[2..3]
      File.join(base_path, 'group', code_1, code_2, self.uid)
    end

    def worksetBelongsTo
      object_info = self.objectInfo
      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
          parent_info = parent.objectInfo()

          # TODO: move this to Group, allow it to iterate deeper but put a limit on it
          if parent_info.has_key? 'belongsTo'
            belongs_to = parent_info['belongsTo']

            if belongs_to[-9] == '-'
              code = belongs_to[belongs_to.length - 36 - 9..-1]
              type = belongs_to[0..-(38+9)]
            else
              code = belongs_to[belongs_to.length - 36..-1]
              type = belongs_to[0..-38]
            end

            if type == 'workset'
              workset = Occam::Workset.where(:uid => belongs_to).first
            end
          end
        end
      end

      workset
    end

    def belongsTo
      object_info = self.objectInfo
      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          parent = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
          parent_info = parent.objectInfo()

          # TODO: move this to Group, allow it to iterate deeper but put a limit on it
          if parent_info.has_key? 'belongsTo'
            belongs_to = parent_info['belongsTo']

            if belongs_to[-9] == '-'
              code = belongs_to[belongs_to.length - 36 - 9..-1]
              type = belongs_to[0..-(38+9)]
            else
              code = belongs_to[belongs_to.length - 36..-1]
              type = belongs_to[0..-38]
            end

            if type == 'workset'
              workset = Occam::Workset.where(:uid => belongs_to).first
            end
          end
        end
      end

      parent
    end
  end
end
