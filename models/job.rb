class Occam
  class Job < ActiveRecord::Base
    require 'json'

    # Fields

    # id          - Unique identifier.

    # codependant - Whether or not this job is a codependant of another
    def codependant=(value)
      # Coerse to integer
      if value.is_a? TrueClass
        value = 1
      elsif value.is_a? FalseClass
        value = 0
      end

      self[:codependant] = value
    end

    def codependant
      self[:codependant] == 1
    end

    def codependant?
      self.codependant
    end

    # has_dependencies - Whether or not this job has dependencies that must be
    # satisfied first.
    def has_dependencies=(value)
      # Coerse to integer
      if value.is_a? TrueClass
        value = 1
      elsif value.is_a? FalseClass
        value = 0
      end

      self[:has_dependencies] = value
    end

    def has_dependencies
      self[:has_dependencies] == 1
    end

    def has_dependencies?
      self.has_dependencies
    end

    # run - What "run" this job is partly managing. A run may have many jobs.
    belongs_to :run

    # object     - The object to build/install/etc.
    belongs_to :object, :foreign_key => :occam_object_id

    # person
    belongs_to :person

    # workset
    belongs_to :workset

    # experiment
    belongs_to :experiment

    # status     - The status of the job.
    validates_inclusion_of :status,
                           :in => [:unqueued,  :queued,  :pending,
                                   :suspended, :running, :finished]

    # kind       - The type of job.
    validates_inclusion_of :kind,
                           :in => [:build, :install, :run]

    # dependencies - The job that must be completed before this one can finish.
    has_many :job_dependencies
    has_many :dependencies, :through     => :job_dependencies,
                            :source      => :depends_on_job

    # codependencies - The job that must be ran while these given jobs are ran.
    has_many :job_codependencies
    has_many :codependencies, :through     => :job_codependencies,
                              :source      => :depends_on_job

    has_many :inputs

    # log_file   - The filename for the output of the job.

    # input_document_id - The document id for this job
    def input(options={})
      options ||= {}
      options[:format] ||= :hash

      document_id = BSON::ObjectId.from_string(self.input_document_id)
      document = Occam::InputDocument.first(:id => document_id)

      if document
        if options[:format] == :hash
          document.serializable_hash
        elsif options[:format] == :json
          document.serializable_hash.to_json
        end
      end
    end

    def input=(value)
      if self.input_document_id
        document_id = BSON::ObjectId.from_string(self.input_document_id)
        document = Occam::InputDocument.first(:id => document_id)
      end

      if document
        document.attributes = value
        document.save
      else
        document = Occam::InputDocument.create(value)
        self.input_document_id = document.id.to_s
        self.save
      end
      document
    end

    def monitoring_info
      Occam::Memcache.getJSON "job_#{self.id}"
    end

    def status
      status = super
      (status or "unqueued").intern
    end

    def kind
      kind = super
      (kind or "run").intern
    end

    def initialize(options = {}, *args)
      # Ensure status,kind is interned, not a string
      # We trust Job class interns because they can only be created internally
      options[:status] = options[:status].intern if options[:status]
      options[:kind]   = options[:kind].intern   if options[:kind]

      super options, *args
    end

    def connections
      self[:connections].split(';')[1..-1].map(&:to_i)
    end
  end
end
