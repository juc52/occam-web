class Occam
  class Session < ActiveRecord::Base
    # Fields
    # id
    # ip
    # created
    # nonce

    # Associations
    # person_id
    belongs_to :person
  end
end
