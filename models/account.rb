class Occam
  class Account < ActiveRecord::Base
    require 'bcrypt'

    # Attached to a person
    belongs_to :person

    # Fields

    # id              - Unique identifier.

    # active          - Whether or not this person has been moderated

    def self.all_inactive
      Account.where(:active => 0)
    end

    def self.all_active
      Account.where(:active => 1)
    end

    # email           - The email for this person.

    # hashed_password - The hash of the password.

    # roles           - A semi-colon (;) delimited string of roles this account has

    # Returns true if the user has the particular role
    def has_role?(role)
      self.roles.to_s.include? ";#{role};"
    end

    # Adds the given role to this user
    def add_role(role)
      roles = self.get_roles
      roles.append(role).uniq!
      self.roles = ";#{roles.join(';')};"
    end

    # Removes the given role if it exists
    def delete_role(role)
      roles = self.get_roles
      roles.delete(role)
      self.roles = ";#{roles.join(';')};"
    end

    def get_roles
      (self.roles or ";;").split(';')[1..-1] or []
    end

    # Create a hash of the password.
    def self.hash_password(password)
      BCrypt::Password.create(password, :cost => Occam::BCRYPT_ROUNDS)
    end

    # Determine if the given password matches the person.
    def authenticated?(password)
      (self.active == 1) && BCrypt::Password.new(hashed_password) == password
    end
  end
end
