class Occam
  class Object < ActiveRecord::Base
    require 'json'
    require_relative '../lib/git'

    # Fields

    # published - DateTime when object was originally published
    # updated   - DateTime when object was updated
    def update_timestamps
      now = Time.now.utc
      self[:published] ||= now if !persisted?
      self[:updated]     = now
    end
    before_save :update_timestamps

    # id        - Unique identifier.

    # name      - The name of the object.
    validates :name, :presence => true

    # object_type - Object's type (simulator, benchmark, etc)
    def type
      self.object_type
    end

    def type=(value)
      self.object_type = value
    end

    # Return a list of all known object types
    def self.types
      Occam::Object.all.select(:object_type).distinct.map(&:object_type)
    end

    # dependencies (through Dependency)
    has_many :dependencies, :foreign_key => :dependant_id

    # inputs
    has_many :inputs,
             :class_name  => "Occam::ObjectInput",
             :foreign_key => :occam_object_id

    # Yield all unique input types
    def input_types
      self.described_inputs
    end

    def indirect_input_types(object_type=nil)
      # For every type, find existing objects elsewhere that can generate them.
      # Sometimes, a simulator may want a trace, but it is more likely you
      # will just attach a trace generator instead. So this is for attaching
      # 'Generator' type objects to objects that take generic inputs.
      types = []
      self.described_inputs(object_type).each do |input|
        # for every input, look up all object_outputs that yield it.
        types.concat(input.indirect_objects.select(:object_type).distinct)
      end
      types
    end

    # Yield all objects that can output a particular type/group pair.
    def self.outputs_type(object_type, group)
      Occam::Object.where(:id => Occam::ObjectOutput.where(:object_type => object_type, :object_group => group).select(:occam_object_id))
    end

    def inputs_info(object_type = nil)
      info = self.objectInfo

      (info['inputs'] || []).select do |input|
        (input['type'] == "occam/runnable" && info.has_key?('run')) || (object_type.nil? || object_type == input['type'])
      end
    end

    # Yield all inputs, optionally all with the given type.
    def described_inputs(object_type = nil)
      info = self.objectInfo

      (info['inputs'] || []).select do |input|
        object_type.nil? || object_type == input['type']
      end.map do |input|
        Occam::ObjectInput.new(:object_type => input['type'], :object_group => input['group'], :fifo => input['fifo'] ? 1 : 0)
      end
    end

    # outputs
    has_many :outputs,
             :class_name  => "Occam::ObjectOutput",
             :foreign_key => :occam_object_id

    # Yield all unique output types
    def output_types
      self.outputs.select(:object_type).distinct.map(&:object_type)
    end

    # Returns the first inactive dependency (or nil if none)
    def first_inactive_dependency
      self.dependencies.each do |d|
        if !d.depends_on.active
          return d.depends_on
        end
      end

      nil
    end

    # Returns true if all dependencies are active
    def dependencies_active?
      self.first_inactive_dependency.nil?
    end

    # tags      - The semicolon separated list of tags.

    # active    - Whether or not this object has been moderated
    def active=(value)
      # Coerse to integer
      if value.is_a? TrueClass
        value = 1
      elsif value.is_a? FalseClass
        value = 0
      end

      self[:active] = value
    end

    def active
      self[:active] == 1
    end

    # built     - Whether or not the object has been built
    def built=(value)
      # Coerse to integer
      if value.is_a? TrueClass
        value = 1
      elsif value.is_a? FalseClass
        value = 0
      end

      self[:built] = value
    end

    def built
      self[:built] == 1
    end

    # Returns a query of all inactive Objects. These are Objects that
    # await administrator approval.
    def self.all_inactive(type = nil)
      ret = Object.where(:active => 0)
      if not type.nil?
        ret = ret.where(:object_type => type)
      end
      ret
    end

    # Returns a query of all active and available Objects. This means
    # they have been moderated (if necessary) by an administrator and they
    # have been successfully built.
    def self.all_active
      # There is no point in not having 'built' specified or placed in a
      # second function. You cannot have active: false and built: true
      # for instance.
      Object.where(:active => 1, :built => 1)
    end

    def self.names(type = nil)
      ret = self.all.select(:name, :id)
      if not type.nil?
        ret = ret.where(:object_type => type)
      end
      ret
    end

    def self.all_tags(type, with = "")
      # TODO: obviously, normalize tags
      self.all_active.where(:object_type => type).to_a.map{|ex| ex.tags[1..-2].split(";")}.flatten.uniq.select{|tag| tag.match(Regexp.escape(with))}
    end

    def self.all_tags_json(with = "")
      self.all_tags(with).to_json
    end

    def self.with_tag(type, tag)
      Object.all_active.where(:object_type => type).where('tags LIKE ?', "%;#{tag};%")
    end

    def self.search(term)
      Object.where('name LIKE ?', "%#{term}%").select([:name, :description, :id])
    end

    def self.search_adv(name, tags, description, website, organization, license, authors)
      query = Object.where('name LIKE ?', "%#{name}%")
      tags = tags.split ;
      tags.each do |tag|
        query = query.where('tags LIKE ?', "%#{tag}%")
      end
      query = query.where('description LIKE ?', "%#{description}")
      query = query.where('website LIKE ?', "%#{website}")
      query = query.where('organization LIKE ?', "%#{organization}")
      query = query.where('license LIKE ?', "%#{license}")
      query = query.where('authors LIKE ?', "%#{authors}")
      query = query.select([:name, :description, :id])
    end

    # Install the object to the system (delayed via worker)
    def install
      return nil

      # Cannot install if not active!
      return nil if !self.active

      job = self.install_job

      if job.nil?
        # Install task depends on dependencies being available on the system
        deps_jobs = self.dependencies.map do |dep|
          if !dep.depends_on.active
            # Cannot install if dependencies aren't active
            return nil
          end

          dep_job = dep.depends_on.build_job
          if dep_job.nil?
            # We need to build this dependency, so build it
            dep.depends_on.install
            dep_job = dep.depends_on.build

            # If we cannot queue a build of this dependency, abort
            return nil if dep_job.nil?
          end

          dep_job
        end.select do |build_job|
          build_job.status != :finished
        end

        job = Job.create(:object           => self,
                         :kind             => "install",
                         :codependant      => false,
                         :has_dependencies => deps_jobs.any?,
                         :dependencies     => deps_jobs,
                         :status           => "queued")
      end

      job
    end

    # Returns the Job object that is responsible for installing this object
    def install_job
      #Job.where(:occam_object_id => self.id, :kind => "install").first
      nil
    end

    # Build the object (delayed via worker)
    def build
      return nil

      # Cannot build if not active!
      return nil if !self.active

      install_job = self.install_job
      job         = self.build_job

      if install_job.nil?
        # Cannot build unless we have a task in the system to install
        return nil
      end

      # TODO: Race condition on depends_on??
      if job.nil?
        job = Job.create(:object           => self,
                         :kind             => "build",
                         :status           => "queued",
                         :codependant      => false,
                         :has_dependencies => !install_job.nil?,
                         :dependencies     => install_job && [install_job])
      end

      job
    end

    # Returns the Job object that is responsible for building this object
    def build_job
      #Job.where(:occam_object_id => self.id, :kind => "build").first
      nil
    end

    def recipes
      Recipe.where(:occam_object_id => self.id)
    end

    # Updates the object and creates a new version of the built object
    def update
    end

    # Pull object record information from the given url in script_path
    def import(url=nil, json=nil)
      self
    end

    def initialize(revision, options = {}, *args)
      system = System.retrieve

      if system.moderate_objects
        options[:active] = false
      else
        options[:active] = true
      end

      # Coerse to integer
      if options[:active].is_a? TrueClass
        options[:active] = 1
      elsif options[:active].is_a? FalseClass
        options[:active] = 0
      end

      if options[:built].is_a? TrueClass
        options[:built] = 1
      elsif options[:built].is_a? FalseClass
        options[:built] = 0
      end

      # Ensure a leading and ending ';' in the tag CSV
      options[:tags] ||= ""
      if options[:tags].is_a? Array
        options[:tags] = ";#{options[:tags].join(';')};"
      end

      if !options[:tags].start_with?(";")
        options[:tags] = ";#{options[:tags]}"
      end
      if !options[:tags].end_with?(";")
        options[:tags] = "#{options[:tags]};"
      end

      # Figure out the slug path to install the object to
      if options[:name]
        slug = options[:name].to_url
        root_path  = system.objects_path
        options[:path] ||= "#{root_path}/#{slug}"
      end

      object_path = options[:path]

      if options.has_key? :objectInfo
        @objectInfo = options[:objectInfo]
        options.delete :objectInfo
      end

      if revision != nil
        @revision = revision
        @full_revision = Occam::Git.fullRevision(object_path)
        @git = Occam::Git.new(object_path, @full_revision)
        @local = false
      else
        @local = true
      end

      super options, *args
    end

    def local?
      @local
    end

    # Override to_a to split tag strings to arrays
    def to_hash
      hash = self.serializable_hash

      hash["tags"] = hash["tags"].split(';').drop(1)
      hash["authors"] = hash["authors"].split(';').drop(1)

      hash["type"] = hash["object_type"]
      hash.delete "object_type"

      # Provide urls for schemata
      hash.delete "input_schema_document_id"
      hash.delete "output_schema_document_id"
      # TODO: add configurations/outputs sections

      hash["input_schema"]  = "/objects/#{self.id}/input_schema.json"
      hash["output_schema"] = "/objects/#{self.id}/output_schema.json"

      # Delete normalized fields (their info is within 'install')
      hash.delete "binary_path"
      hash.delete "package_type"
      hash.delete "script_path"

      # Set up 'self', 'install', 'build', and 'run' blobs
      hash["install"] = hash["install_metadata"]
      hash["build"]   = hash["build_metadata"]
      hash["self"]    = hash["self_metadata"]
      hash["run"]     = hash["run_metadata"]
      hash["init"]    = hash["init_metadata"]
      hash.delete "install_metadata"
      hash.delete "build_metadata"
      hash.delete "self_metadata"
      hash.delete "run_metadata"
      hash.delete "init_metadata"

      # Add ourselves as a mirror
      hash["self"] << {
        "git"  => "/objects/#{self.id}",
        "html" => "/objects/#{self.id}"
      }

      # Remove nil fields
      hash.delete_if {|k, v| v.nil?}

      hash.delete "id"

      hash
    end

    def to_json(*args)
      self.to_hash.to_json
    end

    def git
      @git ||= Git.new(self.path, self.revision)
    end

    def parentRevision
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.parentRevision(self.uid, self.revision)
      else
        self.git.parent(self.revision)
      end
    end

    def childRevisions
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.childRevisions(self.uid, self.revision)
      else
        self.git.children(self.revision)
      end
    end

    def fullRevision
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.fullRevision(self.uid, self.revision)
      else
        Git.fullRevision(self.path, self.revision)
      end
    end

    def objectInfo
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      @objectInfo ||= self.retrieveJSON("object.json")
      if @objectInfo['id'] != self.uid
        info = @objectInfo
        @objectInfo = {}
        (info['derives'] || []).each do |derivative|
          if derivative['id'] == self.uid
            new_info = info.clone()

            new_info.delete 'derives'
            new_info.delete 'inputs'
            new_info.delete 'outputs'
            new_info.delete 'install'
            new_info.delete 'build'
            new_info.delete 'run'
            new_info.delete 'configurations'
            new_info.delete 'include'

            new_info.merge!(derivative)
            @objectInfo = new_info
            break
          end
        end
      end
      @objectInfo
    end

    def retrieveJSON(path)
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.retrieveJSON(self.uid, self.revision, path)
      else
        self.git.retrieveJSON(path)
      end
    end

    def retrieveFile(path)
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      # TODO: allow a max byte size to return for some views to show parts of files
      if Occam::Backend.connected
        Occam::Backend.retrieveFile(self.uid, self.revision, path)
      else
        self.git.retrieveFile(path)
      end
    end

    def configurations
      self.objectInfo['configurations'] || []
    end

    def self.download_json(url, content_type = 'application/json', limit = 10)
      # Just try and pull json

      uri = URI(url)
      request = Net::HTTP::Get.new(uri.request_uri)
      request['Accept'] = content_type
      request.content_type = content_type

      http = Net::HTTP.new(uri.hostname, uri.port)
      if uri.scheme == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end

      response = http.request(request)

      # Did we get a response? (allow redirects)
      if response.is_a?(Net::HTTPRedirection) && limit > 0
        location = response['location']
        puts "redirect"
        Occam::Object.download_json(location, content_type, limit - 1)
      elsif response.is_a?(Net::HTTPSuccess) && response.content_type == "application/json"
        JSON.parse(response.body)
      else # Otherwise, assume git:
        Occam::Git.description(url)
      end
    end

    def self.all_unknown_dependencies(url, json=nil)
      json ||= Occam::Object.download_json(url)
      json["type"] ||= "simulator"

      unknowns = Occam::Object.unknown_dependencies(json)

      deps = []

      unknowns.each do |unknown|
       # url = unknown["git"]
       # json = Occam::Object.download_json(url)
       # obj  = Occam::Object.import(url, json)
       # data = Occam::Object.all_unknown_dependencies(url, json)
       # deps.concat data
       # deps.append obj
      end

      deps
    end

    def self.import_all(url, json=nil)
      json ||= Occam::Object.download_json(url)

      uuid = json['id']

      command = "pull #{url}"

      Occam::Worker.perform(command)

      obj = Occam::Object.where(:uid => uuid).first
    end

    # Pulls an object from a OCCAM json description file.
    def self.import(url, json=nil)
      json ||= Occam::Object.download_json(url)
      json["type"] ||= "simulator"
      uid = json['id']

      # Pull out existing object or create a new one
      object = Object.where(:object_type => json["type"]).where(:uid => uid).first || Object.new(nil, :objectInfo => json, :uid => uid)
      object.uid = uid
      object.import(url, json)
    end

    def self.unknown_dependencies(json)
      ret = []

      if json.has_key? "dependencies"
        json["dependencies"].each do |dependency|
          object_type = dependency["type"] || "simulator"

          if dependency.has_key? "id"
            object = Object.where(:object_type => object_type).where(:uid => dependency["id"]).first
          end

          if object.nil?
            ret << dependency
          end
        end
      end

      ret
    end

    # Given a json object description, return all realized dependencies.
    def self.dependencies(json)
      ret = []

      if json.has_key? "dependencies"
        json["dependencies"].each do |dependency|
          # TODO: Other code syndication options
          object_type = dependency["type"] || "simulator"
          if dependency.has_key? "git"
            object = Object.where(:object_type => object_type).where(:script_path => dependency["git"]).first
          end

          if object.nil? && dependency.has_key?("name")
            object = Object.where(:object_type => object_type).where(:name => dependency["name"]).first
          end

          unless object.nil?
            ret << Dependency.new(:depends_on => object)
          end
        end
      end

      ret
    end

    def self.slugFor(object_type)
      object_type.tr('/', '-')
    end

    def self.objectTypeFromUUID(uuid)
      if uuid[-9] == '-'
        type = uuid[0..-(38+9)]
      else
        type = uuid[0..-38]
      end

      type
    end

    def authors
      info = self.objectInfo
      ret = []

      (info['authors'] or []).each do |author|
        person = Occam::Person.find_by(:uid => author['id'])
        if person
          ret << person
        end
      end

      ret
    end

    def collaborators
      info = self.objectInfo
      ret = []

      (info['collaborators'] or []).each do |author|
        person = Occam::Person.find_by(:uid => author['id'])
        if person
          ret << person
        end
      end

      ret
    end

    def file
      # Retrieves the file attached to this object
      objectInfo = self.objectInfo()
      if objectInfo.has_key? 'file'
        self.retrieveFile(objectInfo['file'])
      else
        ""
      end
    end

    def fileAsJSON
      # Retrieves the file attached to this object as JSON
      objectInfo = self.objectInfo()
      if objectInfo.has_key? 'file'
        self.retrieveJSON(objectInfo['file'])
      else
        {}
      end
    end

    def schema
      # Retrieves the schema attached to this object as JSON
      objectInfo = self.objectInfo()
      if objectInfo.has_key? 'schema'
        self.retrieveJSON(objectInfo['schema'])
      else
        {}
      end
    end

    def path
      # Return the path of the object in the local object store
      base_path = File.realpath(Occam::Config.configuration['paths']['objects'])
      code = self.uid[self.uid.length - 36..-1]
      code_1 = code[0..1]
      code_2 = code[2..3]
      File.join(base_path, self.object_type_safe, code_1, code_2, self.uid)
    end

    # Returns any available configurators for this object.
    def configurators
      Occam::Object.where(:object_type => "configurator", :configures => self.uid)
    end

    def self.basic_type_search(query)
      Occam::Object.where('object_type LIKE ?', "%#{query}%").select(:object_type, :object_type_safe).distinct
    end

    def self.basic_name_search(query)
      Occam::Object.where('name LIKE ?', "%#{query}%").select(:object_type, :name, :uid, :object_type_safe)
    end

    def self.basic_tag_search(query)
      # TODO: return only an array of tags??
      Occam::Object.where('tags LIKE ?', ";%#{query}%;").select(:object_type, :name, :uid, :object_type_safe)
    end

    # Returns the bibtex formatted version of the citation, if one exists
    def bibtex
      objectInfo = self.objectInfo()
      citation = objectInfo['citation']
      if citation
        # TODO: safety of object name for bibtex purposes
        ret = "@#{(citation['type'] || "article").upcase}{#{self.name},\n"
        citation.each do |k,v|
          if k != "type"
            if v.is_a? Array
              v = v.join(';')
            end
            # TODO: escape semicolons and curly braces??
            ret << "#{k}={#{v}},\n"
          end
        end
        ret << "}"
      else
        nil
      end
    end
  end
end
