class Occam
  class Run < ActiveRecord::Base
    # Fields

    # id        - Local Unique identifier.

    # uid       - Unique identifier.

    # jobs      - The jobs pertaining to this run through the workflow.
    has_many :jobs

    # person - The person that created this experiment run.
    belongs_to :person

    # experiment - The Experiment this run belongs to.
    belongs_to :experiment

    # workset - The Workset this run belongs to.
    belongs_to :workset

    # private - Boolean: whether or not the run is locked to creators/collaborators
    def can_view?(account)
      # TODO: this
      if self.experiment.private
      else
        true
      end
    end

    def status
      # TODO: this

      # Assume it is finished
      status = :finished

      order = [:unqueued, :queued, :running, :failed, :finished]

      self.jobs.each do |job|
        job_status = job.status

        if order.index(job_status) < order.index(status)
          status = job_status
        end
      end

      status
    end

    # Cancel the experiment by unqueuing all attached jobs
    def cancel
      # TODO: have OCCAM kill the run through a worker command
    end

    def run
      # TODO: this
    end

    def done?
      self.done_connections.length == self.jobs.count
    end

    def running_connections
      self.jobs.where(:status => "running").map do |job|
        job.connections
      end.flatten
    end

    def done_connections
      self.jobs.where(:status => "finished").map do |job|
        job.connections
      end.flatten
    end
  end
end
