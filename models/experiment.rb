class Occam
  class Experiment < ActiveRecord::Base
    require 'json'
    require 'pp'
    require 'digest'

    # Fields

    # id        - Unique identifier.

    # name      - The name of the experiment.
    validates :name, :presence => true

    # tags      - The semicolon separated list of tags.

    # workflow  - The workflow to use to generate results.
    has_one :workflow

    # runs      - The individual workflow passes.
    has_many :runs

    # configuration - The configuration JSON.
    def configuration
      #document_id = BSON::ObjectId.from_string(self.configuration_document_id)

      #document = Occam::ConfigurationDocument.first(:id => document_id)

      #if document
      #  document.serializable_hash
      #end
      {}
    end

    def configuration=(value)
      if self.configuration_document_id
        document_id = BSON::ObjectId.from_string(self.configuration_document_id)
        document = Occam::ConfigurationDocument.first(:id => document_id)
      end

      if document
        document.attributes = value
        document.save
      else
        document = Occam::ConfigurationDocument.create(value)
        self.configuration_document_id = document.id.to_s
        self.save
      end
      document
    end

    # person   - The person that created this experiment.
    belongs_to :person

    # recipe - The Recipe this experiment was spawned from (if any).
    belongs_to :recipe

    # workset - The Workset this experiment belongs to.
    belongs_to :workset

    # group - The Group this experiment belongs to.
    belongs_to :group

    # forked_from - The experiment this one was based off of (if any).
    belongs_to :forked_from, :class_name => "Experiment"

    # Many jobs can be spawned from an experiment
    has_many :jobs

    # private - Boolean: whether or not the experiment is locked to creators/collaborators

    def can_view?(person)
      # TODO: handle this on the workset case
      #if self.private
      #else
        true
      #end
    end

    def results?
      false
    end

    # Cancel the experiment by unqueuing all attached jobs
    def cancel
      self.runs.each do |run|
        run.cancel
      end
    end

    # Run the experiment by spawning run jobs
    def run(workset)
      # TODO: sanitize
      options = ""

      command = "run --dispatch --to #{self.uid} --revision #{self.revision} --within #{workset.uid} --within-revision #{workset.revision}"

      Occam::Worker.perform(command)
    end

    def status
      # Assume it is finished
      status = :finished

      order = [:unqueued, :queued, :running, :failed, :finished]

      self.runs.map do |run|
        job_status = run.status

        if order.index(job_status) < order.index(status)
          status = job_status
        end
      end

      status
    end

    def self.all_tags(with = "")
      self.all.to_a.map{|ex| ex.tags[1..-2].split(";")}.flatten.uniq.select{|tag| tag.match(Regexp.escape(with))}
    end

    def self.all_tags_json(with = "")
      self.all_tags(with).to_json
    end

    def self.with_tag(tag)
      Experiment.where('tags LIKE ?', "%;#{tag};%")
    end

    def fork(name, to, person)
      # TODO: sanitize
      command = "clone #{name} --to #{self.uid} --revision #{self.revision} --within #{to.uid} --within-revision #{to.revision} --store --as #{person.uid}"
      new_id = Occam::Worker.perform(command).strip

      puts "looking at experiment #{new_id}"
      experiment = Experiment.where(:uid => new_id).first
      puts "found experiment #{experiment}"
      experiment
    end

    def forked_us
      Experiment.where(:forked_from_id => self.id).select([:name, :id])
    end

    def self.basic_search(term)
      Experiment.where('name LIKE ?', "%#{term}%").select([:name, :id])
    end

    def self.search_sim(name, tags, simulator, forked)
      query = Experiment.where('name LIKE ?', "%#{name}%")
      tags = tags.split ;
      tags.each do |tag|
        query = query.where('tags LIKE ?', "%#{tag}%")
      end
      if simulator > 0
        query = query.where('simulator_id = ?', "#{simulator}")
      end
     #query = query.where('person_id LIKE ?', "%#{person}%")
      if forked == 1
        query = query.where('forked_from_id IS NOT NULL')
      elsif forked == 2
        query = query.where('forked_from_id IS NULL')
      end
      query = query.select([:name, :id])
    end

    def self.search(params)
      data = {}
      if params["simulator_id"] != "___any___"
        simulator = Object.where(:type => :simulator).find_by(:id => params["simulator_id"].to_i)

        if params["data"] && params["data"].is_a?(Array)
          data = params["data"].first

          schema = simulator.input_schema
          Occam::Configuration.type_check(data, schema, nil, true)
        end
      end

      query = Experiment
      query = query.where('name LIKE ?', "%#{params["name"]}%") if params["name"] && params["name"] != ""

      tags = params["tags"] || ""

      tags = tags.split(';')
      tags.each do |tag|
        query = query.where('tags LIKE ?', "%;#{tag};%")
      end

      query = query.where('simulator_id = ?', "#{simulator.id}") if simulator
      query = query.where('forked_from_id IS NOT NULL') if params["forked"]

      # Configuration Facet Search
      # Find configurations in the document store and add the final sql query for those ids
      unless data.empty?
        configurations = Occam::Configuration.search(data)
        if configurations.count > 0
          query = query.where('configuration_document_id IN (?)', configurations.to_a.map{|e| e.id.to_s})
        else
          return []
        end
      end

      query = query.select([:name, :id])
    end

    # Spawns Run records for every possible run within this experiment.
    def spawn_runs
    end

    def initialize(options = {}, *args)
      # Ensure a leading and ending ',' in the tag CSV
      options[:tags] ||= ""
      if options[:tags].is_a? Array
        options[:tags] = ";#{options[:tags].join(';')};"
      end

      if !options[:tags].start_with?(";")
        options[:tags] = ";#{options[:tags]}"
      end
      if !options[:tags].end_with?(";")
        options[:tags] = "#{options[:tags]};"
      end

      super options, *args
    end

    def attach(object, workset, connection_index, created_object_type=nil, created_object_group=nil)
      # TODO: sanitize
      options = ""
      if created_object_group
        options = options + "--group #{created_object_group} "
      end

      if created_object_type
        command = "attach #{connection_index >= 0 ? connection_index : ""} #{options} --create #{created_object_type} --to #{self.uid} --revision #{self.revision} --within #{workset.uid} --within-revision #{workset.revision}"
      else
        command = "attach #{connection_index >= 0 ? connection_index : ""} #{options} --id #{object.uid} --object-revision #{object.revision} --to #{self.uid} --revision #{self.revision} --within #{workset.uid} --within-revision #{workset.revision}"
      end
      puts command
      Occam::Worker.perform(command)
    end

    def detach(workset, connection_index)
      # TODO: sanitize
      options = ""
      command = "detach #{connection_index} #{options} --to #{self.uid} --revision #{self.revision} --within #{workset.uid} --within-revision #{workset.revision}"
      Occam::Worker.perform(command)
    end

    def update_info(info, workset)
      # Encode all options as base64 for the sake of sanitization and so we
      # don't worry about escaping things
      def expandDataMap(hash, key="")
        # TODO: handle dots in base64
        hash.map do |k, v|
          new_key = key + "." + k
          if key == ""
            new_key = k
          end

          if v.is_a? Hash
            expandDataMap(v, new_key)
          else
            "\"#{Base64.strict_encode64(new_key.to_s)}\" \"#{Base64.strict_encode64(v.to_s)}\""
          end
        end.join(" ")
      end

      data = expandDataMap(info)

      command = "set --base64 #{data} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)} --within #{Base64.strict_encode64(workset.uid)} --within-revision #{Base64.strict_encode64(workset.revision)}"
      Occam::Worker.perform(command)

      self
    end

    def configure(connection_index, data, workset)
      # Encode all options as base64 for the sake of sanitization and so we
      # don't worry about escaping things
      def expandDataMap(hash, key="")
        # TODO: handle dots in base64
        hash.map do |k, v|
          new_key = key + "." + k
          if key == ""
            new_key = k
          end

          if v.is_a? Hash
            expandDataMap(v, new_key)
          else
            "\"#{Base64.strict_encode64(new_key.to_s)}\" \"#{Base64.strict_encode64(v.to_s)}\""
          end
        end.join(" ")
      end

      data = expandDataMap(data)

      command = "configure --base64 #{Base64.strict_encode64(connection_index.to_s)} #{data} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)} --within #{Base64.strict_encode64(workset.uid)} --within-revision #{Base64.strict_encode64(workset.revision)}"
      Occam::Worker.perform(command)
    end

    def git
      @git ||= Git.new(self.path, self.revision)
    end

    def worksetBelongsTo
      object_info = self.objectInfo
      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
          parent_info = parent.objectInfo()

          # TODO: move this to Group, allow it to iterate deeper but put a limit on it
          if parent_info.has_key? 'belongsTo'
            belongs_to = parent_info['belongsTo']

            if belongs_to[-9] == '-'
              code = belongs_to[belongs_to.length - 36 - 9..-1]
              type = belongs_to[0..-(38+9)]
            else
              code = belongs_to[belongs_to.length - 36..-1]
              type = belongs_to[0..-38]
            end

            if type == 'workset'
              workset = Occam::Workset.where(:uid => belongs_to).first
            end
          end
        end
      end

      workset
    end

    def belongsTo
      object_info = self.objectInfo
      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          parent = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
          parent_info = parent.objectInfo()

          # TODO: move this to Group, allow it to iterate deeper but put a limit on it
          if parent_info.has_key? 'belongsTo'
            belongs_to = parent_info['belongsTo']

            if belongs_to[-9] == '-'
              code = belongs_to[belongs_to.length - 36 - 9..-1]
              type = belongs_to[0..-(38+9)]
            else
              code = belongs_to[belongs_to.length - 36..-1]
              type = belongs_to[0..-38]
            end

            if type == 'workset'
              workset = Occam::Workset.where(:uid => belongs_to).first
            end
          end
        end
      end

      parent
    end

    def parentRevision
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.parentRevision(self.uid, self.revision)
      else
        self.git.parent(self.revision)
      end
    end

    def childRevisions
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.childRevisions(self.uid, self.revision)
      else
        self.git.children(self.revision)
      end
    end

    def fullRevision
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.fullRevision(self.uid, self.revision)
      else
        Git.fullRevision(self.path, self.revision)
      end
    end

    def objectInfo
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      @objectInfo ||= self.retrieveJSON("object.json")
    end

    def retrieveJSON(path)
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.retrieveJSON(self.uid, self.revision, path)
      else
        self.git.retrieveJSON(path)
      end
    end

    def retrieveFile(path)
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.retrieveFile(self.uid, self.revision, path)
      else
        self.git.retrieveFile(path)
      end
    end

    def workflow
      info = self.objectInfo

      workflow = info['workflow'] || {}
      workflow['connections'] = workflow['connections'] || []

      index = 0
      workflow['connections'].each do |connection|
        if connection.has_key? 'object'
          connection['index'] = index
          connection['input'] = {}
          connection['object_realized'] = Occam::Object.where(:uid => connection['object']['id']).first
          if connection['object'].has_key? 'revision'
            connection['object_realized'].revision = connection['object']['revision']
          end
          connection['connections'] = []
        end

        index += 1
      end

      workflow['connections'].each do |connection|
        if connection.has_key? 'object'
          connection['connections'] = workflow['connections'].select do |sub_connection|
            sub_connection['to'] == connection['index']
          end

          # Determine input connection
          connection['connections'].each do |sub_connection|
            if connection['object_realized']
              if not (connection['input_types'] || []).include?(sub_connection['object']['type'])
                input_info = connection['object_realized'].inputs_info(sub_connection['object']['type']).first
                puts "OKOK"
                if input_info.has_key?("max")
                  connection['input_max'] = (connection['input_max'] || 0) + (input_info['max'] || 0)
                  connection['input_types'] = (connection['input_types'] || []).push(sub_connection['object']['type'])
                end
              end
            end
          end
        end
      end

      workflow
    end

    def tail_connections
      workflow = self.workflow
      workflow['connections'].select do |connection|
        (connection['to'] || -1) == -1
      end
    end

    def authors
      info = self.objectInfo
      ret = []

      (info['authors'] or []).each do |author|
        person = Occam::Person.find_by(:uid => author['id'])
        if person
          ret << person
        end
      end

      ret
    end

    def outputs(type=nil)
      info = self.objectInfo
      generates = info['generates'] || []

      if type
        generates.select do |output|
          output['type'] == type
        end
      else
        generates
      end
    end

    def collaborators
      info = self.objectInfo
      ret = []

      (info['collaborators'] or []).each do |author|
        person = Occam::Person.find_by(:uid => author['id'])
        if person
          ret << person
        end
      end

      ret
    end

    def path
      # Return the path of the object in the local object store
      base_path = File.realpath(Occam::Config.configuration['paths']['objects'])
      code = self.uid[self.uid.length - 36..-1]
      code_1 = code[0..1]
      code_2 = code[2..3]
      File.join(base_path, 'experiment', code_1, code_2, self.uid)
    end
  end
end
