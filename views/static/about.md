# The <span class="logo">Occam</span> System

<span class='logo'>Occam</span> (Open Curation for Computer Architecture Modeling) is a project that will serve as the catalyst for the tools, education, and community-building needed to bring openness, accountability, comparability, and repeatability to computer architecture experimentation.

## Simulator Support

Researchers work very hard to develop simulators that can be used by others.
However, these large, complex pieces of software are difficult to use or require
non-trivial effort to install. We have not created a convention to maintain these
useful repositories of code. <span class='logo'>Occam</span> attempts to solve this by developing a
standard that can increase the ease of using existing simulators without requiring
simulator writers to change their code or even agree to our standard.

## Describing a Simulator

Here is an excerpt from [DRAMSim2](https://wiki.umd.edu/DRAMSim2)'s simulator
description file:

```
{
  "name": "DRAMSim2",

  "website": "https://wiki.umd.edu/DRAMSim2",

  "description": "**DRAMSim** is a cycle accurate model of a DRAM memory controller, the DRAM
                  modules which comprise system storage, and the bus by which they communicate.
                  All major components in a modern memory system are modeled as their own
                  respective objects within the source, including: ranks, banks, command queue,
                  the memory controller, etc.\n\nThe overarching goal is to have a simulator that
                  is extremely small, portable, and accurate. The simulator core has a
                  well-defined interface which allows it to be CPU simulator agnostic and should
                  be easily modifiably to work with any simulator.  This core has no external run
                  time or build time dependencies and has been tested with g++ on Linux as well as
                  g++ on Cygwin on Windows.",

  "authors": ["Rosenfeld, P.", "Cooper-Balis, E.", "Jacob, B."],
  "organization": "University of Maryland, College Park",

  "git": "git://github.com/dramninjasUMD/DRAMSim2.git",
  "license": "BSD",

  "tags": ["memory", "DRAM", "DDR2", "DDR3", "trace-based", "memory cards",
           "memory architecture", "DRAM architecture"]
}
```

We can then place that inside a code repository. For example: [https://bitbucket.org/occam/occam-dramsim2](https://bitbucket.org/occam/occam-dramsim2/src/e19d7a6fc4e6518194bea0a56c3a4d6ebf16b2ed?at=master). Now it is available to be imported in the <span class='logo'>Occam</span> system:

![One can press the import button on the new simulator page to import from a git repository](/images/about-import-sim.png)

![Upon pressing import, the information is loaded from the remote server](/images/about-import-sim-2.png)

This information can be provided by the simulator developers or by a third-party. Once it
has been written for one system, including the public instance, it may then be used by any <span class='logo'>Occam</span> system.

## Open-ended Configuration

In order to handle the sheer multitude of often confusing configuration options, methods, and formats, <span class='logo'>Occam</span> standardizes the effort of configuration. Alongside the simulator description, an input schema is provided in the script repository. This can describe groups of configuration parameters, defaults, and input types. For example, DRAMSim2:

```
{
  "num_chans": {
    "type": "int",
    "default": 1,
    "label": "Number of Channels",
    "validation": "power of 2",
    "description": "The number of *logically independent* channels. That is, each with a separate
                    memory controller. Should be a power of 2."
  },
  "jedec_data_bus_bits": {
    "type": "int",
    "default": 64,
    "label": "JEDEC Data Bus Bitsize",
    "description": "Always 64 for DDRx. If you want multiple *ganged* channels, set this to N*64"
  },

  ...

  "Debugging Parameters": {
    "debug_trans_q": {
      "type": "boolean",
      "default": "false",
      "label": "Output Transaction Queue Debugging Statements"
    },
    "debug_cmd_q": {
      "type": "boolean",
      "default": "false",
      "label": "Output Command Queue Debugging Statements"
    },

    ...

  }
}
```

Produces the following form:

![OCCAM describes simulation configuration in an easy-to-use form](/images/about-config-sim.png)

By describing configurations in this manner, we can define our simulator input from the web interface easily and in a consistent manner across simulators. We can more easily avert costly input errors typical with yet-another-arbitrary-format. The web interface can even alert you to validation errors before queuing and running the simulator.

Also, defining the schema in a uniform way also allows researchers to take advantage of intelligent agents which can sweep parameters and discover new results. For an instance of novelty, one can take the idle time of a machine and dedicate it to such a task.

All the the simulator writers (or volunteer third-parties) have to provide is the input schema noted above and a script to turn that into the configuration input canonical to the simulator. (Simulators may skip this step if they can read <span class='logo'>Occam</span> input directly) For example, the following python script is provided for DRAMSim2 alongside the description and input schema. It turns the json input given by the configuration form into a system.ini:

```
import json

input_file = open('system.ini', 'w')
data = json.load(open('input.json'))

for k, v in data.items():
  input_file.write(k.upper()+"="+str(v)+"\n")
```

Short and sweet. Not much is required to wrap a simulator in our system, but the gains are numerous.

## Building and Running

One of the painful aspects of running simulations is, much like the rest of the software world, getting the software actually running.

<span class='logo'>Occam</span> wrappers standardize the method of building, installing, and eventually running the simulator software. Simulators are installed in a system path and cannot be mutated by running experiments. They are initially built and then placed in this path by a builder and installer task.

Note how in the DRAMSim2 description above, there is a `git` field. This tells <span class='logo'>Occam</span> where to find the source code for the simulator. It pulls the code from here along with the description files which also contain a build script.

It simply queues the build like it would for an experiment, and when the machine has idle time, it starts a build process. The scripts can be rather complicated, but for DRAMSim2, it is fortunately simple:

```
cd ..
make
```

This is the exception and not the rule. Other simulators may require the building of dependencies and rather interesting environments in which they build or run. <span class='logo'>Occam</span> serves to eliminate the hassle and provide a system that simply works. DRAMSim2 is installed and ready within 20 seconds after pressing "Import" and "Create" on the new simulator form noted above.

## Interactive Results

![Results graphs are generated for each experiment and are interactive to allow graphical exploration of data](/images/about-results-graph.png)
![Simulation results are tabulated in a pleasant format for browsing](/images/about-results-table.png)
