This directory contains the rendering logic for the entire site. Each file, denoted as `haml`, represents HTML using a middleware markup language called Haml ([Documentation](http://haml.info/)). The views are subdivided much like controllers by category loosely related to a model.

Some files are simply fragments which are meant to be mixed into other pages. These are called **partials**. These files start with an underscore (`_`) such as "jobsa/\_list.haml" which is a fragment that represents the HTML for listing one job among many. The files follow a naming convention: (let's say we are looking at an Experiment)

* `show.haml` - The page that shows one Experiment in particular.
* `index.haml` - The page that shows all Experiments.
* `new.haml` - The page that renders the form to create/edit an Experiment.
* `_list.haml` - The partial that renders a list item (<li>) for one Experiment. Some other page will call this in a loop within a <ul> or <ol>

There are obviously other pages beyond these standard pages.

The sub-categories are as follows:

* `admin` - Administrative pages
* `experiments` - Pages/partials related to Experiments.
* `groups` - Pages/partials related to Groups.
* `import` - Pages relating to importing objects
* `index` - The front page
* `jobs` - Partials related to jobs
* `objects` - Pages/partials related to any generic Object.
* `people` - Pages/partials related to representing a Person.
* `results` - Pages/partials related to displaying/graphing results.
* `search` - Pages/partials related to searching objects.
* `sessions` - Pages/partials related to logging in/out etc.
* `static` - Static content (markdown usually)
* `stylesheets` - CSS content (written in SASS)
* `system` - Pages/partials related to general system information/statistics.
* `workflows` - Pages/partials related to the Workflow Widget.
* `worksets` - Pages/partials related to Worksets.
