class Occam
  # List all worksets
  get '/worksets' do
  end

  # Create a new workset
  post '/worksets' do
    status 404 and return if current_person.nil?

    if params["forked_from"]
      forked_from = Workset.find_by(:id => params["forked_from"].to_i)
    end

    workset_id = current_person.addWorkset(params["name"])
    # TODO: errors

    redirect "/worksets/#{workset_id}"
  end

  # Retrieve a list of tags for autocomplete
  get '/worksets/tags' do
    content_type 'application/json'
    Occam::Workset.all_tags_json(params["term"])
  end

  # Redirect to the current revision of a certain workset
  get '/worksets/:uuid' do
    workset = Workset.find_by(:uid => params[:uuid])
    uuid    = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif (workset.can_review? && params.has_key?("review")) || workset.can_view?(current_person)
      # Redirect to latest/base revision
      actual_revision = workset.revision

      if params["revision"]
        workset.revision = params["revision"]
      end

      full_revision = workset.fullRevision

      if full_revision.nil?
        status 404
      end

      revision = full_revision

      object_info = workset.objectInfo

      if params.has_key?("review")
        object_info.delete 'authors'
        object_info.delete 'collaborators'
        object_info.delete 'citation'
        object_info.delete 'organization'
        object_info.delete 'website'
      end

      render :haml, :"worksets/show", :locals => {
        :reviewing     => params.has_key?("review"),
        :object_info   => object_info,
        :current_revision => actual_revision,
        :head_view     => true,
        :local         => false,
        :local_links   => workset.local_links(current_person),
        :revision      => revision,
        :help          => params["help"],
        :person        => workset.authors.first,
        :workset       => workset,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => workset.authors,
        :collaborators => workset.collaborators,
        :graphs        => [],
        :groups        => workset.groups,
        :experiments   => workset.experiments
      }
    else
      # Unauthorized
      status 404
    end
  end

  get '/worksets/:uuid/:revision/json' do
    workset  = Workset.find_by(:uid => params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_view?(current_person)
      if params["revision"]
        workset.revision = params["revision"]
      end

      object_info = workset.objectInfo
      content_type "application/json"
      object_info.to_json
    else
      # Unauthorized
      status 404
    end
  end

  delete '/worksets/:uuid/:revision/review-links/:id' do
    workset  = Workset.find_by(:uid => params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      if params["revision"]
        workset.revision = params["revision"]
      end

      review = ReviewCapability.find_by(:id => params[:id])
      if review.nil?
        status 404
      else
        review.destroy

        redirect "/worksets/#{workset.uid}/#{workset.revision}"
      end
    else
      # Unauthorized
      status 404
    end
  end

  get '/worksets/:uuid/:revision' do
    workset  = Workset.find_by(:uid => params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif (workset.can_review?(params["revision"]) && params.has_key?("review")) || workset.can_view?(current_person)
      actual_revision = workset.revision

      if params["revision"]
        workset.revision = params["revision"]
      end

      object_info = workset.objectInfo

      if params.has_key?("review")
        object_info.delete 'authors'
        object_info.delete 'collaborators'
        object_info.delete 'citation'
        object_info.delete 'organization'
        object_info.delete 'website'
      end

      render :haml, :"worksets/show", :locals => {
        :reviewing     => params.has_key?("review"),
        :current_revision => actual_revision,
        :object_info   => object_info,
        :head_view     => false,
        :local         => false,
        :local_links   => workset.local_links(current_person),
        :help          => params["help"],
        :person        => workset.authors.first,
        :revision      => revision,
        :workset       => workset,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => workset.authors,
        :collaborators => workset.collaborators,
        :graphs        => [],
        :groups        => workset.groups,
        :experiments   => workset.experiments
      }
    else
      # Unauthorized
      status 404
    end
  end

  get '/worksets/:uuid/:revision/edit' do
    workset  = Workset.find_by(:uid => params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_view?(current_person)
      if params["revision"]
        workset.revision = params["revision"]
      end

      object_info = workset.objectInfo

      render :haml, :"worksets/new", :locals => {
        :errors        => nil,
        :object_info   => object_info,
        :head_view     => false,
        :local         => false,
        :local_links   => workset.local_links(current_person),
        :help          => params["help"],
        :person        => workset.authors.first,
        :revision      => revision,
        :workset       => workset,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => workset.authors,
        :collaborators => workset.collaborators,
        :graphs        => [],
        :groups        => workset.groups,
        :experiments   => workset.experiments
      }
    else
      # Unauthorized
      status 404
    end
  end

  get '/worksets/:uuid/:revision/fork' do
    workset  = Workset.find_by(:uid => params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_view?(current_person)
      if params["revision"]
        workset.revision = params["revision"]
      end

      object_info = workset.objectInfo

      render :haml, :"worksets/fork", :locals => {
        :errors        => nil,
        :object_info   => object_info,
        :head_view     => false,
        :local         => false,
        :local_links   => workset.local_links(current_person),
        :help          => params["help"],
        :person        => workset.authors.first,
        :revision      => revision,
        :workset       => workset,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => workset.authors,
        :collaborators => workset.collaborators,
        :graphs        => [],
        :groups        => workset.groups,
        :experiments   => workset.experiments
      }
    else
      # Unauthorized
      status 404
    end
  end

  post '/worksets/:uuid/:revision/fork' do
    workset  = Workset.find_by(:uid => params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_view?(current_person)
      if params["revision"]
        workset.revision = params["revision"]
      end

      workset = workset.fork(params["name"], current_person)

      #redirect "/people/#{current_person.uid}"
      redirect "/worksets/#{workset.uid}"
    else
      # Unauthorized
      status 404
    end
  end

  post '/worksets/:uuid/:revision/groups' do
    workset  = Workset.find_by(:uid => params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      object_info = workset.objectInfo

      if workset.revision == params["revision"]
        redirectBase = true
      else
        redirectBase = false
      end

      if params["revision"]
        workset.revision = params["revision"]
      end

      revisions = workset.addGroup(params["name"])

      workset_revision = revisions[-1]

      if redirectBase
        redirect "/worksets/#{uuid}"
      else
        redirect "/worksets/#{uuid}/#{workset_revision}"
      end
    else
      # Unauthorized
      status 404
    end
  end

  post '/worksets/:uuid/:revision/experiments' do
    workset  = Workset.find_by(:uid => params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      object_info = workset.objectInfo

      if workset.revision == params["revision"]
        redirectBase = true
      else
        redirectBase = false
      end

      if params["revision"]
        workset.revision = params["revision"]
      end

      revisions = workset.addExperiment(params["name"])
      revisions = revisions.split("\n")

      workset_revision = revisions[0]
      experiment_id = revisions[-1]

      if redirectBase
        redirect "/worksets/#{uuid}/experiments/#{experiment_id}"
      else
        redirect "/worksets/#{uuid}/#{workset_revision}/experiments/#{experiment_id}"
      end
    else
      # Unauthorized
      status 404
    end
  end

  # Add collaborator to an existing workset
  post '/worksets/:id/collaborators' do
    workset = Workset.find_by(:id => params["id"])

    if workset
      person = Person.find_by(:username => params["username"])

      if person
        if workset.can_edit?(current_person)
          workset.people << person
          workset.save
        end

        redirect "/worksets/#{workset.id}"
      else
        status 404
      end
    else
      status 404
    end
  end

  # Remove a collaborator from this workset
  # Allowed by: creator and person owner
  get '/worksets/:id/collaborators/:person_id/remove' do
    workset = Workset.find_by(:id => params["id"])

    if workset
      person = Person.find_by(:id => params["person_id"])

      if person
        workset.people.delete(person)

        redirect "/worksets/#{workset.id}"
      else
        status 404
      end
    else
      status 404
    end
  end

  # Get a list of all worksets
  get '/worksets' do
    if params["tag"]
      worksets = Workset.with_tag params["tag"]
    else
      worksets = Workset.all
    end

    render :haml, :"worksets/index", :locals => {
      :worksets => worksets,
      :tags     => Workset.all_tags,
      :tag      => params["tag"]
    }
  end

  # Form to create a new workset
  get '/worksets/new' do
    render :haml, :"worksets/new", :locals => {
      :errors      => nil,
      :forked_from => nil,
      :workset     => Workset.new,
      :simulator   => nil,
      :simulators  => Object.names(:simulator)
    }
  end

  # Edit an existing workset
  post '/worksets/:uuid' do
    workset = Workset.find_by(:uid => params[:uuid])

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      if params.has_key? "private"
        workset.private = params["private"].to_i
        workset.save
      end
      if params.has_key? "review"
        workset.revision = params["review"]
        workset.allow_review(current_person)
      end
      fields = params.select { |k,_| ["description", "name", "tags", "private"].include? k }
      unless fields.empty?
        workset.update_info fields
        workset.save
      end
      redirect "/worksets/#{workset.uid}/#{workset.revision}"
    else
      # Unauthorized
      status 404
    end
  end
end
