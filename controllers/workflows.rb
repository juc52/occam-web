class Occam
  # Add object to workflow
  post '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/connections' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]

    if params["revision"]
      experiment.revision = params["revision"]
    end

    connection_index = -1
    if params["connection_index"]
      connection_index = params["connection_index"].to_i
    end

    workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
    if workset.nil?
      status 404
      return
    end

    object_id = params["object"]
    created_object_type = nil
    created_object_group = nil
    if object_id
      split_index = object_id.rindex('!')
      if split_index
        object_id = params["object"][split_index+1..-1].to_i
        created_object_type = params["object"][0..split_index-1]
      else
        object_id = object_id.to_i
      end
    else
      # Creating new object
      created_object_type = params["object_type"]
    end

    if experiment.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      object_info = experiment.objectInfo

      if experiment.revision == revision
        redirectBase = true
      else
        redirectBase = false
      end

      if workset.revision == params[:workset_revision]
        redirectWorksetBase = true
      else
        redirectWorksetBase = false
      end

      workset.revision = params[:workset_revision]

      obj = nil
      if object_id
        obj = Occam::Object.where(:id => object_id).first
        if obj.nil?
          status 404
          return
        end
      end

      experiment.attach(obj, workset, connection_index, created_object_type, created_object_group)

      redirect_url = "/worksets/"

      if redirectWorksetBase
        redirect_url << "#{workset.uid}/"
      else
        redirect_url << "#{workset.uid}/#{workset.revision}/"
      end

      redirect_url << "experiments/"

      if redirectBase
        redirect_url << uuid
      else
        redirect_url << "#{uuid}/#{revision}"
      end

      redirect(redirect_url)
    else
      # Unauthorized
      status 406
    end
  end

  delete '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/connections/:connection_id' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]

    if params["revision"]
      experiment.revision = params["revision"]
    end

    connection_index = params[:connection_id].to_i

    workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
    if workset.nil?
      status 404
      return
    end

    if experiment.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      object_info = experiment.objectInfo

      if experiment.revision == revision
        redirectBase = true
      else
        redirectBase = false
      end

      if workset.revision == params[:workset_revision]
        redirectWorksetBase = true
      else
        redirectWorksetBase = false
      end

      workset.revision = params[:workset_revision]

      experiment.detach(workset, connection_index)

      redirect_url = "/worksets/"

      if redirectWorksetBase
        redirect_url << "#{workset.uid}/"
      else
        redirect_url << "#{workset.uid}/#{workset.revision}/"
      end

      redirect_url << "experiments/"

      if redirectBase
        redirect_url << uuid
      else
        redirect_url << "#{uuid}/#{revision}"
      end

      redirect(redirect_url)
    else
      # Unauthorized
      status 406
    end
  end
end
