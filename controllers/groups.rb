class Occam
  # List all groups
  get '/groups' do
  end

  get '/worksets/:workset_uuid/groups/:uuid' do
    group = Group.find_by(:uid => params[:uuid])
    uuid  = params[:uuid]

    if group.nil?
      status 404
    # Authorize
    elsif group.can_view?(current_person)
      # Redirect to latest/base revision
      system = Occam::System.first
      code = uuid[uuid.length - 36..-1]

      full_revision = group.fullRevision

      if full_revision.nil?
        status 404
      end

      revision = full_revision

      object_info = group.objectInfo

      workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
      if workset.nil?
        status 404
        return
      end

      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          #workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
        end
      end

      render :haml, :"groups/show", :locals => {
        :object_info   => object_info,
        :head_view     => true,
        :local         => false,
        :local_links   => workset.local_links(current_person),
        :help          => params["help"],
        :revision      => revision,
        :person        => group.authors.first,
        :workset       => workset,
        :group         => group,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => [],
        :collaborators => [],
        :graphs        => [],
        :groups        => [],
        :experiments   => []
      }
    else
      # Unauthorized
      status 406
    end
  end

  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid' do
    group = Group.find_by(:uid => params[:uuid])
    uuid  = params[:uuid]

    if group.nil?
      status 404
    # Authorize
    elsif group.can_view?(current_person)
      # Redirect to latest/base revision
      system = Occam::System.first
      code = uuid[uuid.length - 36..-1]

      full_revision = group.fullRevision

      if full_revision.nil?
        status 404
      end

      revision = full_revision

      object_info = group.objectInfo

      workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
      if workset.nil?
        status 404
        return
      end

      workset.revision = params[:workset_revision]

      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          #workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
        end
      end

      render :haml, :"groups/show", :locals => {
        :object_info   => object_info,
        :head_view     => false,
        :local         => false,
        :local_links   => workset.local_links(current_person),
        :help          => params["help"],
        :revision      => revision,
        :person        => group.authors.first,
        :workset       => workset,
        :group         => group,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => [],
        :collaborators => [],
        :graphs        => [],
        :groups        => [],
        :experiments   => []
      }
    else
      # Unauthorized
      status 406
    end
  end

  post '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/groups' do
    group    = Group.find_by(:uid => params[:uuid])
    uuid     = params[:uuid]
    revision = params[:revision]

    workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
    if workset.nil?
      status 404
      return
    end

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      object_info = workset.objectInfo

      if group.revision == revision
        redirectBase = true
      else
        redirectBase = false
      end

      if workset.revision == params[:workset_revision]
        redirectWorksetBase = true
      else
        redirectWorksetBase = false
      end

      workset.revision = params[:workset_revision]

      if revision
        group.revision = params["revision"]
      end

      group.addGroup(workset, params["name"])

      redirect_url = "/worksets/"

      if redirectWorksetBase
        redirect_url << "#{workset.uid}/"
      else
        redirect_url << "#{workset.uid}/#{workset.revision}/"
      end

      redirect_url << "groups/"

      if redirectBase
        redirect_url << uuid
      else
        redirect_url << "#{uuid}/#{revision}"
      end

      redirect(redirect_url)
    else
      # Unauthorized
      status 406
    end
  end

  post '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/experiments' do
    group    = Group.find_by(:uid => params[:uuid])
    uuid     = params[:uuid]
    revision = params[:revision]

    workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
    if workset.nil?
      status 404
      return
    end

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      object_info = workset.objectInfo

      if group.revision == revision
        redirectBase = true
      else
        redirectBase = false
      end

      if workset.revision == params[:workset_revision]
        redirectWorksetBase = true
      else
        redirectWorksetBase = false
      end

      if revision
        group.revision = params["revision"]
      end

      workset.revision = params[:workset_revision]

      revisions = group.addExperiment(workset, params["name"])

      workset_revision = revisions[-1]

      redirect_url = "/worksets/"

      if redirectWorksetBase
        redirect_url << "#{workset.uid}/"
      else
        redirect_url << "#{workset.uid}/#{workset.revision}/"
      end

      redirect_url << "groups/"

      if redirectBase
        redirect_url << uuid
      else
        redirect_url << "#{uuid}/#{revision}"
      end

      redirect(redirect_url)
    else
      # Unauthorized
      status 406
    end
  end

  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision' do
    group    = Group.find_by(:uid => params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if group.nil?
      status 404
    # Authorize
    elsif group.can_view?(current_person)
      object_info = group.objectInfo

      if params["revision"]
        group.revision = params["revision"]
      end

      workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
      if workset.nil?
        status 404
        return
      end

      workset.revision = params[:workset_revision]

      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
        end
      end

      render :haml, :"groups/show", :locals => {
        :object_info   => object_info,
        :head_view     => false,
        :local         => false,
        :local_links   => workset.local_links(current_person),
        :help          => params["help"],
        :person        => group.authors.first,
        :revision      => revision,
        :workset       => workset,
        :group         => group,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => [],
        :collaborators => [],
        :graphs        => [],
        :groups        => [],
        :experiments   => []
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Redirect to the current revision for the given group
  get '/groups/:uuid' do
    group = Group.find_by(:uid => params[:uuid])
    uuid  = params[:uuid]

    if group.nil?
      status 404
    # Authorize
    elsif group.can_view?(current_person)
      # Redirect to latest/base revision
      system = Occam::System.first
      if uuid[-9] == '-'
        code = uuid[uuid.length - 36 - 9..-1]
      else
        code = uuid[uuid.length - 36..-1]
      end

      full_revision = group.fullRevision

      if full_revision.nil?
        status 404
      end

      revision = full_revision

      object_info = group.objectInfo

      workset = nil
      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        if belongs_to[-9] == '-'
          code = belongs_to[belongs_to.length - 36 - 9..-1]
          type = belongs_to[0..-(38+9)]
        else
          code = belongs_to[belongs_to.length - 36..-1]
          type = belongs_to[0..-38]
        end

        if type == 'workset'
          workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
        end
      end

      render :haml, :"groups/show", :locals => {
        :object_info   => object_info,
        :local         => false,
        :local_links   => workset.local_links(current_person),
        :help          => params["help"],
        :revision      => revision,
        :person        => group.authors.first,
        :workset       => workset,
        :group         => group,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => [],
        :collaborators => [],
        :graphs        => [],
        :groups        => [],
        :experiments   => []
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Show the given group at the given revision
  get '/groups/:uuid/:revision' do
    group    = Group.find_by(:uid => params[:uuid])
    revision = params[:revision]
    uuid     = params[:uuid]

    if params["revision"]
      group.revision = params["revision"]
    end

    if group.nil?
      status 404
    # Authorize
    elsif group.can_view?(current_person)
      object_info = group.objectInfo

      workset = nil
      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        if belongs_to[-9] == '-'
          code = belongs_to[belongs_to.length - 36 - 9..-1]
          type = belongs_to[0..-(38+9)]
        else
          code = belongs_to[belongs_to.length - 36..-1]
          type = belongs_to[0..-38]
        end

        if type == 'workset'
          workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
        end
      end

      render :haml, :"groups/show", :locals => {
        :object_info   => object_info,
        :local         => false,
        :local_links   => workset.local_links(current_person),
        :help          => params["help"],
        :person        => group.authors.first,
        :revision      => revision,
        :workset       => workset,
        :group         => group,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => [],
        :collaborators => [],
        :graphs        => [],
        :groups        => [],
        :experiments   => []
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Form to generate a new group
  get '/worksets/:id/groups/new' do
    workset = Occam::Workset.find_by(:id => params[:id].to_i)

    if workset.nil?
      status 404
    elsif workset.can_edit?(current_person)
      workflow = workset.workflow

      if workflow.nil?
        status 404
      else
        render :haml, :"groups/new", :locals => {
          :errors      => nil,
          :forked_from => nil,
          :experiment  => Experiment.new,
          :recipe      => nil,
          :workset     => workset,
          :workflow    => workflow
        }
      end
    else
      status 406
    end
  end

  # Launch graph builder for this collection of data
  get '/worksets/:id/groups/:group_id/graphs/new' do
    group   = Occam::Group.find_by(:id => params[:group_id])
    workset = Occam::Workset.find_by(:id => params[:id])

    if workset.nil? or group.nil?
      status 404
    else
      if group.workset_id == workset.id && workset.can_edit?(current_person)
        experiments = group.experiments.select{|e| !e.results.nil?}
        experiment = experiments.first

        object = workset.workflow.tail_connections.first.object
        output = experiment.results

        render :haml, :"results/graph_builder", :locals => {
          :no_footer   => true,
          :experiments => experiments,
          :workset     => workset,
          :object      => object,
          :schema      => object.output_schema,
          :data        => output["data"] || {},
          :errors      => output["errors"] || [],
          :warnings    => output["warnings"] || [],
        }
      else
        status 406
      end
    end
  end

  # View all results
  get '/worksets/:id/groups/:group_id/results' do
    group   = Occam::Group.find_by(:id => params[:group_id])
    workset = Occam::Workset.find_by(:id => params[:id])

    if workset.nil? or group.nil?
      status 404
    else
      if workset.can_view?(current_person)
        experiments = group.experiments.select{|e| !e.results.nil?}
        experiment = experiments.first

        object = experiments.first.workset.workflow.tail_connections.first.object
        output = experiment.results

        render :haml, :"results/show", :locals => {
          :no_footer   => true,
          :experiments => experiments,
          :object      => object,
          :schema      => object.outputs.first.schema,
          :data        => output["data"] || {},
          :errors      => output["errors"] || [],
          :warnings    => output["warnings"] || [],
        }
      else
        status 406
      end
    end
  end

  get '/worksets/:id/groups/:group_id/cancel' do
    group   = Occam::Group.find_by(:id => params[:group_id])
    workset = Occam::Workset.find_by(:id => params[:id])

    if workset.nil? or group.nil?
      status 404
    else
      if workset.can_edit?(current_person)
        experiments = group.experiments

        experiments.each do |experiment|
          experiment.cancel
        end

        if request.preferred_type('text/html')
          # Redirect to the workset page (if asking for html)
          redirect "/worksets/#{workset.id}/groups/#{group.id}"
        elsif request.preferred_type('application/json')
          # If asking for json, return success or fail
          content_type 'application/json'
          {
            :result => "success"
          }.to_json
        else
          status 404
        end
      else
        status 406
      end
    end
  end

  # Run all experiments in this group
  get '/worksets/:id/groups/:group_id/run' do
    group   = Occam::Group.find_by(:id => params[:group_id])
    workset = Occam::Workset.find_by(:id => params[:id])

    if workset.nil? or group.nil?
      status 404
    else
      if workset.can_edit?(current_person)
        experiments = group.experiments

        experiments.each do |experiment|
          experiment.run
        end

        if request.preferred_type('text/html')
          # Redirect to the workset page (if asking for html)
          redirect "/worksets/#{workset.id}/groups/#{group.id}"
        elsif request.preferred_type('application/json')
          # If asking for json, return success or fail
          content_type 'application/json'
          {
            :result => "success"
          }.to_json
        else
          status 404
        end
      else
        status 406
      end
    end
  end

  # Show a group
  get '/worksets/:id/groups/:group_id' do
    group   = Occam::Group.find_by(:id => params[:group_id])
    workset = Occam::Workset.find_by(:id => params[:id])

    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    if workset && group
      if workset.can_view?(current_person)
        case format
        when 'text/html'
          workflow    = nil #group.workflow

          render :haml, :"groups/show", :locals => {
            :help          => params["help"],
            :workset       => workset,
            :group         => group,
            :forked_from   => nil, #group.forked_from,
            :forked_us     => nil, #group.forked_us,
            :person        => group.authors.first,
            :authors       => group.authors,
            :collaborators => workset.collaborators,
            :experiments   => group.experiments,
            :workflow      => workflow
          }
        when 'application/json'
          # Return object metadata
          content_type 'application/json'
          hash = group.serializable_hash
          hash[:experiments] = group.experiments.map(&:serializable_hash)
          hash.to_json
        end
      else
        status 406
      end
    else
      status 404
    end
  end

  # Form to populate a group from an experiment

  # Create a new group
  post '/worksets/:id/groups' do
    workset = Workset.find_by(:id => params[:id])

    if workset.nil?
     status 404
    elsif workset.can_edit?(current_person)
      workflow = Occam::Workflow.new(:workset => workset)
      g = Group.create(:name          => params["name"],
                       :tags          => params["tags"],
                       :person       => current_person,
                       :forked_from   => nil,
                       :workset       => workset,
                       :workflow      => workflow,
                       :experiments   => [])

      if g.errors.any?
        render :haml, :"groups/new", :locals => {
          :errors      => g.errors,
          :forked_from => nil,
          :experiment  => Experiment.new,
          :recipe      => nil,
          :workset     => workset,
          :workflow    => workflow
        }
      else
        g.instantiate
        redirect "/worksets/#{workset.id}/groups/#{g.id}"
      end
    else
      status 406
    end
  end

  # Form to edit group to add configuration range
  get '/worksets/:id/groups/:group_id/add_range' do
    group   = Occam::Group.find_by(:id => params[:group_id].to_i)
    workset = Workset.find_by(:id => params[:id])

    if group.nil? or workset.nil?
     status 404
    elsif workset.can_edit?(current_person)
      workflow = workset.workflow

      render :haml, :"groups/new_range", :locals => {
        :help        => params["help"],
        :errors      => nil,
        :forked_from => nil,
        :group       => group,
        :experiment  => Experiment.new,
        :recipe      => nil,
        :workset     => workset,
        :workflow    => workflow
      }
    else
      status 406
    end
  end

  # Edit group (add configuration, change name, etc)
  post '/worksets/:id/groups/:group_id' do
    workset  = Occam::Workset.find_by(:id => params[:id].to_i)
    group    = Occam::Group.find_by(:id => params[:group_id].to_i)
    workflow = group.workflow

    if params["data"]
      p params["data"]
      ConfigurationDocument.decode_base64(params["data"])

      original_config = params["data"].dup

      # Generate permutations upon configuration ranges

      # Find all fields with a range specified
      find_ranges = ConfigurationDocument.rake_ranges(params["data"])
      p find_ranges

      # Find all possible values for each range field
      exploded_ranges = ConfigurationDocument.form_range(find_ranges)
      p exploded_ranges

      # Get every combination of values
      x, *xs = *exploded_ranges

      if x.nil? # No ranges, just add a single experiment
        ConfigurationDocument.type_check(original_config)

        experiments = [
          Experiment.new(:name          => params["name"],
                         :tags          => params["tags"],
                         :person       => current_person,
                         :forked_from   => nil,
                         :recipe        => nil,
                         :workset       => workset,
                         :workflow      => workflow,
                         :configuration => original_config)]
      else
        configs = x.product(*xs)

        # Each experiment gets an identifier attached
        i = 1

        # For each configuration, merge with the one given, and then type check them
        # and create the experiment
        experiments = configs.map do |config|
          # 'config' here is an array of hashes, which only have one key: the
          # parameter that was ranged. It contains a possible value for it.

          # Each hash in the array is another ranged value:
          # [ { "num_chans" => 1 }, { "jedec_data_bus_bits" => 64 } ]

          # Now, merge this with the configuration given to us.
          name = "#{params["name"]}"
          config.each do |value|

            def deep_merge(first, second)
              merger = proc { |key, v1, v2| Hash === v1 && Hash === v2 ? v1.merge(v2, &merger) : v2 }
              first.merge(second, &merger)
            end

            params["data"] = deep_merge(params["data"], value["hash"])
            key = value["name"].split('.').last
            name = "#{name}-#{key}-#{value["value"]}"
          end

          p params["data"]
          ConfigurationDocument.type_check(params["data"])

          i += 1

          Experiment.new :name          => name,
                         :tags          => params["tags"],
                         :person       => current_person,
                         :forked_from   => nil,
                         :recipe        => nil,
                         :workset       => workset,
                         :workflow      => workflow,
                         :configuration => params["data"]
        end
      end

      group.experiments = experiments
      group.configuration = original_config
      group.save
    end

    redirect "/worksets/#{workset.id}/groups/#{group.id}"
  end
end
