The `controllers` directory contains a collection of routines that govern how actions executed via URL routes are handled. We generally split up the controllers by category and those categories generally correspond to a model.
That is, when "/worksets/workset-9ea21478-20df-11e5-a404-001fd05bb228" is typed in the browser, it will, by our convention, invoke the code in `worksets.rb` indicated for that route. Routes in OCCAM are a little more complicated than general web applications. For instance, "/worksets/workset-9ea21478-20df-11e5-a404-001fd05bb228/experiments/experiment-a7ca6e88-20df-11e5-91bb-001fd05bb228" is a long route that will resolve for "/worksets/:workset_uuid/experiments/:uuid" in `experiments.rb`. So, experiments and groups are in their own files for convenience even though they are invoked through worksets. This is because they are hierarchically structured to belong to worksets.

* `admin.rb` - Administrative routes and pages.
* `experiments.rb` - Pages/routes related to Experiment objects.
* `groups.rb` - Pages/routes related to Group objects.
* `objects.rb` - Pages/routes related to any generic Object.
* `people.rb` - Pages/routes related to People.
* `results.rb` - Pages/routes related to data viewing and graph building.
* `runs.rb` - Pages/routes related to Run records.
* `search.rb` - Pages/routes related to searching.
* `session.rb` - Pages/routes related to sessions (logging in, logging out, etc)
* `static.rb` - Pages/routes that map directly to static pages. (404, 500, markdown pages, etc)
* `system.rb` - Pages/routes that relate to system properties or statistics.
* `workflows.rb` - Pages/routes that relate to Experiment workflow management.
* `worksets.rb` - Pages/routes related to Workset objects.
