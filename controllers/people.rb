class Occam
  # Get a list of all people
  get '/people' do
    people = Occam::Person.all
    render :haml, :"people/index", :locals => {
      :people => people
    }
  end

  # Create a new person
  post '/people' do
    person = Occam::Person.create :username => params["username"],
                                  :password => params["password"],
                                  :roles    => :experimentalist

    # Sign in
    new_session = person.activate_session
    session[:person_id] = person.id
    session[:nonce] = new_session.nonce

    redirect "/people/#{person.uid}"
  end

  # Form to create a new person
  get '/people/new' do
    render :haml, :"people/new", :locals => {:errors => nil}
  end

  # Update person/profile information or activate
  post '/people/:uuid' do
    person = Occam::Person.find_by(:uid => params[:uuid])

    if person.nil?
      status 404
    else
      status 406 and return if current_person.nil?

      columns = {}
      if current_person.id == person.id
        # Normal people can change their username, etc
        columns = params.select { |k,v| [
            "username",
            "email",
            "bio",
            "name",
            "organization"
          ].include?(k) }
      elsif current_person.has_role? :administrator
        # Administrators can set "active"
        columns = params.select { |k,_| [
            "active",
          ].include?(k) }
      else
        status 406
      end

      unless columns.empty?
        person.update_columns columns
        person.save
      end

      if current_person == person
        redirect "/people/#{person.uid}"
      elsif current_person.has_role? :administrator
        redirect "/admin"
      end
    end
  end

  # Retrieve a specific person page
  get '/people/:uuid' do
    person = Occam::Person.find_by(:uid => params[:uuid])
    experiments = Occam::Experiment.where(:person_id => params[:uuid])
    jobs = Occam::Job.where(:experiment_id => experiments.map(&:id))

    if person.nil?
      status 404
    else
      render :haml, :"people/show", :locals => {
        :errors         => nil,
        :person         => person,
        :worksets       => person.worksets,
        :collaborations => person.collaborations,
        :experiments    => experiments,
        :jobs           => jobs,
      }
    end
  end

  # Form to edit an person's profile
  get '/people/:uuid/edit' do
    person = Occam::Person.find_by(:uid => params[:uuid])

    if person.nil?
      status 404
    else
      if current_person.id != person.id
        status 406
      else
        render :haml, :"people/edit", :locals => {
          :errors  => nil,
          :person => current_person
        }
      end
    end
  end

  # Show all Runs for this Person
  get '/people/:uuid/runs' do
    person = Occam::Person.find_by(:uid => params[:uuid])

    if person.nil? || current_person.nil? || person.id != current_person.id
      status 404
    else
      render :haml, :"people/runs", :locals => {
        :errors => nil,
        :person => person
      }
    end
  end
end
