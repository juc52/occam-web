class Occam
  require 'base64'

  # List all object types
  get '/objects' do
    # We can query by type
    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    case format
    when 'text/html'
      # TODO: render
    when 'application/json'
      # Return object metadata
      content_type 'application/json'
      object_list = Occam::Object.all
      if params["by_type"]
        object_list = object_list.where(:object_type => params["by_type"])
      end
      object_list.to_json
    end
  end

  # List all objects of that type, if known
  get '/objects/:object_type' do
    object_type = params[:object_type]
    objects = Occam::Object.where(:object_type => object_type)

    if params["tag"]
      # TODO: sanitize?
      objects = objects.where('tags LIKE ?', "%;#{params['tag']};%")
    end

    render :haml, :"objects/type", :locals => {
      :object_type => object_type,
      :objects => objects
    }
  end

  # List information about the particular object history (as json), if found
  get '/objects/:object_type/:uuid.json' do
    object_type = params[:object_type]
    uuid        = params[:uuid]

    if object_type == "workset"
      return redirect "/worksets/#{uuid}"
    elsif object_type == "experiment"
      return redirect "/experiments/#{uuid}"
    elsif object_type == "group"
      return redirect "/groups/#{uuid}"
    end

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      # Redirect to latest/base revision
      full_revision = object.fullRevision

      if full_revision.nil?
        status 404
      end

      redirect "/objects/#{object_type}/#{uuid}/#{full_revision}.json"
    end
  end

  # List information about the particular object history, if found
  get '/worksets/:workset_uuid/objects/:uuid' do
    uuid        = params[:uuid]
    object_type = Occam::Object.objectTypeFromUUID(uuid)

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      # Redirect to latest/base revision
      code = uuid[uuid.length - 36..-1]

      object_path = object.path
      object_info = object.objectInfo

      workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
      if workset.nil?
        status 404
        return
      end

      render :haml, :"objects/show", :locals => {
        :object      => object,
        :object_info => object_info,
        :workset     => workset,
        :revision    => object.revision
      }
    end
  end

  # List information about the particular object history, if found
  get '/objects/:object_type/:uuid' do
    object_type = params[:object_type]
    uuid        = params[:uuid]

    if object_type == "workset"
      return redirect "/worksets/#{uuid}"
    elsif object_type == "experiment"
      return redirect "/experiments/#{uuid}"
    elsif object_type == "group"
      return redirect "/groups/#{uuid}"
    end

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      # Redirect to latest/base revision
      full_revision = object.fullRevision

      if full_revision.nil?
        status 404
      end

      # TODO: which redirect code?? (non-cached)
      redirect "/objects/#{object_type}/#{uuid}/#{full_revision}"
    end
  end

  # List information about the particular object history, if found
  get '/objects/:object_type/:uuid/schema' do
    object_type = params[:object_type]
    uuid        = params[:uuid]

    if object_type == "workset"
      return redirect "/worksets/#{uuid}"
    elsif object_type == "experiment"
      return redirect "/experiments/#{uuid}"
    elsif object_type == "group"
      return redirect "/groups/#{uuid}"
    end

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      # Redirect to latest/base revision
      full_revision = object.fullRevision

      if full_revision.nil?
        status 404
      end

      # TODO: which redirect code?? (non-cached)
      redirect "/objects/#{object_type}/#{uuid}/#{full_revision}/schema"
    end
  end

  # List information about the particular object history, if found
  get '/objects/:object_type/:uuid/file' do
    object_type = params[:object_type]
    uuid        = params[:uuid]

    if object_type == "workset"
      return redirect "/worksets/#{uuid}/file"
    elsif object_type == "experiment"
      return redirect "/experiments/#{uuid}/file"
    elsif object_type == "group"
      return redirect "/groups/#{uuid}/file"
    end

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      # Redirect to latest/base revision
      full_revision = object.fullRevision

      if full_revision.nil?
        status 404
      end

      # TODO: which redirect code?? (non-cached)
      redirect "/objects/#{object_type}/#{uuid}/#{full_revision}/file"
    end
  end

  # Retrieve the full revision for this partial revision
  get '/objects/:object_type/:uuid/:revision/full_revision' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    if params[:object_type] == "workset"
      object = Occam::Workset.where(:uid => uuid).first
    elsif params[:object_type] == "person"
      object = Occam::Person.where(:uid => uuid).first
    elsif params[:object_type] == "group"
      object = Occam::Group.where(:uid => uuid).first
    elsif params[:object_type] == "experiment"
      object = Occam::Experiment.where(:uid => uuid).first
    else
      object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first
    end

    if object.nil?
      status 404
    else
      object.revision = revision

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)
      data = object.fullRevision

      if not data
        status 404
      else
        content_type "application/json"
        {
          "revision" => data
        }.to_json
      end
    end
  end

  # Retrieve the parent revision for this object
  get '/objects/:object_type/:uuid/:revision/parent_revision' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    if params[:object_type] == "workset"
      object = Occam::Workset.where(:uid => uuid).first
    elsif params[:object_type] == "person"
      object = Occam::Person.where(:uid => uuid).first
    elsif params[:object_type] == "group"
      object = Occam::Group.where(:uid => uuid).first
    elsif params[:object_type] == "experiment"
      object = Occam::Experiment.where(:uid => uuid).first
    else
      object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first
    end

    if object.nil?
      status 404
    else
      object.revision = revision
      object_path = object.path

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)
      data = object.parentRevision

      if not data
        status 404
      else
        content_type "application/json"
        {
          "parentRevision" => data
        }.to_json
      end
    end
  end

  # Retrieve the child revisions for this object
  get '/objects/:object_type/:uuid/:revision/child_revisions' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    if params[:object_type] == "workset"
      object = Occam::Workset.where(:uid => uuid).first
    elsif params[:object_type] == "person"
      object = Occam::Person.where(:uid => uuid).first
    elsif params[:object_type] == "group"
      object = Occam::Group.where(:uid => uuid).first
    elsif params[:object_type] == "experiment"
      object = Occam::Experiment.where(:uid => uuid).first
    else
      object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first
    end

    if object.nil?
      status 404
    else
      object.revision = revision
      object_path = object.path

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)
      data = object.childRevisions

      if not data
        status 404
      else
        content_type "application/json"
        {
          "childRevisions" => data
        }.to_json
      end
    end
  end

  # Retrieve the file from the git repository for the object
  get '/objects/:object_type/:uuid/:revision/tree/*' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    if params[:object_type] == "workset"
      object = Occam::Workset.where(:uid => uuid).first
    elsif params[:object_type] == "person"
      object = Occam::Person.where(:uid => uuid).first
    elsif params[:object_type] == "group"
      object = Occam::Group.where(:uid => uuid).first
    elsif params[:object_type] == "experiment"
      object = Occam::Experiment.where(:uid => uuid).first
    else
      object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first
    end

    if object.nil?
      status 404
    else
      object.revision = revision
      object_path = object.path

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)
      # Also, allow this through the backend. So put this in the Object/etc model
      path = params[:splat].join('/')
      data = Occam::Git.new(object_path, revision).retrieveFile(path)

      if not data
        status 404
      else
        # Check extension
        case File.extname(path)
        when ".svg"
          content_type "image/svg+xml"
        when ".xml"
          content_type "text/xml"
        when ".json"
          content_type "application/json"
        else
          content_type "text/plain"
        end
        data
      end
    end
  end

  get '/objects/:object_type/:uuid/:revision.json' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      object_info = object.objectInfo

      content_type "application/json"
      object_info.to_json
    end
  end

  # Retrieves the file as structued JSON
  get '/objects/:object_type/:uuid/:revision/data' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      object_info = object.objectInfo

      # TODO: handle file type
      content_type "application/json"
      object.retrieveFile(object_info['file'])
    end
  end

  # Retrieves the file as structued JSON
  get '/objects/:object_type/:uuid/:revision/data/*' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      object_info = object.objectInfo

      # TODO: handle file type
      data = object.retrieveJSON(object_info['file'])

      # Retrieve field from splat
      params[:splat].first.split('/').each do |key|
        if data.is_a? Array
          data = data[key.to_i]
        else
          # Revert from base64

          # Add padding back
          if (key.length % 4) > 0
            key = key + ("=" * (4 - (key.length % 4)))
          end

          key = Base64.urlsafe_decode64(key)

          data = data[key]
        end

        if data.nil?
          status 404
          return
        end
      end

      content_type "application/json"
      data.to_json
    end
  end

  get '/objects/:object_type/:uuid/:revision/file' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      object_info = object.objectInfo

      # TODO: handle file type
      content_type "text/plain"
      object.retrieveFile(object_info['file'])
    end
  end

  get '/objects/:object_type/:uuid/:revision/schema' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      object_info = object.objectInfo

      # TODO: handle file type
      content_type "application/json"
      object.retrieveFile(object_info['schema'])
    end
  end

  # List information about the particular object at this revision, if found
  get '/objects/:object_type/:uuid/:revision' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    if object_type == "workset"
      return redirect "/worksets/#{uuid}/#{revision}"
    elsif object_type == "experiment"
      return redirect "/experiments/#{uuid}/#{revision}"
    elsif object_type == "group"
      return redirect "/groups/#{uuid}/#{revision}"
    end

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      object.revision = revision
      revision = object.fullRevision
      object.revision = revision
      object_info = object.objectInfo

      render :haml, :"objects/show", :locals => {
        :object      => object,
        :object_info => object_info,
        :revision    => revision
      }
    end
  end

  get '/objects/:object_type/:uuid/:revision/configurations' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:object_type => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      object.revision = revision
      info = object.objectInfo

      if info.empty?
        status 404
        return
      end

      if not info.has_key? 'configurations'
        status 404
      else
        configurations = info['configurations']
        configurations.map do |configuration|
          if configuration.has_key? 'schema'
            # Pull schema file from git
            configuration['schema'] = object.git.retrieveJSON(configuration['schema'])
          else
            configuration['schema'] = {}
          end
          configuration
        end

        render :haml, :"objects/configurations", :locals => {
          :revision       => params[:revision],
          :info           => info,
          :object         => object,
          :recipe         => nil,
          :configurations => configurations
        }
      end
    end
  end

  get '/objects/:object_type/:uuid/:revision/configurations/:configuration_id' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:object_type => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      object.revision = revision
      info = object.objectInfo

      if info.empty?
        status 404
        return
      end

      params[:configuration_id] = params[:configuration_id].to_i

      if not info.has_key? 'configurations' or params[:configuration_id] >= info['configurations'].length
        status 404
      else
        configuration = info['configurations'][params[:configuration_id]]
        if configuration.has_key? 'schema'
          # Pull schema file from git
          schema = object.git.retrieveJSON(configuration['schema'])
        else
          schema = {}
        end

        render :haml, :"objects/configuration", :locals => {
          :revision      => params[:revision],
          :info          => info,
          :object        => object,
          :recipe        => nil,
          :configuration => configuration,
          :schema        => schema,
        }
      end
    end
  end

  get '/objects/:object_type/:uuid/:revision/outputs/:output_id' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:object_type => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      info = object.objectInfo

      if info.empty?
        status 404
        return
      end

      params[:output_id] = params[:output_id].to_i

      if not info.has_key? 'outputs' or params[:output_id] >= info['outputs'].length
        status 404
      else
        output = info['outputs'][params[:output_id]]
        if output.has_key? 'schema'
          # Pull schema file from git
          schema = object.git.retrieveJSON(output['schema'])
        else
          schema = {}
        end

        render :haml, :"objects/output", :locals => {
          :revision      => params[:revision],
          :info          => info,
          :object        => object,
          :recipe        => nil,
          :output        => output,
          :schema        => schema,
        }
      end
    end
  end

  # Object builder
  get '/new' do
    object = Occam::Object.new
    render :haml, :"objects/new", :locals => {
      :help   => params["help"],
      :errors => object.errors,
      :object => object,
    }
  end

  # Object import
  get '/import' do
    render :haml, :"objects/import", :locals => {
      :errors      => nil,
      :default_url => ""
    }
  end

  post '/import' do
    if params.has_key? "url"
      url  = params["url"]

      json = Occam::Object.download_json(url)
      obj  = Occam::Object.import(url, json)
      deps = Occam::Object.all_unknown_dependencies(url, json)

      errors = obj.errors

      if obj.persisted?
        # Already exists, redirect to that page
        redirect "/objects/#{obj.object_type}/#{obj.uid}"
      else
        return render :haml, :"objects/import_confirm", :locals => {
          :errors       => obj.errors,
          :object       => obj,
          :dependencies => deps,
          :url          => url
        }
      end
    else
      status 404
    end
  end

  # TODO: This should do import and new?
  post '/objects' do
    object = Occam::Object.import_all(params["url"])

    if object.errors.any?
      status 422
      render :haml, :"objects/import", :locals => {
        :errors => object.errors,
        :default_url => params["url"]
      }
    else
      object.install
      object.build

      redirect "/objects/#{object.object_type_safe}/#{object.uid}"
    end
  end

  # List all (with some pagination) objects
  # Parameters:
  #   "by_type" => The type of object to filter the results.
  get '/objects' do
    # We can query by type
    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    case format
    when 'text/html'
      # TODO: remove the simulator/benchmark bake-ins. Just give a tree view.
      simulators     = Object.all_active.where(:object_type => 'simulator').limit(3)
      simulator_tags = Object.all_tags('simulator')

      benchmarks     = Object.all_active.where(:object_type => 'benchmark').limit(3)
      benchmark_tags = Object.all_tags('benchmark')

      render :haml, :"objects/index", :locals => {
        :simulators     => simulators,
        :simulator_tags => simulator_tags,
        :benchmarks     => benchmarks,
        :benchmark_tags => benchmark_tags,
      }
    when 'application/json'
      # Return object metadata
      content_type 'application/json'
      object_list = Occam::Object.all_active
      if params["by_type"]
        object_list = object_list.where(:object_type => params["by_type"])
      end
      object_list.to_json
    end
  end

  # List all objects of a certain type
  get '/objects/types/:type' do
    objects = Occam::Object.where(:object_type => params[:type])
    tags = Occam::Object.all_tags(params[:type])

    render :haml, :"objects/type_index", :locals => {
      :objects => objects,
      :type    => params[:type],
      :tags    => tags,
      :tag     => nil
    }
  end

  get '/objects/:id/update' do
    object = Object.find_by(:id => params[:id].to_i)

    if object.nil?
      status 404
    else
      object.update
      redirect "/objects/#{object.id}"
    end
  end

  # Update a specific object
  post '/objects/:id', :auth => [:administrator] do
    object = Object.find_by(:id => params[:id].to_i)

    if object.nil?
      status 404
    else
      columns = {}

      # Administrators can set "active"
      columns = params.select { |k,_| [
          "active",
        ].include? k }

      unless columns.empty?
        object.update_columns columns
        object.save
      end

      redirect '/admin'
    end
  end

  # Explicit json content type
  get '/objects/:id.json' do
    object = Occam::Object.find_by(:id => params[:id])

    if object.nil?
      status 404
    else
      content_type 'application/json'
      object.to_json
    end
  end

  # Return the collection of all available object types
  get '/objects/types' do
  end
end
