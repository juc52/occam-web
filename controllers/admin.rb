class Occam
  # Administration page (only for admin users)
  get '/admin', :auth => [:administrator] do
    # Gather system configuration
    system = System.all.first

    # Gather pending people
    people = Account.all_inactive

    # Gather pending objects
    objects = Occam::Object.all_inactive()

    # Render form
    render :haml, :"admin/index", :locals => {
      :system   => system,
      :people => people,
      :objects  => objects,
    }
  end
end
