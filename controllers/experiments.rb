class Occam
  # Get a list of all experiments
  get '/experiments' do
    if params["tag"]
      experiments = Experiment.with_tag params["tag"]
    else
      experiments = Experiment.all
    end

    render :haml, :"experiments/index", :locals => {
      :experiments => experiments,
      :tags        => Experiment.all_tags,
      :tag         => params["tag"]
    }
  end

  # Renders a form to fork an experiment to some workset.
  get '/experiments/:uuid/:revision/fork' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    uuid  = params[:uuid]

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      revision = params[:revision]
      experiment.revision = revision

      render :haml, :"experiments/fork", :locals => {
        :errors           => nil,
        :current_revision => experiment.revision,
        :revision         => revision,
        :help             => params["help"],
        :recipe           => nil,
        :workset          => nil,
        :worksets         => current_person.worksets,
        :experiment       => experiment
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Forks an experiment (or updates form for no-javascript use)
  post '/experiments/:uuid/:revision/fork' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    uuid  = params[:uuid]

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      revision = params[:revision]
      experiment.revision = revision

      workset = nil
      group = nil

      if params["to"] && params["to"].start_with?("workset")
        workset = Workset.find_by(:uid => params["to"])
      elsif params["to"] && params["to"].start_with?("group")
        group   = Group.find_by(:uid => params["to"])
      end

      if params["update"]
        worksets = nil
        groups   = nil

        if workset.nil? && group.nil?
          worksets = current_person.worksets
        elsif workset.nil?
          groups = group.groups
        else
          groups = workset.groups
        end

        render :haml, :"experiments/fork", :locals => {
          :errors           => nil,
          :current_revision => experiment.revision,
          :revision         => revision,
          :help             => params["help"],
          :recipe           => nil,
          :worksets         => worksets,
          :groups           => groups,
          :to               => group || workset,
          :experiment       => experiment
        }
      else
        # Fork to given workset/group etc
        experiment = experiment.fork(params["name"], group || workset, current_person)
        if experiment.nil?
          # TODO: Error
        else
          redirect "/experiments/#{experiment.uid}/#{experiment.revision}"
        end
      end
    else
      # Unauthorized
      status 406
    end
  end

  # Redirect an experiment to its current revision
  get '/worksets/:workset_uuid/experiments/:uuid' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    uuid  = params[:uuid]

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      # Redirect to latest/base revision
      system = Occam::System.first
      if uuid[-9] == '-'
        code = uuid[uuid.length - 36 - 9..-1]
      else
        code = uuid[uuid.length - 36..-1]
      end

      object_path = experiment.path

      full_revision = experiment.fullRevision
      revision = full_revision

      if full_revision.nil?
        status 404
      end

      object_info = experiment.objectInfo

      workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
      if workset.nil?
        status 404
        return
      end

      parent_workset = workset

      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          parent_workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
          parent_info = parent.objectInfo()

          # TODO: move this to Group, allow it to iterate deeper but put a limit on it
          if parent_info.has_key? 'belongsTo'
            belongs_to = parent_info['belongsTo']

            if belongs_to[-9] == '-'
              code = belongs_to[belongs_to.length - 36 - 9..-1]
              type = belongs_to[0..-(38+9)]
            else
              code = belongs_to[belongs_to.length - 36..-1]
              type = belongs_to[0..-38]
            end

            if type == 'workset'
              parent_workset = Occam::Workset.where(:uid => belongs_to).first
            end
          end
        end
      end

      if parent and not workset
        # TODO: oh god
        workset = Occam::Workset.first
      end

      render :haml, :"experiments/show", :locals => {
        :current_revision => experiment.revision,
        :object_info    => object_info,
        :head_view      => true,
        :revision       => revision,
        :local          => false,
        :help           => params["help"],
        :person         => workset.authors.first, # TODO: fix
        :workset        => workset,
        :parent_workset => parent_workset,
        :group          => parent,
        :recipe         => nil,
        :experiment     => experiment,
        :job            => nil, # TODO: it is a set of jobs
        :job_info       => nil,
        :workflow       => nil,
        :forked_from    => nil,
        :forked_us      => nil,
        :authors        => [],
        :collaborators  => [],
        :graphs         => [],
        :groups         => [],
        :experiments    => []
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Redirect an experiment to its current revision
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    uuid  = params[:uuid]

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      # Redirect to latest/base revision
      system = Occam::System.first
      if uuid[-9] == '-'
        code = uuid[uuid.length - 36 - 9..-1]
      else
        code = uuid[uuid.length - 36..-1]
      end

      object_path = experiment.path

      full_revision = experiment.fullRevision
      revision = full_revision

      if full_revision.nil?
        status 404
      end

      object_info = experiment.objectInfo

      workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
      if workset.nil?
        status 404
        return
      end

      workset.revision = params[:workset_revision]

      parent_workset = workset

      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          parent_workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
          parent_info = parent.objectInfo()

          # TODO: move this to Group, allow it to iterate deeper but put a limit on it
          if parent_info.has_key? 'belongsTo'
            belongs_to = parent_info['belongsTo']

            if belongs_to[-9] == '-'
              code = belongs_to[belongs_to.length - 36 - 9..-1]
              type = belongs_to[0..-(38+9)]
            else
              code = belongs_to[belongs_to.length - 36..-1]
              type = belongs_to[0..-38]
            end

            if type == 'workset'
              parent_workset = Occam::Workset.where(:uid => belongs_to).first
            end
          end
        end
      end

      if parent and not workset
        # TODO: oh god
        workset = Occam::Workset.first
      end

      render :haml, :"experiments/show", :locals => {
        :current_revision => experiment.revision,
        :object_info   => object_info,
        :head_view     => false,
        :revision      => revision,
        :local         => false,
        :help          => params["help"],
        :person        => workset.authors.first, # TODO: fix
        :workset       => workset,
        :parent_workset => parent_workset,
        :group         => parent,
        :recipe        => nil,
        :experiment    => experiment,
        :job           => nil, # TODO: it is a set of jobs
        :job_info      => nil,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => [],
        :collaborators => [],
        :graphs        => [],
        :groups        => [],
        :experiments   => []
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Show a specific experiment at the given revision
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      actual_revision = experiment.revision

      if params["revision"]
        experiment.revision = params["revision"]
      end

      object_info = experiment.objectInfo

      workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
      if workset.nil?
        status 404
        return
      end

      workset.revision = params[:workset_revision]

      parent_workset = workset

      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          parent_workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
          parent_info = parent.objectInfo()

          # TODO: move this to Group, allow it to iterate deeper but put a limit on it
          if parent_info.has_key? 'belongsTo'
            belongs_to = parent_info['belongsTo']

            code = belongs_to[belongs_to.length - 36..-1]
            type = belongs_to[0..-38]

            if type == 'workset'
              parent_workset = Occam::Workset.where(:uid => belongs_to).first
            end
          end
        end
      end

      if parent and not workset
        # TODO: oh god
      end

      render :haml, :"experiments/show", :locals => {
        :current_revision => actual_revision,
        :object_info   => object_info,
        :head_view     => false,
        :local         => false,
        :help          => params["help"],
        :person        => workset.authors.first, # TODO: fix
        :workset       => workset,
        :parent_workset => parent_workset,
        :group         => parent,
        :recipe        => nil,
        :experiment    => experiment,
        :job           => nil, # TODO: it is a set of jobs
        :job_info      => nil,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => [],
        :collaborators => [],
        :revision      => revision,
        :graphs        => [],
        :groups        => [],
        :experiments   => []
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Run the experiment
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/run' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]

    if experiment && params["revision"]
      experiment.revision = params["revision"]
    end

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      object_info = experiment.objectInfo

      workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
      if workset.nil?
        status 404
        return
      end

      workset.revision = params[:workset_revision]

      # Queue work
      experiment.run(workset)

      redirect "/worksets/#{workset.uid}/#{workset.revision}/experiments/#{experiment.uid}/#{experiment.revision}"
    else
      # Unauthorized
      status 406
    end
  end

  # Show a specific experiment's tabulated results at the given revision
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/results' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]

    if params["revision"]
      experiment.revision = params["revision"]
    end

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      object_info = experiment.objectInfo

      workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
      if workset.nil?
        status 404
        return
      end

      workset.revision = params[:workset_revision]

      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          workset = workset || Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
        end
      end

      if parent and not workset
        # TODO: oh god
      end

      render :haml, :"results/show", :locals => {
        :object_info   => object_info,
        :head_view     => false,
        :local         => false,
        :help          => params["help"],
        :person        => workset.authors.first, # TODO: fix
        :workset       => workset,
        :group         => parent,
        :recipe        => nil,
        :experiment    => experiment,
        :job           => nil, # TODO: it is a set of jobs
        :job_info      => nil,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => [],
        :collaborators => [],
        :revision      => revision,
        :graphs        => [],
        :groups        => [],
        :outputs       => experiment.outputs('application/json'),
        :experiments   => []
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Show a specific experiment's tabulated results at the given revision
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/graphs/new' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]

    if params["revision"]
      experiment.revision = params["revision"]
    end

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      object_info = experiment.objectInfo

      workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
      if workset.nil?
        status 404
        return
      end

      workset.revision = params[:workset_revision]

      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          workset = workset || Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
        end
      end

      if parent and not workset
        # TODO: oh god
      end

      render :haml, :"results/graph_builder", :locals => {
        :object_info   => object_info,
        :head_view     => false,
        :local         => false,
        :help          => params["help"],
        :person        => workset.authors.first, # TODO: fix
        :workset       => workset,
        :group         => parent,
        :recipe        => nil,
        :experiment    => experiment,
        :job           => nil, # TODO: it is a set of jobs
        :job_info      => nil,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => [],
        :collaborators => [],
        :revision      => revision,
        :graphs        => [],
        :groups        => [],
        :outputs       => experiment.outputs('application/json'),
        :experiments   => []
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Edit a specific experiment configuration at the given revision
  post '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/configurations/:index' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]

    if params["revision"]
      experiment.revision = params["revision"]
    end

    workset = Occam::Workset.where(:uid => params[:workset_uuid]).first
    if workset.nil?
      status 404
      return
    end

    if experiment.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)

      object_info = experiment.objectInfo

      if experiment.revision == revision
        redirectBase = true
      else
        redirectBase = false
      end

      if workset.revision == params[:workset_revision]
        redirectWorksetBase = true
      else
        redirectWorksetBase = false
      end

      workset.revision = params[:workset_revision]

      connections = experiment.workflow["connections"]

      applies = params.keys.select do |key|
        key.start_with? "apply-recipe"
      end.first

      if applies
        recipe_index = applies[13..-1]
        puts "RECIPE!! #{recipe_index}"

        connection_index, configuration_index = recipe_index.split('-').map(&:to_i)

        recipe_index = params["recipe-#{recipe_index}"].to_i

        # Apply the recipe
        # Ignore all other data
        puts "connection: #{connection_index}"
        puts "configurat: #{configuration_index}"
        puts "recipe_ind: #{recipe_index}"

        connection = connections[connection_index.to_i]
        object     = connection["object_realized"]
        info       = object.objectInfo
        recipes    = info['configurations'][configuration_index]['recipes']
        recipe_info = recipes[recipe_index]
        recipe_data = object.retrieveJSON(recipe_info['file'])

        # Form the data parameter with that recipe
        data = {
          connection_index => {
            configuration_index => recipe_data
          }
        }
      else
        data = params["data"]

        Configuration.decode_base64(data)
      end

      puts "foo"
      puts data
      data.each do |connection_index, connection_data|
        # Pull object information
        connection = connections[connection_index.to_i]
        object     = connection["object_realized"]
        connection_data.each do |configuration_index, configuration_data|
          experiment.configure(connection_index, configuration_data, workset)
        end
      end

      redirect_url = "/worksets/"

      if redirectWorksetBase
        redirect_url << "#{workset.uid}/"
      else
        redirect_url << "#{workset.uid}/#{workset.revision}/"
      end

      redirect_url << "experiments/"

      if redirectBase
        redirect_url << uuid
      else
        redirect_url << "#{uuid}/#{revision}"
      end

      redirect(redirect_url)
    else
      # Unauthorized
      status 406
    end
  end

  # Redirect an experiment to its current revision
  get '/experiments/:uuid' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    uuid  = params[:uuid]

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      # Redirect to latest/base revision
      system = Occam::System.first
      code = uuid[uuid.length - 36..-1]

      object_path = experiment.path

      full_revision = experiment.fullRevision
      revision = full_revision

      if full_revision.nil?
        status 404
      end

      object_info = experiment.objectInfo

      workset = nil
      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        if belongs_to[-9] == '-'
          code = belongs_to[belongs_to.length - 36 - 9..-1]
          type = belongs_to[0..-(38+9)]
        else
          code = belongs_to[belongs_to.length - 36..-1]
          type = belongs_to[0..-38]
        end

        if type == 'workset'
          workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
          parent_info = parent.objectInfo()

          # TODO: move this to Group, allow it to iterate deeper but put a limit on it
          if parent_info.has_key? 'belongsTo'
            belongs_to = parent_info['belongsTo']

            if belongs_to[-9] == '-'
              code = belongs_to[belongs_to.length - 36 - 9..-1]
              type = belongs_to[0..-(38+9)]
            else
              code = belongs_to[belongs_to.length - 36..-1]
              type = belongs_to[0..-38]
            end

            if type == 'workset'
              workset = Occam::Workset.where(:uid => belongs_to).first
            end
          end
        end
      end

      if parent and not workset
        # TODO: oh god
        workset = Occam::Workset.first
      end

      # TODO: This may not be a good reflection of the workset revision??
      redirect "/worksets/#{workset.uid}/#{workset.revision}/experiments/#{experiment.uid}/#{experiment.revision}"
    else
      # Unauthorized
      status 406
    end
  end

  # Show a specific experiment at the given revision
  get '/experiments/:uuid/:revision' do
    experiment = Experiment.find_by(:uid => params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]

    if params["revision"]
      experiment.revision = params["revision"]
    end

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      object_info = experiment.objectInfo

      workset = nil
      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        if belongs_to[-9] == '-'
          code = belongs_to[belongs_to.length - 36 - 9..-1]
          type = belongs_to[0..-(38+9)]
        else
          code = belongs_to[belongs_to.length - 36..-1]
          type = belongs_to[0..-38]
        end

        if type == 'workset'
          workset = Occam::Workset.where(:uid => belongs_to).first
        elsif type == 'group'
          parent = Occam::Group.where(:uid => belongs_to).first
        end
      end

      if parent and not workset
        # TODO: oh god
        workset = Occam::Workset.first
      end

      # TODO: This may not be a good reflection of the workset revision??
      redirect "/worksets/#{workset.uid}/#{workset.revision}/experiments/#{experiment.uid}/#{experiment.revision}"
    else
      # Unauthorized
      status 406
    end
  end

  # Retrieve a list of tags for autocomplete
  get '/experiments/tags' do
    Experiment.all_tags_json(params["term"])
  end
end
