class Occam
  get '/search' do
    if request.preferred_type.include? 'application/json'
      simulators = Simulator.basic_search   params["q"]
      experiments = Experiment.basic_search params["q"]

      content_type "application/json"
      [
        {
          :title => "Simulators",
          :results => simulators.map do |simulator|
            ["/simulators/#{simulator.id}", "#{simulator.name}"]
          end

        },
        {
          :title => "Experiments",
          :results => experiments.map do |experiment|
            ["/experiments/#{experiment.id}", "#{experiment.name}"]
          end
        }
      ].to_json
    else
      render :haml, :"search/index", :locals => {
      }
    end
  end

  post '/search' do
    query = params["search"]

    types   = Object.basic_type_search(query)
    objects = Object.basic_name_search(query)

    render :haml, :"search/results", :locals => {
      :types   => types,
      :objects => objects
    }
  end

  get '/search/configurations/:id' do
    simulator = Simulator.find_by(:id => params[:id].to_i)

    render :haml, :"search/configurations", :layout => !request.xhr?, :locals => {
      :simulator => simulator,
      :schema    => simulator.input_schema,
    }
  end

  get '/search/simulators' do
    if request.preferred_type.include? 'application/json'
    else
      render :haml, :"search/simulators", :layout => !request.xhr?, :locals => {
        :simulators => Simulator.names
      }
    end
  end

  post '/search/simulators' do
    simulators = Simulator.search_adv   params["name"],
      params["tags"],
      params["description"],
      params["website"],
      params["organization"],
      params["license"],
      params["authors"]

    render :haml, :"search/results", :locals => {
      :simulators  => simulators,
      :experiments => []
    }
  end

  get '/search/experiments' do
    if request.preferred_type.include? 'application/json'
    else
      if request.xhr?
        render :haml, :"search/experiments", :layout => false, :locals => {
          :simulators => Simulator.names
        }
      else
        render :haml, :"search/experiments", :locals => {
          :simulators => Simulator.names
        }
      end
    end
  end

  post '/search/experiments' do
    if params["config"]
      redirect "/search/configurations/#{params["simulator_id"]}"
    else
      experiments = Experiment.search params

      render :haml, :"search/results", :locals => {
        :simulators  => [],
        :experiments => experiments
      }
    end
  end

  # Get a search result
  get '/search/results' do
    query = params["q"]
    simulators = Simulator.search params["q"]
    experiments = Experiment.search params["q"]
  end
end
