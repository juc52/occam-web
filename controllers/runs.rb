class Occam
  # Get a list of all runs
  get '/runs' do
  end

  # Cancel the run
  get '/runs/:id/cancel' do
    run        = Occam::Run.find_by(:id => params[:id])
    if run.nil?
      status 404
      return
    end

    experiment = run.experiment
    workset    = run.workset

    if workset.nil?
      status 404
    else
      if workset.can_edit?(current_account)
        run.cancel

        if request.preferred_type('text/html')
          # Redirect to the workset page (if asking for html)
          if group
            redirect "/worksets/#{workset.id}/groups/#{group.id}/experiments/#{experiment.id}"
          else
            redirect "/worksets/#{workset.id}/experiments/#{experiment.id}"
          end
        elsif request.preferred_type('application/json')
          # If asking for json, return success or fail
          content_type 'application/json'
          {
            :result => "success"
          }.to_json
        else
          status 404
        end
      else
        status 406
      end
    end
  end

  # Start the run
  get '/runs/:id/run' do
    run        = Occam::Run.find_by(:id => params[:id])
    experiment = run.experiment
    group      = experiment.group
    workset    = experiment.workset

    if workset.nil?
      status 404
    else
      if workset.can_edit?(current_account)
        run.run

        if request.preferred_type('text/html')
          # Redirect to the workset page (if asking for html)
          if group
            redirect "/worksets/#{workset.id}/groups/#{group.id}/experiments/#{experiment.id}"
          else
            redirect "/worksets/#{workset.id}/experiments/#{experiment.id}"
          end
        elsif request.preferred_type('application/json')
          # If asking for json, return success or fail
          content_type 'application/json'
          {
            :result => "success"
          }.to_json
        else
          status 404
        end
      else
        status 406
      end
    end
  end
end
