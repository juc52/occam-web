class Occam < Sinatra::Base
  # Retrieve login form
  get '/login' do
    render :haml, :"sessions/login", :locals => {:errors => nil}
  end

  # Sign on
  post '/login' do
    account = Account.where("LOWER(username) like ?", params["username"].downcase).take
    if account && account.authenticated?(params["password"])
      new_session = account.person.activate_session
      session[:person_id] = account.person.id
      session[:nonce] = new_session.nonce

      redirect "/people/#{account.person.uid}"
    else
      status 422

      errors = Person.new.errors
      errors.add(:username, "or password is invalid")

      render :haml, :"sessions/login", :locals => {
        :errors => errors
      }
    end
  end

  # Sign out
  get '/logout' do
    if current_person
      current_person.deactivate_session_for(session[:nonce])
    end
    session[:person_id] = nil
    session[:nonce] = nil

    redirect '/'
  end
end
