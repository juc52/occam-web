class Occam
  # Get the system configuration
  get '/system' do
    system = System.all.first
    render :haml, :"system/show", :locals => {
      :system => system
    }
  end
end
